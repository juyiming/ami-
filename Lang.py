
class Lang:
    def __init__(self, name):
        self.name = name
        self.word2index = {"PADING":0,"SOS":1,"EOS":2}
        self.word2count = {}
        self.index2word = {0:"PADING", 1: "SOS", 2: "EOS"}
        self.n_words = 3  # Count SOS and EOS
        self.person2index = {"A": 0, "B": 1, "C": 2, "D": 3, "E": 4, "F": 5}
        self.index2person = {0 :"A", 1: "B", 2: "C", 3: "D", 4: "E", 5: "F"}
        self.n_persons = 5  # Count SOS and EOS


    def addSentence(self, sentence):
        for word in sentence:
            self.addWord(word)

    def addWord(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.n_words
            self.word2count[word] = 1
            self.index2word[self.n_words] = word
            self.n_words += 1
        else:
            self.word2count[word] += 1


    def getIdFromWord(self,word):
        if word in self.word2index:
            return self.word2index[word]
        else:
            print(word)
            return -1

    def getWordFromId(self,id):
        if id in self.index2word:
            return self.index2word[id]
        else:
            return None

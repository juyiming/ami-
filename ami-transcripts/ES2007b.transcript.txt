A#Think we can first #0
C#Right it was function F_ eight or something #0
B#Mm #0
C#This one right there #0
B#Tha Okay #0
C#Okay #0
B#Who is gonna do a PowerPoint presentation #0
C#Think we all Huh #0
B#You will as well #0
C#Oh I thought we all were #0
C#Yeah  I have one too  okay #0
B#Okay #0
C#S Whoops I forgot to put the thing on Okay #0
B#Yep #0
D##0
C##0
A#Right #0
A#I just wanna 'cause basically I can't re I've really crap at remembering everyone's name so I just wanna rather than going uh Miss Marketing and Miss this and Miss that wanted to know your names again #0
A##0
B#Okay I'm Okay #0
A#just gonna leave this up here 'cause I'll you know #0
C#Yeah #0
A##0
C#Sure  that's a good idea #0
A#So Okay  and Gabriel #0
B#I'm Catherine with a C_ #0
B#C_A_T_H_ E_R_ I_N_E_ #0
C#Uh Gabriel #0
A#E_L_ is it #0
C#E_L_ #0
A#'Kay #0
A#And you're s r R_E_I_S_S_ Okay #0
D#I am Reissa #0
D#R_E_I_S_S_A_ #0
D#Double S_ A_  yeah yeah #0
D##0
D#Sorry #0
A##0
D##0
A#'S just a bit nicer calling people by their names I think #0
C##0
B#Right #0
B#True #0
A#Uh  right #0
A##0
D#Mm 'kay #1
A#Okay  right  welcome to meeting B_ #0
A#Um this is gonna go a lot better than the last meeting  basically  uh 'cause I know what I'm supposed to be doing now #0
C##0
B##0
D##0
B##0
A#I am your Project Manager  and  uh yeah  I'm just here to sort of liaise between the three of you and get things going  get people talking and and I'm gonna be making notes and sending them off to the powers that be and stuff basically #0
A##0
B##0
A#Um right  this for the purposes of this meeting what this meeting is all about is um I'm gonna have some presentations from all three of you  what you've been working on for the last wee while  when you haven't been getting hit with spam on your computers and and  you know  filling out silly questionnaires and things #0
B##0
D##0
A#But hopefully you've been actually been doing something productive #0
A#So we're gonna each of you gonna give us a litt a little presentation #0
D#Mm #0
A#Um #0
A##0
A#Then we're gonna work  you know  from each of your presentations #0
A##0
A#We'll we'll uh talk about what we actually need as a final coming together of it all #0
A#Um and then we'll  yeah  we'll sort of conclude anything else comes up at the end of it #0
B#How long is the meeting #0
A#This meeting it's not very long #0
A#It's uh probably down to about thirty five minutes now #0
B#Okay #0
A#So I want each of your presentations to not be too long  five five minutes  something like that #0
B##0
B#No problem #0
B##0
A#Um if you haven't done a PowerPoint thingy  it doesn't matter  it it just it just says that you it's that's just one particular medium #0
A#If you haven't had time to prepare one  you can draw stuff on the noteboard  you can talk to us  you can you know however you want to do your little presentation  basically  you can #0
A#Don't feel pressurised into using this thing #0
A#'Cause I don't #0
B##0
A#Uh okay #0
A##0
A#So um #0
A#You okay over there #0
A#Reissa  are you uh b are you joining in with this meeting here or are y or are y or are you are you just are you just uh doing some Internet shopping there #0
D#I'm fine #0
D#Yeah #0
D#I uh yeah  yeah #0
C#Think she's finishing up her presentation #0
D#D I mean  I I'm finishing off my presentation #0
D#No no #0
B##0
D##0
A##0
D#Uh I'm done #0
D#Okay #0
A#Okay  jolly good #0
A#Alright  let's have um well  we all know that it's it's a remote control that we're gonna be dealing with #0
A#I think the first thing we should look at is um probably the um what it is that it is actually supposed to be #0
D#Mm #0
A##0
A#So that's gonna be you Catherine  if we wanna hear from you first #0
B#Okay #1
B#Okay #0
B#Um just connecting this #0
B#Are we getting i Really #0
A#You don't have to worry about screwing it in just there you go #0
B#Okay #0
B#Cool #0
B#Okay #0
B#So I've got a very quick uh Uh #0
B#Okay #0
D##1
B#So the working design  I've got a very quick presentation on this  so um I've oh no  you can't see a thing #0
B#Oh well  I'm gonna draw it on the board then #0
B##0
B#It's in blue uh  and I couldn't change it #0
C#Oh #0
C##0
B#We it's fine on my screen  but never mind #0
A#Ah #1
B#So um the idea is that we've got the energy source um  which in our case will pr  oh well okay  never mind #0
B##0
B#So um I think maybe uh two batteries  I dunno what they're called six  or something like that #0
A#Mm-hmm #0
B#Uh and then um then on the uh remote control itself will have um the sender for the signal  which could be uh an infra-red signal  um which will be sent by an electronic chip #0
B##0
B#And uh the chip will be controlled by the user interface #0
B#So we'll hear about that later from Gabriel #0
A#Mm-hmm #0
B#And uh the sender will send to the telly itself an infra-red signal to tell it to switch on or switch channels #0
B#Um and that's it really for the working design #0
A#Great #0
A#Okay #0
B#Sorry the presentation wasn't very uh clear but Really #0
A#I prefer the pe I prefer the human touch personally #0
B#Cool #0
A#Yeah #0
B#Um  should I erase this or Okay #0
A#Do you wanna just give us a moment  I just wanna copy this down #0
A#Um I dunno if you guys have got any questions for Catherine on any of this #0
B#Fine #0
B##0
B#Or suggestions #0
D#Is a battery like the only way of No  no No I meant like No 'cause like cha 'cause always changing um um batteries can get like annoying #0
B#Well  it's just  you don't want it plugged in really  s In indoors #0
C#Yeah  alternate energy source  like win wind power or Bicycle power #0
A##0
B##0
A#Yeah  you blow on it and i Yeah  it's worked for the last fifty years you know #0
C##0
B##0
A##0
B##0
D#The battery's down and maybe  I dunno  solar charged #0
D##0
B#I dunno  swi I th I th I think changing your batteries once every six months is not really a pain  but Yeah #0
D##0
D#Mm #0
D#Yeah #0
C#One question I have  and I don't know how much control we have over this is um  as far as the infra-red signal  do we have control over  you know  how far away you can be from the receiving unit  the the T_V_  and still have it be operational #0
C#I mean  maybe we want one with a strong signal stream #0
A#How far away is your television #0
A#It's never gonna be more than it's never gonna be  you kno unless you've got a T_V_ the size of a football pitch  it's not doesn't have to go that far  does it #0
C#Uh That's true #0
B#Well  the thing is uh you you don't Well  we can make the the signal strong enough to go through walls if if you fancy it #0
A#Doesn't have to go through a wall  because you're not gonna be looking through a wall #0
D#Yeah  but if like you're on the phone in the other room and you need turn television off or something and you don't really want to go into the put the telephone down  and go into the other room #0
D##0
B#I didn't think about that but Why not #0
D#How about Bluetooth #0
D#Instead of using infra-red  use Bluetooth #0
D#Isn't that a better signal #0
B#I just think that it's it's gonna cost more and I'm I'm not sure it's you're gonna use it #0
A#Yeah  yeah I d it sounds like you you w don't wanna overcomplicate things #0
D#Mm #0
D##0
A#You know we don't need it #0
B#It's a fancy idea uh it's quite nice  but then I don't th I dunno  either you if you wanna watch the telly you're in the room  you are gonna But Oh  we can we can keep the idea if you i We can see at a later stage  maybe  I don't Do you need the border #0
A#Yeah  exactly #0
A#Basically  we're we're desi we're designing and marketing a television remote control unit #0
A#We're not w w w w designing something that you can plug in a headset to and and you know connect to your laptop computer and stuff #0
A#It's uh Okay #0
D#Mm #0
D#'S just an idea #1
A#Right  well done  Catherine #0
A#Um Gab Gabriel let's uh let's hear from you on on on such things #0
C#Okay #0
C#Uh I'm just gonna use the PowerPoint uh #0
B#Okay #0
B##0
B#Sorry #0
C#Technical #0
C#Okay #0
D##0
D##0
D#Adjusting #0
C#Okay  so  while this is warming up  there it is uh #0
D#Yeah #1
C#So I'm doing the user interface design #0
A#Mm-hmm #0
C#Yeah  and basically uh  as far as methods  I was I was looking looking at looking at uh already existing remotes  trying to find some inspiration from designs that are already out there #0
C#Thinking of what we can retain  what we can do away with  uh what we what we can perfect a little bit as far as design um #0
C#we don't want to do something that's too radical of a change  I guess  I mean people want a remote that's familiar  that has their favourite functionalities um and and does the basics  but Um so we can improve what's out there and maintain that  the basic functionality that people want #0
A#Mm yeah #0
C#Um so things that seem like absolute must-haves uh would be a volume control  um so up-down keys for that  uh channel keys up-down  but then also a numerical key pad so that they can just key directly to the channel that they want  rather than doing up-down  and uh a mute button #0
C##0
C#Uh one thing that I didn't include here  that I forgot that we talked about last time  was doing um some sort of lock uh function #0
C#Uh  I don't I dunno  uh that's one possibility #0
A#Okay #1
C#And so in the research that I was doing there's basically two types of remotes  ones that are engineering centred and ones that are more user centred  which I don't know if I can access the web page from here  but I can show you uh #0
C#Yeah #0
C#So this is a engineer centred one  so you see it's rather busy  but it also lets you play your movie  stop your movie  fast-forward  all this  um freeze frame #0
A#Mm-hmm #0
C##0
C#Uh and this is a user centred one #0
C#Uh it's it's easier to g just glance at this and see what's possible to do  you're not gonna be staring at it for five minutes #0
A#Yeah #0
C##0
A#Great #0
C#And I judging from what what we all talked about during our last meeting I kind of gathered that that's what we were going after  uh or the direction we were going in at least #0
C#Um #0
C#So  the engineering centred ones uh provide a lot of functionality  but it can be a little bit overwhelming  so the user centred ones just focus on ease of use #0
C#Uh and this sort of overlaps with what the marketing person uh  Reissa  because uh we we need to find out what what people want before we make firm decisions on this #1
D##0
D#Mm #0
A#Yeah #0
D##0
D##0
C#So uh  yeah  that's me #0
A#Great #0
A#Okay #0
A#Now that's I just have a q a q question for you #0
A#This w um research that you've been doing looki looking at other  you know  existing units stuff #0
A#Um have you found that anyone else has do has looked into the locking function or Yeah #0
C#No that that that seemed like a novel idea as f as far as I know #0
C#I mean obviously another exists like you like you said in in mobile phones #0
C#That was sort of the inspiration for it #0
C#Um I've never seen that with in in all my years in in the remote business #0
A#Yeah #0
C#I've  haven't I've never seen a locking functionality #0
C#I dunno  what uh do you guys have a a yea or nay on that a feeling about whether that's really necessary #0
A##0
A##0
A#Um I would say it's If it's simple to do  which I think it probably should be  even if it's a physical  you know  a f a like a f a physical switch or a physical cover for the remote  even something like that  um then yes  it's like  you know  like s you said earlier on ab ab ab a flip thing  something like that  but you know being physical #0
C#Yeah #0
C#Mm-hmm #0
C#'Kay #0
C#Right #0
A#Look into #1
A#Um I've had word down from head office that something that we should be centred well  something we should take into account is um we've gotta keep the corporate image within this remote control unit #0
A##0
A#It's gotta d look like it's in the R_ and R_ #0
C#Mm #0
C##0
A#You know  the the company it's it's  from what I can see from our other products  are yellow with blue writing on them #0
D##0
D##0
C#Right #0
D#Mm #0
A#Um #0
C#And our motto is is we put the fashion in electronics #0
A#We put the fashion in electronics #0
D#Mm #0
C#I think I think we have to carry that mental #0
A#There you go #0
A#So it's kinda gotta look it's gotta look new and s you know something fashionable #0
A#If if remote control well  if telephones can be fashionable  then maybe remote control units can be #0
C#Mm #0
D#Mm #1
C#Well yeah these  I think  we can so we talked about the layout in my presentation and what I didn't mention yet really is is the sort of like the ergonomic design #0
A#Yeah #0
A#Because we need Yeah #0
C#I t I think we can make big improvements over these two that you see here  I mean #0
A##0
B##0
C#Uh  everything is going t ergonomic  you know  there's you know mice for your computers that are very ergonomic and keyboards and that could be one of our niches p sort of uh uh in the market  I guess #0
A#Great #0
A#Okay  fantastic #0
C#Um #0
A#Right  well done  Gabriel #1
D#Okay #0
A#Um Reissa #0
D##0
D#Where does it go into #0
A#Let's plug you in  baby #0
C##0
D#Here #0
C#Yeah #0
D#The blue thing #0
C#Uh  yeah  this is getting all #0
D##0
C#Mm #0
C##0
D##0
C#Yeah  then you just have to do function F_ eight and it should come up #0
D#Well  function F_ eight #0
D#No oh #0
A#Yeah  w it it just takes a wee while #0
C#Yeah  it just takes a second uh #0
D##0
D#Come on #1
D#Right #0
D#Okay #0
D##0
D#Okay #0
D#Well  for our marketing report uh we observed remote control users in a usability lab  and also gave so this is research and we also gave participants um questionnaires to fill out #0
D#Um total number of people tested were a hundred just so you know  so that hundred people were tested and these were the findings #0
D#So seventy five per cent of users find the remote control ugly #0
D#Okay  so they don't like the look of the remote control #0
D#Um eighty f eighty per cent of them would spend more money if the rem remote control looked really cool and fancy #0
D#So I think we all agree with that #0
D#Um current remote controls do not match well with the operating behaviour of the user #0
D##0
D#So  they don't like like the way they operate it doesn't like match how people behave #0
D#Um per cent of the users say that they only use ten per cent of the buttons on a remote  so probably if you have like one  two  three  four  five  the whole up to z ten  they probably don't use those  they only use the up and down channel #0
A#'Cause we've only got five channels #0
D##0
D#exactly #0
B##0
D#That's another thing #0
D##0
A##0
D#Um seventy five per cent of users say they zap #0
D##0
D#Not quite sure what they mean  zap  goes like #0
D##0
A#I think that's k flicking quickly between channels #0
C#Yeah  you wanna navigate the channels quickly I guess #0
D#Yeah #0
A#Yeah #0
D#Mm #0
D#Um takes too much time to learn how to use a new remote #0
D#I think especially for uh the older generation #0
D#I know my grandmother doesn't like mobile phones  takes ages to work how to use #0
D#Anyway um and they also remotes often get lost in the room  so nobody can find them #0
C#Mm #0
D#So maybe tracking devices is a good idea #0
D##0
D#Um personal preferences #0
A##0
A#Wow #0
A#You are a child of technology  aren't you #0
D##0
D##0
D##0
D#Um so yeah um I was thinking something easy to use  especially for older people #0
A##0
D##0
D#Um has to look really cool  flashy groovy for people to buy it #0
D##0
D#And it's easy to find  so I don't know whether maybe and also we asked them whether they wanted whether they'd be interested in um voice activating #0
D##0
B##0
D##0
D##0
D#So voice activation #0
D#So and this was what we came up with #0
D#Then if you look fifteen to twenty five this is age  sorry  age groups #0
D#So fifteen to twenty five said like ninety two ninety one per cent of them said yes #0
A#So there you go  yeah #0
D#Um so basically the majority except for the forty five to fifty five year olds for some reason didn't want a voice activated one #0
D##0
D##0
D#And neither did the older generation  but the younger generation who we are catering for  like who have most of the money nowadays  do want a voice act speech recognition in a remote #0
B##0
A#Uh but do the younger generation have the money #0
C#No I would I would say the older the older people  yeah #0
A#They they don't #0
A#It's older generation  they're the ones that have gone out and People people from the age of thirty f there's a big drop off there #0
D#Well the twenty five to thirty five year old  and thirty five  and the thirty five to forty five  forty five point seven per cent say no  so So they don't Well These guys are growing up #0
D##0
A#For people up to the age of thirty five  you're kinda saying  yeah  they want it #0
A#Um but no they're not sort of most people that have the money are people from the age of thirty five to fifty five  uh 'cause they're the ones that have been working for twenty years #0
C#Yeah  that would be my guess as well #0
A#Um d and tha and that's a that's quite a minority there  so yeah  it's not even like fifty fifty that's th thirty five per cent #0
A##0
A##0
D##0
C#What about just from the the prospective of our manufacturing cost #0
C#I mean if if it's twelve fifty per unit #0
A#Yeah #0
C#I mean  okay  there's Uh #1
D##0
D#Voice activation might not be the best #0
A#I would say scra I'd say scrap that straight off #0
D#Um also with um with buttons  a thing called R_S_I_  so wrist sense Huh #0
D##0
C#Repetitive strain uh rep repetitive strain injury or like from doing Mm #0
D##0
D#Yeah  repetitive strains injury  so they don't I think people who watch T_V_ maybe too often  keep changing channel hurts their wrist #0
B##0
D#I don't think so #0
D##0
B#Well maybe they shouldn't watch so much T_V_ then #0
B##0
C##0
A#So y so it's so it's so you got so that's something we should have a look into then i when desi when designing the ergonomics of see have a look if um there's any w any medical background we can find out about this #0
B##0
D##0
D#Yeah maybe not so hard #0
D##0
C#Mm #0
A##0
B#Maybe it could be  instead of pressing button it could be just touching a Yep #0
D#Yeah #0
A#Let's jus we just want need to cover our arses so that people aren't gonna sue us in ten years' time  say your remote control gave me repetitive strain injury #0
D#Maybe Mm #0
C#Yeah  we should probably consult with our legal department uh #0
A#Yeah #0
A#They're having a lunch break at the moment  but yeah #0
A##0
D##0
B##0
D#Yeah #0
A#I'll see if I can get see if I can get hold of them for the next meeting #0
C#Yeah #0
C#I think we can do some really in in that department  the the ergonomic department  we can make some some really good improvements #0
A#Yeah #0
D#Mm #0
D#Maybe th the buttons not so high up so you don't have to press so much  or we just like flat buttons  something #0
C#Mm #0
D#Yeah #0
A#Okay #0
D#So that is me #0
A#That's great  thank you very much for that  Reissa #0
A#Um okay  so we've basically we've decide we've d we've decided that it's gonna be  you know uh  we're going for a basic television remote #0
A##1
A#It's gotta be safe to use  it's gotta look cool #0
D#Mm #0
A#It's gotta be cheap #0
A#S um #0
D#Mm #0
A#Now going back to the uh industrial design of it  you know  we were looking at whether to use maybe infra-red or Bluetooth #0
A#I think  we should just go with the simplest option on everything  uh and that would be infra-red  energy source  that would be batteries #0
A#Uh mean we we can look into using the s  you know  the little tiny weeny batteries  all like special long-lasting batteries #0
A#Um #0
A##0
A#But a in there's no I don't think there's any point in making a remote control unit that's gonna last for fifty years  because technology will have changed and  you know  we won't have televisions in ten years' time #0
A#So I think we're all um pretty sussed on that #1
A##0
A#Um anyone have any questions #0
D#Mm #0
A#Everybody happy in their work #0
C#Yeah  it seems like we're all on the pretty much on the same page #0
A#Now this is good  we've got a good structure going on #0
A#We all know where we're going to #0
A#Have you been ge has have any of you lot been getting loads of crap spam on your computers #0
A##0
C#Oh it's probably just you  'cause you're the project manager #0
B##0
A##0
D#Well  just questionnaires #0
C##0
A#Yeah #0
C#Sell trying to sell your things #0
A#Yeah  stuff #0
D##0
A#Um okay #0
A#Do oh have you guys found out if we can if we can e-mail stuff to each other #0
C##0
D#Yeah  you can #0
A#Right #0
C#Okay #0
A#Do all you all know my e-mail address #0
C#No I don't #0
D#I think he's participant one  aren't you #0
B#Well  in the project announcement  you've got the addresses  I think #0
C#I Uh Oh  it's just participant one oh okay #0
B#So Project Manager  it's participant one at A_M_I_ #0
C#Yeah #0
A#Can you all d e-mail me your e-mail addresses #0
B##0
D##0
C#Well it's just w it's just it's just par participant one  participant two #0
B#You have them i you have them  but we'll send you an e-mail #0
A##0
A##0
A#Send me  yeah yeah  okay #0
C##0
B#You want to have friends  don't you #0
C##0
C#So are we headed towards like a b a big yellow and black remote as far as maybe that's our next meeting that we discuss that #0
A##0
A#Is it yellow and black or is it yellow and blue #0
A#I I kind of thought it was blue writing on a yellow background  but I might be just going a bit Okay #0
C#Well  it's like white on i white and blue on a black background with white with yellow borders #0
C#Maybe that's like getting ahead of ourselves #0
A#Well  maybe you can come up with a few with a couple of different ideas #0
C##0
B#It wouldn't be Well you could It could come But it could come in different colours and have the R_R_ colours just somewhere like just around the lock button could be the R_R_ logo or colours and If #0
C#Mm #0
D#Can't we have different colours in the remotes  so somebody can choose different col like does it have to be of a certain #0
A#Well  see the thing is is we've gotta keep the company image #0
C#Mm #0
A#It's gotta say people have gotta look at this remote control and instantly recognise that it's a Real Reaction i product #0
D#But if it's a R_R_  it would be Real Reaction  like if it had a symbol on it #0
C#Mm #0
A#There's loads of companies that called R_R_ #0
C#Mm #0
D#Whoa #0
A#This is slog but this is the slogan  this is the the the the type #0
C#And this is something that came on down from from the higher ups  so  I mean  we are sort of beholden to them #0
A#Yeah #0
D#So we have to have it one colour #0
A#Well  not necessarily #0
C#Not one colour  but the pattern needs to be recognisable #0
A#But we have to incorporate it #0
D#Mm #0
A#Not necessarily even if i it just has to ye but you have to look at the product and instantly know that it's one of our products as opposed to a Sony product or a  you know  a Panasonic product #0
D#Mm #0
A#It's got to so maybe  so you can have a look at our our other products and see if they all follow a s similar vein  perhaps #0
A#Yeah #0
B##0
A#Yeah #0
D#Mm #0
A#Quite poss yeah #0
A#Well this is all your department #1
A#Mm okay #0
A#Well  well done everybody #0
C##0
A#And um  I think we uh stop for lunch now #0
C#Yeah  pretty soon I think  I guess that's now #0
D#Are we are we finished ahead of schedule #0
A#We might possibly have done #0
B#Cool #0
C#Alright  see you all soon #1
A##0
A#If we've if we've finished at five minutes before the meeting's supposed to finish  then that means we get an extra five minutes for lunch #0
D##0
D#'Kay uh #0
A#Yeah  there you go #0
A#Right #0
A#I just have to there's a few little bits and pieces I have to write down  but Take your headsets off  kick back  smoke 'em if you got 'em #0
A##1

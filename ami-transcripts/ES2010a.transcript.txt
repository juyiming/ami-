A#Okay #0
A#Hi everybody and welcome to our kick-off meeting um for our new product that we're gonna be designing #0
A#Um I'm Mandy and I'm the Project Manager #0
A#And I know all your names again  Courtney  Fenella and Amber #0
D#Yep #0
B#Yep #0
A#Alright #0
A#Okay  so first let's go through this PowerPoint #0
A#I wonder what button I press #0
C#Just do it on the arrow #0
A#Yeah  or how about I just click #0
A#Okay  here is our agenda for this meeting #0
A#Um we're gonna start with our opening which was our introductions #0
A#We're gonna get to know each other a little bit better #0
A##0
A#Um tool training  we're going to  I guess  figure out what to do on this project with our individual roles #0
A#Um we're gonna make a project plan and then have some time for discussion and close up the meeting #1
A#Okay  here is our project #0
A#We're gonna make a new remote control that's um original  trendy and also user-friendly #0
A#And how we are going to do it is each of us is going to um We're gonna have discuss the functional design first  how is it gonna be used  what's the actual goal here  it has to operate T_V_  blah blah blah #0
A##0
A#And we're going to do individual work on that and then meet #0
A#Same thing with conceptual design #0
A#Just the basic overview of the project and then we're going to do individual work  meet #0
A#That's pretty much the the whole process for today #0
A#And then the detailed design  just more in-depth  get the actual schematics of the remote #1
A#Okay #0
A#Alright #0
A#First we're gonna start off by using our tools #0
A#And the whiteboard thing  do you guys wanna give that a try even though the ink wasn't working or do you wanna do it on here #0
B#I think we should forgo the whiteboard since we can't actually see what we're writing #0
D#We could Yeah  we could on here #0
A#Alright  let's go forward then #0
C##0
A#Um right now so we're all gonna draw our favourite animal and then sum up our favourite characteristics of that animal #0
A#Even if you are not a good drawer like me #0
C##0
A#Alright #0
A##0
B#Artistic skills  nil #0
C#Fine #0
A#Um #0
C##0
D##0
A#Bless you #0
D#Oh  thanks #0
D##0
B##0
A##0
D##0
B##0
B#I draw like I'm in grade five #0
C##0
A##0
A#Oh do I #0
A##0
A#'Kay  about one more minute #0
A##0
D##0
A#Okay #0
A#Okay #0
A#And who would like to start us off #0
D#I'll go #0
A#Alright #0
D#Um this is my picture #0
D##0
D#I drew fish I like fish  because uh  you know  their whole water-vascular system thing #0
D##0
C##0
D#It's pretty cool  and um they've got a pretty good habitat and they are pretty sometimes  sometimes vicious but that's okay #0
A##0
D#Yeah #0
A#Only if they're piranhas #0
C##0
D#Yeah  they they're easy  you know #0
D#Yeah #0
A#Alright #0
A#Who wants to go next #0
C##0
B#I'll go #0
B#I drew a kitty #0
B#It's pretty much impossible to tell that's a cat  but I love cats #0
D#No I I see it #0
C#No  I kne I knew #0
A#No  it looks like a cat #0
D#Yeah  it does look like a cat #0
B#I love cats because they're independent  uh they pretty much know what they want  they get it  they move on #0
B##0
A#I love cats  too #0
C#Yeah #0
A#I'm a cat person #0
D#I'm allergic to cats #0
C#Ah #0
A#Uh #0
B#I'm allergic to cats  too #0
D#Oh  okay #0
D##0
D##0
A#If you're around one I had a roommate who was um allergic  but if she was around my cat forever she became used to it  you know  it's weird #0
B##0
C#In my next life #0
D#Yeah  yeah  if you're around them for a long period of time Oh  yeah  this summer I  oh I had to live with cats #0
B#I still can't sleep with them in my room #0
A#Okay #0
A##0
D#It was crazy #0
D#Yeah #0
A#Okay  Fenella #0
C#Um  I drew a badger #0
C##0
D#Cool #0
B#Yay #0
A#Badger #0
C##0
C#Well  yeah #0
D##0
A#Good choice #0
A#Why a badger #0
C#Uh I dunno  they're grumpy and nocturnal and Well  a little bit like the Yes #0
C##0
D##0
B##0
B#Are you trying to suggest something #0
C##0
A##0
C#Um #0
C#And then  if you know Wind in the Willows badger #0
C##0
D#Oh  okay #0
C#Yeah and then uh I don't know if you know Brian #0
C#He's Liverpudlian writer #0
A#Alright #0
C#Um  that kind of books #0
C#Badgers are cool in that one too #0
C##0
A#Okay #0
A#And I'm last #0
A#'Kay #0
A#Look at my sad sad giraffe #0
D#No  that's good #0
A#No  no  no  it ends up looking like some kind of a dinosaur  but whatever #0
A#I don't know even much about giraffes  but I just love the way they look #0
A#They're just such odd creatures  you know #0
A#I I like that they're so unique and individual  I guess #0
A#I don't know much about their behaviour or anything  though #0
A#Only seen a couple in zoos #0
D#You don't really have to  I mean  if you like 'em Right #0
A#Yeah  but you can appreciate the way they look #1
A#Okay #0
A#Alright #0
A#Guess we're getting straight back into business here #0
A#Um the selling price for our remote is going to be twenty-five Euro  and our profit aim is fifty million Euro #0
A#We're going to make this an international product marketed in the States  in Europe  in Asia #0
A#And um our production cost to make that profit is gonna be a max of twelve fifty Euro per remote #0
A#Okay #0
A#So we're gonna talk for a little while #0
A#Um here are some topics that we might be able to discuss #0
A#Expe our experiences with remote controls um  our first ideas about this new remote  anything that you can bring to the table for this project #0
A#So #0
C#Now #0
A#Yeah #0
A#You wanna start us off #0
A#Anybody have anything to offer #0
C##0
B#Well  we wanna make a multifunctional remote  right #0
A#Right #0
B#One remote for everything #0
C#And everything being Wait  we have what  sound system  T_V_  D_V_D_  V_H_S_  uh TiVo #0
B#Um #0
B#I think they'll be phasing V_H_S_ out shortly #0
D#Yeah  TiVo #0
A#TiVo #0
C#But it's still there  so if po if we're gonna do it Or if it's not like a Sony  if it's like a I don't know #0
B#Okay #0
D#It needs to be compatible 'cause universal remote controls are never universal #0
A#They're never universal #0
A#That's right #0
A#Esp e especially if you buy a a not big product  D_V_D_ player  say  usually it doesn't work if it's not one of the Yeah #0
A#Yeah #0
A#Something from Sam's club #0
B#So we'll have to figure it how to cover all the different variances in signals #0
A#Yeah #0
D#And what we need an insanely good instruction booklet  because you always have to reconfigure all your contraptions to go with the remote anyways #0
A#Yeah #0
A#'Kay  and um another thing that I think is important is the d the design of the product  how it feels in your hand #0
A#If it's just flat and kind of boring th those don't Nobody wants to buy those any more #0
A#They want the ergonomic ones #0
D#They want like the flashy lights #0
A#Yeah #0
D#Oh like this came from Las Vegas #0
A#Ones that ones that look high-tech  too #0
C#But at the same time are simple #0
D#Mm yeah #0
A#Right #0
A#So that people like my mother can use it #0
B#What about something with the curvature like that matches the curvature of a hand #0
A#Yeah #1
C##0
A#'Kay #0
A#Anybody have any experiences with remote controls that they can remember that Yeah #0
C#Just bad ones #0
C##0
A#That's true #0
A##0
C#Um #0
B#What kinda battery would we want to use #0
B#Because battery changing is usually Okay #0
C#D Double A_ #0
D#Double A_ #0
A#Do some of them use triple A_s though #0
D#Yeah some use triple A_s #0
C#Some but Yeah  I guess then it's If we need to do triple A_ we can  but most people usually have double A_s around #0
D#So double or triple #0
A#Okay #0
A#Okay #0
A#Yeah #0
A#But that has to do with the size of it too #0
C#Right #0
B#Yeah #0
A#Well  w as long as we know that issue is Here we can Triple A #0
D#Yeah  if we want it to be more thin  then we'd probably wanna go with a triple A_ #0
B#Can you with a small lithium battery #0
A#But it's okay  we don't have to decide about it now  just as long as we remember battery type and size is important #0
C##1
A#Hey #0
A#Anything else #0
A#Alright #0
A#Moving along #0
A#Oh  we're closing the meeting #0
A#Next meeting is gonna start in thirty minutes #0
A#Here's what we're going to do #0
A#Um the I_D_  which is who #0
A#Okay  you're going to think about the working design #0
A#What do you think that means #0
D##0
A##0
A#Okay #0
A#And U_I_D_  the technical fun functions design  making sure it does everything that we need the remote to do  the functionality of it  operating all those different things #0
C#Mm-hmm #0
A#Okay #0
A#And the marketing person  that's Courtney  is going to do the user requirements specification #0
A#I guess that means specifying um what exactly the user is going to be looking for #0
A#Right #0
D#Right #0
A#I would think so #0
A#Okay #0
A#And you're gonna get more specific instructions emailed to you in just a little while #0
A#Okay  so does anybody have anything they wanna say before we close the meeting #0
A#Okay #0
A#This meeting is officially over #1

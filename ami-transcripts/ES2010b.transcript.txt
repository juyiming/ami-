A#Okay #0
A#Um welcome to our second meeting #0
A#This is the functional design meeting #0
A#And I hope you all had a good individual working time #0
A#Okay  let's get started #0
A#Okay  here's the agenda for the meeting #0
A#After the opening um I am going to fulfil the role of secretary  take the meeting minutes #0
A#And we're gonna have three presentations  one from each of you #0
A#Then we're gonna discuss some new project requirements #0
A#Um gonna come to a decision on the functions of the remote control #0
A#And then we're gonna close up the meeting #0
A#And we're gonna do this all in about forty minutes #0
A#Okay #0
A#First I want to discuss the goals of this meeting #0
A#First we need to determine the user requirements and the question that we can ask ourselves is what needs and desires are to be fulfilled by this remote control #0
A#And then we're going to determine the technical functions  what is the effect of the apparatus  what actually is it supposed to do  what do people pick up the remote and use it for #0
A#And then lastly we're going to determine its working design  how exactly will it perform its functions  that's the whole technical side of 'Kay I'll just give you a minute  'cause it looks like you're making some notes #0
C##0
A#'Kay #0
A#Oh  well let's go ahead and  back  previous #1
C##0
C##0
C##0
A#So what I wanna do right now is hear from all three of you  on your research that you just did #0
A#Who would like to start us off #0
C#I don't mind going first #0
A#'Kay #0
A#Okay #0
A#Um do you have a PowerPoint or no #0
C#Yeah  it's in the should be in the m Project #0
A#Okay #0
D#Do you want us to do our PowerPoints now or I can go #0
C#You know you could you could do it yourself actually #0
A#Oh #0
B#Did you send it #0
A#Save it in the project documents #0
C#Put it in Project Documents  yeah #0
A#Okay #0
A#Mm-mm-mm #0
A#This one #0
C#Sure #0
C##0
A#Okay #0
C#Yeah #0
B#Okay #0
C#Okay #0
B#Great #0
C#Um well  the function of a remote control  as what uh we've been informed  is basically to send messages to the television set  for example  switch it on  switch it off  go to this channel  go to channel nine  turn the volume up  etcetera #0
C##0
C#Um some of the considerations is just um for example the what it needs to include it's the numbers  you know  zero to nine  so you can move to a channel  the power button on slash off  the channel going up and down  volume going up and down  and then mute  a mute function #0
C#And then functions for V_H_S_  D_V_D_  for example  play  rewind  fast-forward  stop  pause  enter #0
C#And enter would be for like  you know  the menus #0
C#And then other menus for D_V_ as well as T_V_  whether that means like um we can go and decide the brightness of the screen  things like that  all the more complicated functions of menus #0
C##0
C#And we can decide if that's what we want   um if we want to include that on the remote  if that's something that would stay on the T_V_ itself  for example #0
A#Okay #0
A#Okay #1
C#These are two examples #0
C#Um and you can see on the left  it's got a lot more buttons  and I don't know if you can read it  but it says  step  go to  freeze  um slow  repeat  program  mute  and so those are some of the buttons and so it gives you an idea of s one example #0
A##0
C#And then on the right  it's a lot more simpler  it's got volume  it's got the play the like circle set  which is play  rewind  but it's also what is fast-forward is also like next on a menu #0
C#So you have functions that are d uh duplicating #0
A#Right #0
C#And you have a mute button and then the numbers and the eject  and the power button #0
C#So that gives you two different kinds  a more complex and more simple version #0
C#Okay #0
A#Ready #1
C#And then lastly  it's just the questions that we want to consider like what functions do we want it to include  and how simple  complex it should be #0
C#And what functions it needs to complete #0
C#Uh  what are needed to complete insulation process  'cause  you know  that's something that also has to be considered and it's gonna be hopefully a one-time thing  when you set it up it should be set to go  but we have to include the functions that can allow it to set up i in the first place #0
C#So that's it #0
A#Alright #0
A#Very good presentation #0
A#Thank you #0
A#You speak with such authority on the matter #0
C#Mm #0
A#Okay #0
A##1
C#Left #0
A#Who would like to um follow that one up #0
A#Now  that we've discussed Okay #0
A#Do you want me to run it or you wanna Okay #0
D#Yeah  you should run it #0
A##0
A#Functional requirements #0
D##0
D#Mm yes #0
A#'Kay #0
A#Alright #0
A#Now we have Courtney with the functional requirements #0
D#Yes  okay so we tested a hundred subjects in our lab  and we just we watched them and we also made them fill out a questionnaire  and we found that the users are not typically happy with current remote controls #0
D##0
D#Seventy five percent think they're ugly #0
D#Eighty percent want they've are willing to spend more  which is good news for us um if we make it look fancier  and basically w we just need something that really I mean there's some other points up there  but they it needs to be snazzy and it but yet simple #0
D#So that's really what we need to do #0
C#Wait #0
D#And we need we need it to be simple  yet it needs to be high-tech looking #0
D#So Like They like I guess use the buttons a lot #0
C#And that meaning what #0
C#Okay #0
D#Yeah #0
D#I don't know #0
D#It's from my uh research #0
C#Right #0
A#Okay  what do you m Oh  I'm sorry #0
D#My team wasn't very clear #0
C#Only use ten percent of the buttons #0
D#That's okay #0
D##0
A#What do you mean by um the current remote controls do not match well with the operating behaviour of the user  like they have to press the buttons #0
D#I I think it's like the engineering versus user  whereas like the engineering she showed that the engineering ones are more complex and users don't really need all of the buttons that are contained on there  because they only use ten percent of the buttons really #0
A#Okay #0
A#Oh  right #0
A#The buttons #0
B##0
A#Okay #0
A#Yeah #0
A#Okay #0
B#We only use ten per cent of our brains #0
C##0
B##0
D#Good point #0
A#It works #0
C##0
D#It's a necessary evil #0
B##0
B#yeah #0
A#Ready for the next slide #0
A##0
D#Mm-hmm #0
D#And so people say that they typically lose it  as you yourself know  because you probably lose your remote control all the time  much like any small appliance like a cellphone  and they we need something simple  because most people  well thirty four percent say that it's just too much time to learn how to use a new one  and we don't want to go we don't want to vary too far from the normal standard remote  but I mean they do need to be able to identify it  and R_S_I_  I'm not very sure what that is #0
D##0
A#Hmm #0
C#Lost #0
C#S Wait  is that like your ergonomics like your hand movements or something #0
A#It's okay #0
A#It's very important #0
D#Yes  it is important for the remote control world #0
A##0
A##0
D#Sh Uh possibly #0
A#Could be  yeah #0
B#Do we really need t to provide more information on what R_S_I_ is #0
C#Like Channel  volume  power #0
C##0
A#Oh #0
D#Uh yeah  that's what my web site said  I don't know #0
A#I think that's a pretty good guess though #0
D#Yeah  I would assume so #0
C##0
D#I think we're supposed to know it as remote control experts #0
C#It's like if you're holding it to just say  where are you  and thing beeps  you know #0
A#Yeah #0
D#But also s so the channel  the volume and the power buttons are the most important on our company website you can find like the specific statistics concerning to how much each button is used  but those are the definitely the top ones #0
D##0
A#It's okay #0
A##0
A#Okay #0
A#Next slide #0
D#Yes #1
D#And so personally I think that we need a modern eye-catching design  but it it really needs to be simple #0
D#So saying from y your slide  your presentation  the engineering versus the user-specified remotes  I think that we should go with something that's more user-friendly #0
A#User-friendly #0
D#Where the engineering ones  the boxes  tend to make it look more complicated than it really is #0
D#Um the functionality of the product really needs to be considered as to like what type of buttons do we really need on it #0
D#And it needs to be open to a wide range of consumers  so even though we need a small number of buttons  we also need to take in like are most people going to be using it for a D_V_D_ player  a TiVo  what what exactly are we using it for  as well as the age range #0
D#So we need a hip  but not a corny marketing scheme for promoting our product #1
D#And also we found our team found that speech recognition is it's like an up-and-coming thing they really consumers are really interested in it  and since our findings found that people are willing to pay more money for a remote for it to be more high-class we could consider it #0
A#And so just to just to clarify by speech recognition you mean they would say  channel five  and the thing would go to channel five #0
D#I guess so  yeah #0
C##0
A#Okay #0
A##0
D#Yeah  I guess we can interpret it like  we can just try out different types of speech recognition within our remote programme #0
A#Oh  that'd be lovely #0
C##0
C#Yeah #0
A##0
A#Didn't they um didn't our rival companies manufacture a remote that you would press the button on the T_V_ and it would the remote would beep so if you have lost it Mm #0
C#It's kinda like what the remote phone used to do #0
A#Oh  yeah  that's true #0
C#You know like go to the base #0
A#We could definitely include that if we wanted to #0
C#Yeah #0
A#If it's within our price #1
A#Okay #0
A#Are we ready for our last presentation  Amber #0
B#Yeah  I'm just trying to move it #0
A#Okay #0
B#'Kay #0
B#I think it should be there  working design #0
C#Working design #0
A#There we go #0
C##0
A#'Kay #0
B#'Kay #0
B#Uh I didn't get a chance to complete this one  'cause some of the tools that I was given were frustrating #0
A#Oh my bad #0
A##0
D#Oh that's fine #0
B#Uh okay  so method method of our design  I think I just start listing th some of the things that we actually need to put into this #0
C#Help me #0
B#We need a power source  we're gonna need a smart chip if we're gonna make it multi-functional #0
B#Um extra functions will probably need an additional chip #0
B#Either that or the smart chip will have to be extremely smart #0
C##0
D#What exactly is a smart chip #0
B#Usually a smart chip is just a chip that's been programmed and designed so that it can complete a fair range of functions #0
C#Well  how much extra would the additional chip be #0
C#Is that gonna push us over our production costs #0
B#I wouldn't think so  'cause we could probably get it from like  in bulk  from a a newer company #0
C#Mm-hmm #0
B#And they tend to sell their chips pretty cheap #0
A#Okay #0
A#Ready #0
B#Um yep  nothing here #0
C##0
A#That's okay #0
B#Um power source  I figured  batteries  'cause they're easily available #0
B#Typically a remote has either two double A_s or four triple A_s  sometimes three #0
B#Uh it really kinda depends on the size of the actual remote itself #0
B#Um a large on-off button  demographically we're moving towards an older generation of people  so a large on-off button would probably be good #0
B##0
C##0
A#Hmm #0
B#Selection button for various entertainment devices  so you want something that will permit you to select the D_V_D_ player or the T_V_ or the stereo system #0
B#Um smart chip that perverts uh that permits  sorry  universal application again  something that'll allow us to skip over between devices  and that's kinda it #0
B#Uh this is my fifty second design #0
C##0
B#Power source over here #0
B#We're gonna have a switch obviously between the power source and the rest of it  and you're gonna need the switch #0
B#Um extra bulb could just be for flashiness  um subcomponent which would be like a way of diverting the power to different parts of the the device #0
B#Um the chip and of course the infra-red bulb  so it can communicate with the various devices that it needs to talk to #0
D#So what exactly we are looking at  is this like the front of the remote #0
B#This is just like a rough schematic #0
A#So this would be the front #0
B#So this is the internal workings #0
A#So the red would be the front of the remote though  right #0
D#Oh okay #0
B#Yeah #0
C#Yeah  that's gonna be what's communicating with the T_V_  but the other bulb  I think  is good to just to indicate  I'm doing something  it's sort of like a reassurance #0
A#Okay #0
A#The l the light up kind of Yeah #0
D#Like that we know the battery's working #0
A##0
B#Yeah  so you don't have to stare at that infra-red  'cause you know when the battery starts dying in your remote currently  you have to actually stare at that bulb and go  okay  when I push this button  is it working #0
A#Hmm #0
D#Okay #0
A#It'd probably be lighting up the key too  right #0
B#We can skip that whole thing #0
B#Yep #0
D#Yeah #0
C#Yeah #0
A#'Kay #0
B#So you can put it in the dark #0
A#The buttons #0
B##0
D#Yeah  and that's good #0
D#We should make it glow in the dark #0
A#Okay #0
A#Yeah  definitely #0
D##0
A#'Kay nex R Ready #0
B#Yeah  that's it #0
A#'Kay  any p 'Kay #0
C##0
D#Mm 'kay #1
A#Anything you wanna add for personal preferences though  you f you said already that we needed a large on-off button  you think #0
B#I think that that's a good idea  because you know that's one of the most important buttons #0
A#Anything else #0
C#Just Well  should it be larger buttons in general  you know like uh the examples that I had  they were swi quite small #0
A#Okay #0
C#So should we try and go for something that has l larger buttons #0
D#I think we should #0
D#Like I think that would be in a as in like in for the design  sorry  um #0
D#I think we should definitely go with buttons that don't look like a normal remote  'cause most remotes have small square buttons  I think we should do something like maybe bigger and round like bubbles #0
A#Mm #0
C#Ovals #0
C#Yeah  yeah #0
A#Okay #1
A#Okay  let's talk about all of our We'll come to decision later about all the components that we need to include  let's um wrap up this one  and I'm gonna go back to my PowerPoint  'cause we need to discuss the new project requirements which you might've already seen flashed up on the screen a bit earlier #0
A##0
A#Wait  come back #0
A##0
A#Alright #0
A#Sorry  let's go through this #0
A#Alright #0
A#Here we go #0
A#New product requirements #0
A#First it's only going to be a T_V_ remote #0
A#We're trying not to over-complicate things #0
A#So no D_V_D_  no TiVo  no stereo #0
B##0
B#Okay #0
A#It's not gonna be multi-functional #0
A#Hey #0
A#And we th need to promote our company more  so we need to somehow include our colour and our company slogan on the remote #0
A#We're trying to get our name out there in the world #0
A#Okay #0
A#And you know what teletext is #0
C#Yeah #0
A#in States we don't have it  but um it's like they just have this channel where just has news and weather  kind of sports  it's very um bland looking  it's just text on the screen  not even Yeah  just black with just text #0
C#I know #0
D#What is it #0
C##0
C#Yeah  it's like black  black and white kind of It'll give you the sports #0
D#Like running along the bottom #0
A#Yeah #0
B#You can also get the kind of the T_V_ guide so It's the entire screen is just running information at random #0
D#Wait  is it like the Weather Channel where it's got like the ticker running on the bottom or something #0
A#Kind of #0
C#Except the entire screen #0
A#Yeah it's the whole screen #0
C#You can pick sports  you can pick the news  you entertainment  you know it's like Yeah #0
A#So anyway Right #0
B#Seemingly #0
D#So it's like a separate channel from like what you're watching #0
A#Right #0
A#But it's becoming out-dated now  because of the Internet #0
A#Nobody needs to go to the teletext channel to check the news  and we have twenty four hour news channels now too  so Those are our new product requirements #0
A##0
D#Okay #0
B#Okay #0
B#So  do we have to include the company colour within that #0
A#Alright #0
A#Mm-hmm #0
A#Yes #0
A#It's part of the logo #0
A#Okay #0
C#Company colour being yellow #0
C##1
A#What we're going to do right now is come to some decisions  definitive that we can all agree on  about um the target group and the functions and just definite things that we need to do and then we'll close up the meeting #0
A#So #0
A#Alright #0
A#Whatever #0
A#Okay #0
A#So our target group is You mentioned um older people #0
C#Mm-hmm #0
A#Would it just be universal for everyone  you think #0
A#Because I think even if something has large buttons  as long as they are not childishly large  like even technically non-technically challenged people are gonna use it #0
C#It's gonna make it nicer #0
C#Yeah #0
A#I mean they want something user-friendly  so Yeah #0
B#Mm well  even if we kept the regular standard size of remote  if we reduced the buttons down to the ones that people are saying that they use the most often and a couple extra  'cause they're saying they only use ten per cent of them  then we should be able to accommodate fairly decent sized buttons #0
A#Yeah #0
A#Okay  so we want um for our target group would we say  I mean  young and old  all age ranges  all um  not kids obviously  right #0
A#Or kids #0
D#No  kids need to know how to use a remote  I would think #0
B#Most of them will intuitively pick it up though #0
D#They gotta change between Disney Channel  Cartoon Network #0
A#Okay  so we're going to go anywhere from kids to adult in the age range Um what about technic technical um specifications  like how how technically literate are these people who are going to be using our remote #0
D#Yeah  I think we need it all #0
D#Um I would say we should say dumber than the average person #0
C#We should go for the lowest denominator #0
A#Okay #0
D#Yeah #0
A#Right  okay #0
C#High school educated #0
A#So so they need no technical experience to operate Okay #0
B#how 'bout little to no  because there is no way that you are gonna be able to make it no #1
A#And we also need to determine the specific functions of this  just to get it all out on paper #0
A#So we said it needs to send messages to the T_V_  needs to change the channel  turn on and off  just basic simple stuff like this #0
A#So if you have something just say it and we'll add it to my meeting minutes #0
C#Well it's channel  on-off button  volume  mute #0
A#Mm-hmm  volume #0
D#And channel #0
D#Yeah #0
D#Those are the most important ones #0
A#Right #0
A#And we wanna keep um I'll make a note here that we wanna keep the number of buttons down #0
A#Correct  because people only use ten percent #0
D#Yeah #0
A#'Kay #0
A#Hey  what else #0
C#Um #0
A#Um #0
C##0
B#Do we want this thing to be able to be found easily #0
A#I think so #0
A#What do you A finding kind of device or Yeah  ho homing device #0
D#Sure  yeah #0
B#And like if this is gonna get lost underneath the coach  how are we going to accommodate the quick ability to find it #0
D#I need we we need a like homing device #0
C#Oh right yeah okay #0
A##0
A#Mm 'kay #0
C#Tracking #0
D#Because people really are looking for a remote that's more high-tech #0
A#Okay #0
A#Right #0
B#What if we gave it a charger #0
B#And on the charger  just like a phone  like you get a portable phone and it's got a charger  and if you d leave your phone somewhere  you push the button to find it  and it finds th the phone beeps for you #0
C#But you got a base #0
D#Do you think people'll really go for that though #0
D#Because Yeah #0
B#It's useful for the remote phone #0
A#Hmm #0
A#Would that add to our costs at all  I wonder #0
D#I would think so  because you'd have to develop a base #0
A#Right #0
C#Well  if you have the base  you could start putting in a charger and then you have a different kind of battery #0
B#Yeah #0
D#Hmm #0
B#Rechargeable batteries are cheaper usually #0
C#Yeah #0
A#Okay #0
A#I I think we can make a decision about that later #0
A#Uh we'll still put that as a point that we need to discuss #0
A#So that would include battery source Power source rather #0
A#Is it going to have a charger  or is it going to be run strictly off batteries #0
A#And we also need to deal with the issue you mentioned of speech recognition  if we want that #0
D#Right #0
D#Do w Well  th there's the people who desire speech recognition  there's the different demog demographics have different desires  I don't know if you guys ge It wouldn't copy onto the the thing 'cause it's black  but all the different age groups have different desires for speech recognition #0
C#Well  then we could If we have the speech recognition then we can start aiming at a like another kind of more handicapped disabled uh demo demographic #0
A#Mm #0
A#You could um we could hook it up #0
A#Oh #0
D#So basically older people don't really care #0
D##0
D#It's really the people twenty five to thirty five #0
D#I feel those are the people that really watch a lot of T_V_ though #0
A#Mm-hmm #0
D#They're the ones that get addicted to soap operas and just sitcoms and stuff #0
B##0
C#Well Well  then then do you put the voice recognition do you put the r like receiver on the actual television  in the base  or in the actual remote  'cause then you've already got remote in your hand  why you just gonna speak to the remote  whereas if you just speak in general and you don't have to have the remote in your hand and like talk at it #0
A#And if and if we introduced it when they're this age  they're going to probably always buy a remote that has Right #0
D#Right #0
D#So Right #0
A#Yeah #0
A#and the speech recognition could be part of the lost and found device  too #0
A#If we said  find remote  locate remote  or something #0
A#A certain phrase then it could beep #0
A#I dunno #0
A#Just throwing it out there #0
D#Yeah #1
A#Well Okay  anything else we wanna discuss #0
D##0
A##0
C#Still fifteen minutes #0
C#Um #0
C#Well  do we wanna include the numbers like zero through nine #0
C#Can we conceive of leaving them out #0
A#Um #0
D#Wait  on the remote itself #0
C#Yeah  like you have one  two  three  four  five  six  seven  eight  nine  zero #0
D#Well  we definitely need those #0
A#How how  yeah  how would you leave those out #0
B#Yeah #0
C#Well  I don't know  I mean  if you can like well I don't know  if there's just a way of leaving them out #0
A#Unless you could say the channel #0
B#I think people would find that too foreign #0
D#Yeah #0
A#Yeah  that's true #0
D#You definitely need Wait #0
A#And also remember that in this day in age we need  you know  like a hundred button  too #0
A#I used to have a remote that did not even go up past like fifty #0
A#So I couldn't whenever I got cable  I had to get a new T_V_ #0
A##0
B#It's when we get satellite #0
A#Mm #0
A#get your own remote  or digital cable #0
B#Yeah #0
A#'Kay #0
A#Um #0
A#I guess  we're gonna discuss the project financing later  making sure that we can fit all of the stuff that we want to on our budget #0
A#Um #0
B#Yeah  'cause I don't have any material pricing information available to me at the moment  so R_ the double R_ #0
A#'Kay #0
A#And don't forget we need to include the colour of our company and the logo #0
C#The colour being yellow #0
A#I'm guessing #0
C#And how do we Can't make it entirely Well if you have like a Hang on #0
D#I feel like a ye I feel like a yellow one would be too garish #0
A#And the R_R_ #0
A#We could just have the logo in yellow  or maybe a yellow light for the keys #0
B#Or is the l Yeah  yellow could be and it could doesn't have to be huge #0
D#Or put like stripes  oh yeah  yellow lights #0
B##0
D#Yeah #0
C##0
C#If you have this sort of strip kind of down at the bottom the base of it  just like yellow with the R_R_ #0
A#Right #0
A#So we've simplified  we don't need all those um play  fast-forward  rewind  or no menu buttons #0
C#Right  yeah #0
D#Yeah #0
A#So we've pretty much pared it down to on-off  volume  mute  channel up and down  um the numbers Yeah #0
A##0
C##0
D#Yeah #0
A##0
A#Um can we go back to I'm gonna look really quickly back at those examples and see if there is anything #0
C#Two examples #0
C#Yeah #0
A#Which one is yours  technical functions or functional requirement #0
C#Oh  it's a Yeah #0
A#Okay #0
D#Yeah  audi audio settings and screen settings  we need those like audio settings mono  stereo  pitch  screen settings like brightness  colour  or do we just want that accessed accessed from the television itself #0
A#The T_V_ #0
A#I think that that's fine just for the T_V_ #0
A#I mean how often does the average user need to do that kind of stuff #0
C#Well  the other option is sort of like down at the bottom  like farther away  you just have this sort of box inset where it's like the buttons that you don't use as much  but occasionally you will use #0
A#Hmm #0
D#Yeah  'cause we need to we definitely need to have buttons for like sub-titles and things like that #0
C#and so it's like I don't well  I don't know #0
D#It's 'cause the foreign film market is expanding and stuff  and like on television like I know f k living in Los Angeles it's tons of Spanish network television if it has English sub-titles it's definitely helpful #0
A#Couldn't we do that all through one button  something  a menu button  that pops up with a menu on the T_V_ that says  you know  audio  video  whatever  language  you know #0
C#Right #0
D#So we need up  down  and side-to-side buttons #0
C#Well  that could be No you could just double up with like the channel or the volume buttons #0
A#For the menus #0
B#Mm-hmm #0
A#That's true #0
D#Yeah  okay #0
C#Channel is just up and down #0
D#Okay  yeah #0
C#And then add a Yeah #0
A#Something that looks mayb you know #0
D#Such as  yeah  the one the one over there on the left the engineering centred one #0
A#Y right  right right right #0
A#That one #0
C#So we just have it like add a menu button then for the various things needed  including v voice recognition if we have any like settings for voice recognition now included in the menu #0
A#Right #0
A#In the middle perhaps #0
A#Yep #0
D#Ooh  I just got an idea for a design #0
A#good #0
A#Anybody have anything else they'd like to bring up in this meeting #0
B#I had something  but I forgot #0
A##1
A#Okay #0
A#get out of here #0
A#Let's go back to the meeting closure then and see what we need to do next #0
A#Mm #0
A#Alright #0
A#After this meeting we're gonna be sent a questionnaire and summary again which we need to reply to that e-mail #0
A#And then we're gonna have lunch break #0
A#And after lunch thirty minutes of individual work time #0
A#Um I'm gonna put the minutes I put the minutes for the first meeting already in the project documents folder  if you'd like to review them #0
A#And I'm gonna type up the minutes for this one as well #0
A#Um here's what we're each going to do #0
A#The I_D_ is going to work on the components concept  um U_I_D_ the user interface concept  and you're going to do some trend watching #0
A#'Kay #0
A#Specific instructions will be sent to you by your personal coach #0
A#And if anybody has anything they would like to add #0
A#No #0
A#Okay  well  this meeting is officially over #0
A#Thank you all #1

A#So we can start #0
D#Yeah #0
A#Suppose I have to do my presentation #0
C#Ah okay #1
A#Eh um So  I'll present myself  I'm Ada Longmund  and as you may know it  I'm the pr project manager #0
A##0
C#It's Ada Longmund #0
A##0
A##0
A#So um we will have to um speak about m the project #0
A#Our project project is to create um a new remote control and as you may know there's lot of industrials interesting in creating a remote control  so the remote control has to be original  trendy and um user-friendly #0
A#Record #0
A#So the project method is the following #0
A#So if we're um the functional design  you have to do uh any individual work and uh also work with uh meetings talking with each other #0
A#Uh it will be the same for the conceptual design and also the same for the detailed design #0
A#Uh #0
D##0
A#The tool training is to try out the white board  so Maybe you can draw your favourite animal and make a list of its favourite characteristics #0
D#Mm-hmm #0
C#Maybe someone ha we have to this whiteboard  yeah #0
D##0
C##0
D#So right now #0
A#I don't know if we have to do it now  maybe later later #0
B#So yeah I think you can do it #0
C#Yeah  I don't know #0
B##0
B##0
A#So the selling price of the product will be twenty five Euros #0
B#Twenty five Euros #0
D#Mm #0
A#Yeah #0
A##0
A#Yeah #0
A#I think it's quite good price  yeah #0
A##0
C#I it's it's reasonable  s quite yeah #0
D#It's reasonable  I think  yeah #0
C#Twenty five #0
B##0
A#And uh it will uh be a an international remote control  as we want to sell it in the entire world  and the product costs will be not more than twelve Euros and fifty centimes #0
C#Is Yeah #0
A#So  as you will discuss about the remote control you will have to experience your um with the remote control #0
A##0
A#Um just uh maybe be imaginative with remote const con controls  try to create something new and people would like to to buy #0
D##0
C##0
A##0
A#And and the next meeting will start in thirty minutes  so you'll ha all have your spethisfispis specif specific role and you know I suppose you know what you have to do #0
A##0
C##0
A#And uh you will have to work on the design and also to work on the design of th technical fun functions of the remote control and think of the user requirement specifications #0
A##0
C#I I hope so I_D_ is for the Industrial Design  yeah #0
C##0
D#Those things just refer to each of each of us  I think #0
B#Yep #0
A#Yeah #0
D#AMI and okay #0
A#I_D_  yeah #0
B#Yeah #0
A#So So I will manage all all the group #0
D#Okay #0
C#And U_I_D_  it's for the User Interface Designer  yeah #0
B#That's me #0
D#Okay  and Marketing Expert  it's me #0
C#AMI yeah project #0
C##0
C#you will be the manager yeah #0
D#You can manage all this  yeah #0
C##0
C##0
B#Okay #0
D#Good #0
A#So you have questions #0
D#Um #0
D#Not really #0
A##0
B##0
A#So you all know what the parts of the work you have to do #0
B#Yeah #0
D#So which you  the Industrial Designer #0
B#No I'm user interf I'm user interface design #0
C#I am the Industrial Design  yeah I am the Industrial Designer so #0
D#Okay #0
A#Mm-hmm #0
A#And you #0
A#Mm okay #0
B#Okay #0
D#Yeah #0
B#So what's the difference between user interface design d industrial design #0
C##0
A#Hmm #0
D#I mean  you have to know #0
A##0
D#Ah  you have to know it #0
C##0
C#It's difficult #0
B##0
D#It's your job  so I hope you you know what it is #0
C##0
C#You know very soon #0
C##0
B#Yeah  I think so #0
C#So I I think the user the user interface design is he will design how the user will you know the relation between the user and you know the remote control so And the uh industrial design  it is how the object will look like #0
D#Mm #0
A#I suppose you have to design it and you have to take care of the industrial way to transform it #0
B#Okay  so I make uh u user interface #0
D#Yeah #0
B#You you de you implement the core functions in the Use it #0
A#And i maybe you will transform it #0
A##0
B#Make make yeah #0
B#Maybe I think uh uh i industrial design's uh  it's the function design #0
C#Yeah #0
C#So the materi Okay right #0
B#I design the user f user interface  you design the function #0
A#Maybe  it is the outside and the inside #0
B#Yeah #0
D##0
D#Okay #0
B#Okay #0
C#But I was thinking that he's a user 'cause the user interface to design for example where the but button will be  you know #0
B#Yeah yeah #0
C#But I don't know #0
C#Okay #0
C##0
D#Well #0
D#You know #0
C#I'm the industrial designer #0
D#Oh  okay  okay #0
D#Not the other one #0
C#So #0
B#Okay #0
C#Okay #0
C#So and the marketing expert will Ok Okay #1
D#Yeah  I'm just go I'm trying to give you some trends about what should be done and what the users would like to have and then thi this would I guess converged to the User Interface Designer wi and then Industrial Designer #0
A#And yeah #0
D##0
A#And when designing y the remote control just remember that uh it has to be a kind of international product #0
B#Okay #0
A#So you don't have to do something really specific  as everybody everybody will have to use it  it's sor the same as keyboards #0
B#Mm-hmm #0
D#Mm #0
B#Yeah #0
A#You know  you have Qwerty  Azerty  French and U_K_ keyboard  so really the remote control to be international #0
B#Mm #0
B#Yeah #0
B#Okay #0
C##0
D#Mm-hmm #0
A#And not too expensive #0
B#Yeah #0
B#And uh simple #0
A#As we want to maximise the benefit #0
B#And easy to use #0
D#And you have to keep it under twelve Euros and f fifty  so #0
C##0
A#Yeah  you have to keep in mind that the product cost won't be maxim more than twelve dot fifty Euros #0
B#Ah  yeah #0
D#That's the problem #0
C#It should be There was a step about drawing something in the in the board  I don't know #0
B#Okay #0
B#Okay #0
A#And to be sure that really people will be interested in buying a new remote control with maybe new functionalities that don't exist in the mm existing remote controls now #0
C##0
B#Okay #0
B##0
A#So  is it okay #0
D#Mm #0
B#Yeah #1
D#It's clear #0
C##0
D#Yeah  maybe should go and draw an animal #0
A##0
C#Is it #0
C#Are we supposed to do right now #0
C##0
B#Yeah yeah  you try #0
C##0
B#Try first #0
C#Oh right it's it's from the left to the ri It's I d I was thinking but I n I'm not sure now #0
B##0
B##0
C##0
A#So you think we have to do it now #0
C##0
C##0
B#You can draw something which is very simple #0
B##0
C#Oh Everybody I think everybody should do it  so #0
A#You want me to draw something #0
A#Product manager okay  let's go  I will try #0
C##0
C##0
B#Oh  maybe we should bring Kemy here #0
B#Kemy is really good at drawing #0
D#Many Yeah #0
C##0
B##0
C##0
C#It's not matter So #0
C##0
A##0
C##0
B#You're going to draw #0
C##0
B##0
B#Okay #0
D#Uh it's the same as mine #0
C##0
B##0
C#yeah #0
C#It's a It's a cat #0
B#What's this #0
C##0
B#It's a fat cat #0
C##0
C#It is not a fat cat #0
B##0
B##0
C##0
D#It's the fat cat  okay #0
C#Yeah  it is a Yes you have to draw a rat if you want a rat #0
B#Can you draw uh um rabbit #0
C##0
C##0
B#Oh  hat ha rat #0
D#A rat #0
B#Yeah #0
D#That's difficult #0
A#No #0
A#A mouse is not too difficult #0
C#It's your rat #0
C##0
B##0
A#Mouse is okay #0
C##0
B#Yeah  it's okay #0
D#Yeah #0
D#Just go  you you the closest to the whiteboard #0
C#Okay  go right  but in grow  it's everybody has to grow What are you I don no idea  so It's a rabbit #0
B#Mm #0
D#Jus Yeah #0
B#Okay #0
C##0
B#Oh #0
B#Okay  I draw #0
B#The only thing I can draw is like this #0
B#Oh #0
B#Oh #0
B#Oh #0
D#A duck #0
B#No #0
A##0
B#What's this #0
B##0
C##0
A#You love the eyes #0
C##0
B##0
A#Yeah  that was the eyes #0
B#Okay #0
A#A clown #0
B##0
A#Rabbit #0
B##0
D#Pikachu #0
A##0
D##0
C##0
B##0
B##0
A#It's a rabbit #0
D##0
D#Oh yeah #0
D#Bugs Bunny one #0
A#Yeah #0
B##0
B#Okay #0
C#It's not so bad so #0
B#The only thing I can draw  because it's very simple #0
C##0
D#Okay #0
B##0
D#I go #0
B##0
A##0
B##0
D#What #0
D#Oh #0
D#So what else #0
D#This was my favourite one  but Right #0
C##0
B##0
C#So you don't have a A fish #0
A##0
A#Thank you #0
C##0
A##0
D#A fish #0
A##0
C##0
C#That's a that's a fish #0
C#Okay  let's try to draw something #0
B##0
A#You forgot the chips #0
B##0
D#Oh yeah  doesn't look so fine #0
A##0
C#Have to be really careful #0
A#Fish and chips #0
D##0
D#Okay  it's your turn #0
A##0
C#Ah it's my turn #0
B#Oh #0
C##0
B#Okay  be careful #0
C#Okay #0
C#So #0
B##0
A#Of Oh #0
B#No problem  no problem #0
C#It's ok So  what can I draw some more #0
C##0
A##0
C##0
C##0
C#No #0
C#Mm Yeah  it's it's a se it's my priority this one #0
B##0
C##0
A##0
C##0
B##0
B##0
B##0
C#Yeah #0
D##0
B##0
D#Mm #0
B#A person #0
C##0
A##0
C#No #0
B##0
C#It's a really crazy dog Okay #0
A##0
B##0
C##0
D##0
B#Dog #0
A#Oh yeah #0
A##0
C##0
B##0
D#Good #0
C#Transfer #0
B##0
C#It's a dog in a village #0
B#Okay #0
C##0
C##0
B##0
B##0
C#So what are you sug going to do now #0
D#I think it's done #0
A#Yeah  I think yeah #0
C#It's done #0
D#Yeah #0
B#Okay #0
A#Just have to present project  discuss a little bit about it #0
B#So we have break #0
C#Oh my God #0
D#Oh  we have twenty five minutes for the meeting #0
A#Yeah #0
D#Okay #0
A#So  if you have questions #0
B#Oh #0
B#Hmm #0
C##0
D#Know what time is it #0
D#No #0
B#No #0
B##0
A#It's okay #0
C#Yeah  it's okay #0
A#You know your job #0
A#you know your job #0
C##0
C#We have an idea yeah #0
A#You know your job #0
C#I have an idea of my job so yeah so #0
C##0
C##0
A#Okay #0
C#Yeah #0
B#Okay #0
D#Good #1

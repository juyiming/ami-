A#So I hope you're ready for this uh functional design meeting #0
D#So Of course #0
D##0
A#Um so I will take the minutes you mm you three are going to do presentation #0
A#Um uh we want to know to at the end to know the new project's requirement so we need uh to know the the user uh needs that we want to fulfil to fulfil the from the technical part we want to know how it going to work and um third part uh I don't remember which is not very good #0
D##0
A##0
D##0
A##0
A#Ah of course  how to to design this uh this yeah #0
D#Nice stuff Who starts #0
A##0
D##0
A#So um let's go for the three presentations  so first um Marketing Expert #0
D##0
D##0
D#Oh #0
D#Ha #1
D#okay #0
A#So wait a minute #0
D#So I dunno if I can do that like this #0
A#Mm #0
D#Yeah #0
D#So it's being modified #0
D#Do you want yeah  open #0
D#Read only #0
D#I hope I saved it #0
C##0
D#So  um yeah  this is my name #0
D##0
B#Sammy Benjo #0
B#I know this name uh #0
A##0
D##0
A#Sounds uh Mm uh #0
B#We met before #0
B##0
D#So as you know  you I think you already know me  Sammy Benjo #0
D#I am the expert in marketing and I want to tell you about what people uh s want and uh like and dislike in remote controls  and I hope this is going to help you to to design it correctly #0
D#So next please #0
D#Uh-oh #0
D##0
C#Yeah  it is put F_ five #0
A##0
D##0
A#Hmm #0
D#Hmm #0
C#The full page presentation  yep #0
D#Yeah maybe in the full page because i I spent lots of time doing this presentation  so #0
B#F_ F_ five #0
A#Okay #0
C#Yeah #0
C#Yep #0
A#F_ five #0
C#Yeah #0
D#Uh-huh hmm okay #0
A#Mm-hmm #0
A#Mm #1
D#So basically uh what I suggest is that uh instead of deciding ourself what what could be and what should be a good uh remote control  let's ask people who are users of remote controls how they feel about w the current remote controls  what they like  what they don't like and um and what they do with them by the way because they are supposed to be useful #0
D##0
C##0
D##0
D#Don't forget about that #0
D#So we've we've conducted a a survey on on the use of uh remote controls and I'd like to show you some of the results we found on this survey #0
D#And next please #0
D#Yeah  so basically what we found was that uh there are several things that the user don't like in remote controls #0
D#First of all  they find it very ugly #0
D#Current remote controls as you know they're the same as this one uh they're not nice colour  not nice shape  I mean they're all the same  and they're not l good looking #0
D##0
D#Um what is interesting is that in fact it seems that they were people are ready to pay for nice and look and fancy looking uh remote control  so I think we should probably spend lots of time in and effort in that um #0
D#And the other thing is that uh the the current remote controls are not so easy to use and it it the the current uh facilities that they offer do not match what people really want to use their remote controls #0
D#For instance uh we see that uh they zap very often so I think this is a very uh important uh functionality that it should be easy for them to to zap uh in one way or another #0
D#And most of the buttons uh on uh current remote controls are not used  so I think we should design something where some of the buttons which are those that are used should be easier to see and use than others that only a couple of people are using #1
D#Um next please #0
D#Now people are very frustrated w with their remote controls and they for instance uh they don't even find it it's it's often lost somewhere in the in the in your home and nobody knows where it is #0
D##0
D##0
C##0
D##0
C##0
A##0
D##0
D##0
C#Yeah #0
D#Maybe if we have something where we could ask the remote control please  where are you #0
B#Agree #0
C#Yeah #0
D##0
A##0
C##0
D#Like uh something to to like t I think phones #0
D##0
C#Yeah #0
D#Some of the phones have some of this kind of s functionality #0
D#Uh of course phone you can always phone your phone but you can't phone your your remote control #0
A##0
C##0
D##0
C#You can you are #0
D##0
A#Why not #0
D#But why not #0
D#Yeah #0
C##1
A##0
D#And because of the fact that there are so many buttons in these remote controls that nobody use  in fact they don't even know how to use them  so most of the the people say they they don't know how to they to use properly their r remote controls #0
C#Hmm #0
D#And uh they are bad for R_S_I_ but uh I don't remember what is R_S_I_ #0
C##0
A##0
A##0
D#So I think they are bad #0
D##0
C##0
B##0
A#Okay uh tha that's look great #0
B#R_S_I_ mm-hmm #0
C##0
D#Mm-hmm #0
D##0
D#Mm nobody has any idea about that #0
D#Well I'll check uh with my Oh  okay  I think it's a technical thing which our Okay #0
C#Yeah  it's electromagnetic waves or something kind of maybe uh effect #0
B#No  I don't think so #0
C#Yeah  because infrared uses some electromagnetic technology  and those waves have high Uh #0
A#Mm-hmm #0
A#Okay #0
B#Okay #0
D#So  it seems that it's a lot of people for a concept that we don't know but we have to take this into account #0
B#But twenty six percent  do you know Twenty five #0
A##0
D##0
A#Or something we don't know #0
D##0
C#Uh #0
B##0
B#Every fourth  you know #0
A#Mm-hmm #0
A##0
D##0
C#Yeah it's People really Yeah #0
A##0
B#Every four some of us knows #0
B##0
A##0
C##0
A#Okay #0
D#So anyway that's for what the biggest frustration uh of the user and um what else do I have #0
B#One of us Mm-hmm #0
C#Yeah #1
D#Next slide #0
D#Ah yeah #0
D#So we've listed a couple of uh s uh functions that may be uh used by u the user in the current uh available uh remote controls and uh well the tables look very nice to read but what is important is to understand that the power button is not used often because in general you use it only once per session  but it is very relevant #0
B#Functions #0
D##0
A##0
D##0
D#People want to have a power button #0
D#Channel selection is uh o often used very often used and indeed uh very relevant #0
D##0
A##1
D#Ah now I remember what is R_S_I_ it's repetitivity stress injury #0
D##0
A##0
D##0
D##0
A##0
D#We have to be careful with that word but uh anyway I continue my presentation so yeah  channel selection is um very important  very often used #0
C#Uh #0
D##0
B##0
D#Volume is not often used but people uh want to have control on volume and that makes sense of course #0
D#And n then you have things which are very much less often used like the settings #0
D#Audio settings  screen settings  even teletext and channel settings #0
D#All of them #0
D#they're not often used and they are s more or less relevant #0
D#It seems that people find teletext teletext uh relevant  even if I personally never use it but seems that it's average relevant at least  so #0
A#I have been told that we uh don't consider teletext  that it's out of date now because of internet #0
D#I can tell you that uh in a l in a scale between one and ten relevant uh not relevant to relevant people scored a six on this  which is not as uh these these two one were had I think ten I think #0
A#Mm-hmm #0
D#But but if you compare with these ones  uh I think they scored a one or two #0
D#Not very relevant  so if if there are good reason not to put teletext it's okay but just know that people find it somehow relevant #0
C#Hmm #0
A#Mm-hmm Mm-mm #0
B#Mm-hmm #0
A##0
D#That's for the main functions I think and uh then we can ask uh ourself uh what people don't have that may be useful #0
D#For instance I think net next slide #0
D##0
D#One of the thing the trend uh that uh you are probably aware of is the possibility the eventual possibility of having speech recognition in your remote control  so you wouldn't have to tap tap in your buttons but just tell your remote control or whatever you need you have what you want #0
D#So we've conducted a survey about uh whether people would like or not to have uh this kind of uh functionality in their remote control and as we can see it really depends on the age #0
D#Young people  probably because it's a buzz word  find it very relevant #0
D#And uh as the age goes up the the relevance goes down #0
C##0
D##0
D##0
C#Yeah #0
D#So now it really depends on the kind of uh targeting uh wha who are we targeting with this remote control #0
D##0
D#I think if we are targeting young people then uh it's probably something we have to consider #0
C#'Cause Thank you #0
D#If we are targeting you very old people this is something they really don't know why they this should be so now this is of course  depends on that #0
A#Okay #1
D#And um I don't have any conclusion  I didn't have time the meeting was very tight  so that's basically my findings #0
D#And uh  if you have any question #0
A#Mm I think it's good  okay #0
A#You done a good review #0
B#I got one question  uh you are a Market Expert so should we aim at the young people or not #0
D#I can go back #0
D#Yeah one question  yeah #0
D#I am #0
D##0
D#I think we should aim at the young people #0
D#But uh I think they are they are those uh who might be more interested in a in a new device #0
C#Mm #0
D#In general the the early adopters of a new device are young people  less than more than Then teletext is useless for them I think  yeah #0
B#Okay  then teletext is used less #0
C#Mm #0
A#Mm-hmm #0
B#Okay #0
D#Because they they have other means of finding their information #0
B#Mm-hmm #0
A#Mm-hmm mm mm #0
B#Mm-hmm #0
D#Yeah #0
A#Okay #0
D#But yeah #0
A#That's good point #0
C#Mm  yep #0
B#Mm #0
B#Okay #0
D#Nope #0
D#'Kay #0
A#Okay #1
C#Thank you #1
A#So um now I think it's the turn of the the I'm not sure um Of the technical function  so uh what effect Okay #0
D##0
A##0
A##0
D#So I think it's you  huh #0
C#Uh it's techni function of Yeah #0
D#No #0
D##0
B##0
B#That's me #0
D#No  user requiremen Makes sense #0
A#Wait a second #0
D##0
C#I have to do working design so uh #0
A#Argh #0
A#So you're Okay #0
B#That's but this but number three  yes #0
B#Mm-hmm #1
B#So  my name is Mark Dwight  and um I am responsible for User Interface Design #0
D##0
B#However  uh mm Project Manager asked me to give you some presentation about technical functions design #0
B#Uh  as I'm a more an artist that's gonna be less technical functions but more User Interface and current intentions and everything which is linked with this #0
D##0
A##0
B#So next slide please #0
A#Let's go #0
D##0
B#And uh a general method which is seems to be very useful for our task is not to forget about uh Occam razor #0
C#Mm #0
B#We should never complicate things too much #0
B#We should only make a remote control  nothing more #0
D##0
B#Nothing more than this  just a remote control #0
A##0
B#'Cause current remote controls they are never easy enough to use #0
D##0
B#So  make a click  please #0
B#So here is this remote control #0
D##0
A##0
B#It's quite a standard one  but it's not from a T_V_  it's from a much easier device like air conditioning or something #0
B#But you know  we can use it for a T_V_ easily #0
B#Only buttons we need is on off  volume  channels and maybe some options or something else  and please make a click  compared to this one which one would you prefer #0
D##0
C##0
C#It's Yeah  yeah #0
B#I guess this #0
D#I would say the simplest one as long as there are the uh I find the buttons that I need every time I need a button #0
B#Sure  sure #0
C#Yeah #0
D##1
C#Maybe it can be yeah middle of like  between those two li S you should yeah #0
B#Yeah  and our method is going to be  provide simple simple desires into simple actions #0
A#Oh sorry #0
A##0
D#Nice #0
D#Nice sentence #0
B#Findings #0
D##0
A#Okay #0
A#Oh sorry #0
B#Our question of the style  we should remember that our company puts fashion into electronics and we should never forget about it #0
D##0
D##0
A##0
B#Concept #0
C##0
B#Be simple #0
B##0
B#Be simple and you'll lean on this market #0
C##0
B#Market is a of remote controls you know it better  it's very well  it's it's not an easy field to to play  you know #0
D#Mm #0
B#So be simple #0
D##0
B#For personal preferences I think that to make a baby-proof remote control it got to be a titanium #0
D##0
A##0
D##0
C##0
B#It's a really good style  it going to be look like like this #0
B#It is unbreakable and it is very universal #0
B#W we'll have a screen with a back light which can change colours  and we can put all the options into this screen #0
D#Mm-hmm #0
B#We'll need only few buttons #0
B#All the other things can be controlled through the screen #1
B#And all these buttons should be easy to find and to click  'cause when you watch a movie and you want to change something  you always try to find a good button and click it  but you should do it by touching it and finding it easily just by touch #0
B#So Press I would propose this concept for design  just few buttons  a screen with a back light which can change colours  titanium I think  and uh what else #0
C#Mm #0
B#I got just very few and good ideas #0
B#We need power and volume #0
B#And let us include two nice features into this device  first  power on and off can be made fully automatic #0
B#When you go to the sofa  take your control and point it to the T_V_  the T_V_ turns on #0
A#It's off #0
D#Hmm #0
A#It's on #0
D#And when does it turn off #0
B#When you don't touch the control but you go out of the For for enough time like uh you It's a question to our technical design  our two engineers #0
D#Oh so you have a sensing sensor machine that uh knows Tech Mm-hmm #0
A##0
D##0
B##0
C##0
B#And another nice feature that I would like to implement is uh volume control #0
B#Suppose you set u you set up some volume and then you move out or you move to the other corner of the room and take your control with you #0
B#Like  you want to to change the chair or you want to move to the armchair from the sofa or something  and then the volume changes #0
A#Or you want to go to the kitchen #0
A##0
B#It's easy to do  you just control the According to the distance #0
D#According to your distance to and the angle maybe  if you have a stereo system #0
C#Distance #0
B#Yeah yeah yeah #1
D#Uh I'm not sure about the screen  wha what is the use usefulness of the screen #0
B#So I think it can be just a menu which can be controlled with a left  right  up  down and enter #0
D#Uh is it a touch screen by the way #0
D#So it gives instructions but uh it has to be with an back light somehow #0
C#Yeah #0
B#So  its main purpose in fact is a back light  which change colours  which makes it easier to find  and each can it can respond for your voice  like it can turn on the light for you just to f find it easily  yeah #0
C#Mm-hmm #0
D#Okay #0
D#Okay #0
A#Mm #0
B#So basically that's it #1
A##0
A#Um I see that you target uh several s application not only T_V_ but i like we talk about um universal uh remote control #0
B#Can be easily done  'cause you got simple designs  y we should put it to simple actions #0
A#Yeah #0
A#Hmm #0
B#Let it be universal  so you want to use it for your hi-fi system #0
B#You want to change tracks and you want to adjust volume #0
A#Mm-hmm #0
A##0
B#Just few actions  a few actions for everything #0
C#Mm #0
A#Hmm #0
A#S Mm #0
B#All the rest  we sh we'll put it into this menu on the screen #0
A#Since we were targeting a really soon uh uh date for the the the um i issuing of this uh remote control I think we will only concentrate on T_V_ for the moment and then maybe m make it more generalised yeah #0
C#Mm #0
B#Okay  okay #0
C#Yeah and it mm #0
B#Okay  but it's quite universal you know #0
C#Mm #0
B#We can just extend it to any device #0
C#Mm #0
D#So for instance if I want to go to directly to channel twenty five  how would I do can I do that with this #0
B#Uh twenty five #0
D#Yeah mm let's say I am uh on channel eight now #0
D#You know these days we have hundreds of channels  that's not so easy to go just next next next when you have hundreds of channels #0
B#Mm-hmm #0
B##0
B#In fact I would propose another solution #0
D#Or is it #0
B#Basically you use just four or five channels  right #0
D#Most people yeah #0
B#Yeah #0
B#So uh set up your T_V_ set like channels that you use they're one  two  three and five  and you will never have to go to a twenty fives channel #0
D#In fact in in one uh remote control that I've seen  instead of doing that d you could just say these are the cha ch channel three  twenty eight  forty eight and uh sixty four are those that I want to by cycled with my next button #0
A#Yeah it's it's the same solution  I think #0
D#I uh B yeah #0
C#Yeah #0
A#Hmm #0
C#But even we can have some uh L_C_D_ display  like you can uh de you can just button the number and then it go t because Yeah #0
D#Go to channel twenty five #0
D#One thing is that as I said in my presentation people really do like to z zap #0
D#So even if they are only watching four or five channels  I think they want to zap out of the one hundred channels  just because this is one kind of thing they do  zapping #0
B#Mm-hmm #0
C#But still Yeah #0
A#Yeah uh on zap it's only next next next next next  yeah #0
C#Yeah #0
D#And it's only next #0
C#Yeah #0
D#Yeah so but you have to Okay #0
B#Mm #0
A#Mm-hmm #0
B#We got these buttons here #0
B#Next next #0
C#Yeah. #0
B#Or say this can be back #0
C#Yeah #0
C#But otherwise like we can put some display on numbers and then they can just press suppose two five they just press two and five and then Yeah #0
D#So it would be Okay #0
A#Maybe we can make uh different modes for each button and you can change mode zapping mode or uh current chan Yeah #0
C#Yeah #0
C#Yeah  yeah #0
D#Mm-hmm #0
D#Alright #0
C#Yeah  yeah #0
C#Yeah but since we are focusing only on T_V_ remote controls so we can have more functions for T_V_ uh if you want to go for a universal then we ought to limit for functions for each of our devices #0
D#Listening more #0
D##0
D#Mm-hmm #0
D##0
A#Mm-hmm #0
D#Okay #0
B#Could we carry out some research if we w really need this  like how many people really need to go to channel number twenty five and then to sixty four #0
D#Well I could could uh have a look at that maybe #0
B##0
A##0
C#Yeah #0
C#Yeah #0
D##0
B#Okay #0
D#I'll check in my department if there's someone specialist in that #0
D##0
B##0
C##0
A##0
D##0
B#Okay #0
D#Although I don't know #0
A#Okay #0
B#Alright #0
D##1
B#Thanks for your attention #0
A#Uh you're finish #0
A#Okay #1
A#So now the technical aspects of this new device #0
A##0
A#Mm #0
C#Two #0
C#Yeah  if Sorry #0
A#You prefer it #0
A##0
C#Yeah #1
C#Uh as you know  I am mister Ramaro #0
C#I am an expert in uh industrial design of all electronic devices and I previously devised many uh like digital calculators and electronic calculators #0
C#So now I'll briefly describe the working design of our remote control #0
C##0
C#Well  as you know the basic function of remote control is sending some message to the the device  like T_V_ or V_C_R_ or D_V_D_ player etcetera #0
C#So we will have a p portable device which will send message to the the main device like T_V_ #0
C#So we need to have some energy source to do what to do the functions  what we want on this portable device #0
C##0
C#And usually this so to do these functions we need an interface  which basically some kind of pressing buttons or keys or like moving jack or something like that #0
C#And then these messages these key buttons can be transferred into some kind of message and then it will process by the chip and then it will generate some information to the main device #0
C#It's generally in the form of infrared or some kind of s sensor information #0
C#Then we will have the main control in the main device to do the particular action what we want #0
C#So  basically we need uh since we are focusing on our interface device remote control we need few components  mainly the energy source like the battery and then we have user interface like uh the keypad and you know buttons we want  and then we have some chip  it's mainly digital signal processing chip because since we are I am one doing mostly digital uh devices we ought to have some kind of processor which take care of all these functions and put it in some digital format #0
C#And then we'll have the infrared L_E_D_ source which sends the information to the main device #0
C#Then we'll have switch in our main uh um device to do particular operations  and we ought to do different codes for different T_V_s  so some T_V_s will have different encryption codes for doing s s uh channel changing and these things #0
A#Mm-hmm #0
A#Okay #0
A#To make it quite uh an universal uh device uh #0
C#Yeah yeah  because the people don't use one particular brand so or at least we have more more than five brands  which are really good #0
A#Mm #0
A#Okay #0
C#So we need to check their specifications and do their uh encryption that's passing information to the T_V_ device #0
C#So we need to have particular encryption codes #0
A#Okay #0
C#Then  components  so we have the main uh energy source and then we will have some buttons and then we will have infrared uh source and then we have some inside some chip in in the device #0
C#Uh since I don't have much time so I'll input the connections to all this components #0
C#And since I also want to know feedback from our Marketing Expert and User Interface  so if you want to add some more components we can incorporate them #0
A#Mm-hmm #0
A#Mm-hmm #0
A#And from from the discussion we had do you can you make it on the whiteboard  or mm #0
C#Yeah #0
C#Yeah  I'm sure  because since our User Interface speech recognition and also Marketing Expert for the speech recognition is really handy  so we can have another  like uh s simple speech recogniser on our D_S_B_ chip #0
A#Mm-hmm #0
D#Mm-hmm #0
A#Mm-hmm #0
A#Mm #0
C#Since we have some kind of uh energy this is our this normal battery  so this battery  once you s switch on it will take power and we can have some speech recognition and in our g generally small digital signal processing chip so that and we will put uh the small uh simple speech recogniser and we can also train the speech recogniser for particular user so you just yeah  so that we just use simple recog no but but Yeah #0
A#On Uh train it  okay #0
C##0
D#Mm-hmm #0
A#Mm-hmm #0
B##0
B#Too complex #0
D##0
A##0
D#But uh very very good to sell #0
D##0
C#No  even in you can find even simple mobile device like any mobile brand you can have these voice dialers or these things  yeah #0
D#Think of a all these young people who would love to say that this remote control only works for them  and ha ha you cannot use my remote control  because it's targeted to me #0
B#Okay #0
B#Okay #0
C#Yeah #0
A#Mm #0
C#Yeah #0
D#Whatever #0
A#And what about the price of this component #0
A##0
C#So Uh maybe we can make uh it in five Euros and even less than that  because we want to have uh millions and in bulk  so we can make really simp and we want to make really simple device because we have only very few words like like power  switch on or some like then we'll have something like this um we'll have volume and then we will have s particular channel  so users can listen #1
D##0
A#It mm okay #0
D#Hmm #0
D#Cheap #0
D#Millions #0
A#Mm #0
A##0
D##0
A#Okay #0
D#Cheap #0
A#Mm-hmm #0
D#The user uh will just be able to say uh please can you uh pump up the vo pump up the volume or or it will be something like volume  up  down #0
A#Mm-hmm #0
C#Ye No  yeah  a user can use any kind of sender but they should have this prompt volume and then yeah volume and decrease or increase  so we try to only recognise those words and and because we can't really say user to say same wording then it become more mechanical and yeah #0
D#Okay #0
A#With a keywords and yeah #0
C##0
D#Okay #0
D#Couple of words #0
A#Mm-hmm mm #0
A#Mm-hmm #0
D#Okay #0
A#Okay #0
B#Um #0
C#And then we can have channel they can say  okay I want eight  because we don't know like users have different programmes  I mean they don't really follow same channels strict uh so we just want channel number  we don't want like B_B_C_ or C_N_N_ or something else because it will be complicated so we'll have only these three uh main basic uh anyway volume is not really speech recognition problem  it's it it will be take care of our main mm #0
B##0
A##0
D#Of course uh it has to be Okay #0
A#Okay #0
B#Okay #0
B##0
B#No you know it's a conceptual question  'cause now I see th this the picture in front of my eyes like a user taking his remote control and shouting into it  volume up  volume up  and and he's coming you know  he's really annoyed with this  down  up  down #0
A##0
D##0
D#But then I think you you First of all I I think this is not uh functionality that it is going to be instead of using the buttons #0
D##0
A##0
B##0
A#No  in no not only speech  yeah #0
D#It's on top of using the button #0
B#Okay  for this budget like twelve Euros #0
A##0
A#I it's an option #0
D#Well  I dunno #0
C#Yeah actually we we can have one switch to like uh switch on  on and off  this processor and This really  suppose like here we have our main chip which controls power  volume and this part and this D_S_P_s #0
D##0
B#Mm-hmm #0
C#Again  this to have some interaction like suppose people use D_S_P_ then it particularly sends some information to the chip like in some form  like volume and like this key #0
C#So it may not be like very expensive  because since we are only focusing on T_V_ remote control so and we have only few things here Yeah #1
B#Okay. #0
D#T_V_ #0
A#Okay #0
B#Mm-hmm  mm-hmm #0
A#Sho to to train  okay #0
B#Did you consider the r gest uh gesture recognition #0
D##0
B#Like  if I want to put volume up I like do mm I take my remote control do like something like roll 'em up or roll 'em down #0
C#Um uh uh this point we didn't consider because it's it's very expensive because v our target is only like twelve point five Euros and Yeah  even automatic on off is also a bit problematic  because it different criteria for different people like so suppose people are really uh they just uh they don't touch the remote and mm y you don't know how much time you need to switch on or switch off and Yay yeah #0
A#Mm #0
D##0
D#Very expensive  no #0
A#And well  what about the idea of automatic on off on the button  yeah #0
B#Mm why #0
B#That's just We got a really good Market Expert #0
D#And volume control #0
D#So but uh Sh should we target a a user personalised uh uh remote control #0
D#So in in a given room there might be more than one remote control #0
D#We would uh have each one and uh with our own personal uh settings #0
A##0
C#Yeah that can be possible  especially for power settings  so user can say okay  suppose they're watching a tennis match or something then they can say okay uh after one hour I They can make Yeah we can have Yeah #0
D#Hmm #0
A#Wouldn't that make uh arguments #0
A##0
D##0
D#Yeah  of course #0
A#I want uh And we can increase this the strength y you can buy one with Mm-hmm #1
B##0
C##0
A##0
D#That's no problem  we will sell more #0
D##0
D##0
C##0
A##0
B##0
A##0
D#Yeah exactly #0
D##0
C##0
A##0
A##0
B#Let's send more  let's sell more #0
B#Okay #0
C##0
D##0
A#Okay #0
A#You have mm something else to say #0
C#Uh  not very much  like yeah #0
A#Uh #0
A#No #0
D#Okay #0
D#Thanks #0
A#Okay  thanks #0
C#Thank you #0
C#Yep #0
C#Thank you #1
A##0
A#So mm mm I think  okay  we're just on time #0
C#Can you just yeah #0
A#Um mm mm #1
A#So  we're now going to l have the lunch break #0
D#Mm great #0
A##0
D##0
A#Then we will work again for thirty minutes individual work  and um we will uh meet again for the next meeting  and uh in this one want to to be more focused on the individual actions the um on the components so uh you will focus on the component concept um uh of course the U_I_D_ Mark will be uh focused on the user interface concept and uh our m Market Expert Sammy Benjo uh on the trend watching #0
A##0
A##0
C#Yeah yeah #0
C#Mark will Uh it's in current price  yeah #0
A#So um of course like before specific instruction will be sent to you by your personal personal coach #0
A##0
A#Well I think that's all #1
A#And we have um maybe we have to we say  only for T_V_  not teletext #0
D##0
A#Uh I think automatic on off control uh it's not possible #0
D#Difficult #0
A#Yeah  maybe in the next uh step if we make it work um #0
C#Yeah #0
C#But speech recogniser can be possible #0
A#Yeah  implemented #0
A#O okay  we can think about that #0
D#Mm-hmm #0
A#And um do you see something else #0
D#No #0
B#Uh  should it be equipped with the uh  with uh speakers #0
D##0
D#Speakers in the remote cont Oh yeah #0
B#Like  you want to find it  you shout control  and it answers is I'm here #0
A#Uh yeah that's Or maybe you want to phone him #0
B#Or Just beeps #0
D#It just beeps #0
D#That would be enough #0
D#Something very cheap #0
D#But that's ex that's expensive #0
C#Yeah yeah yeah #0
A#Since now all yeah #0
D#Uh #0
C#Yeah  especially the power  it really consumes because it should be all the time on and Yeah #0
A#Think Uh-huh #0
D#Well I I heard of devices where you just uh whistle them and and they because of the the frequency they they just answer to that #0
A#And uh And it's answered #0
C#Yeah #0
B#I can't whistle #0
B#No  no  I can't #0
D#You can't whistle #0
D#Uh-huh #0
A##0
D#Or a clap #0
B#Mm #0
A#Clap clap clap it's a good I I think it's universal #0
D#You can clap #0
D#Can you #0
C#Yeah #0
D#Clap is good #0
C#Yeah #0
D#Tak Just a suggestion #0
D##0
B##0
A##0
C##0
A#What about people without hand #0
D##0
B#Okay #0
A#Yeah #0
D#With only one hand #0
D##0
A##0
C#Yeah I think it's good #0
D#These are not our target people #0
D##0
D##0
A#Mm uh okay #0
D##0
C#But it's a good feature I guess yeah we need to think about more how to incorporate it #0
A#Mm-hmm #0
A#Okay #0
B#Just don't interfere with other devices like 'cause like all these people do that their lights are turning on with clapping #0
D#Mm-hmm #0
D#Oh that's e that already exists okay okay #0
C#Oh #0
B#Yeah  I got it at my home  like #0
A#Mm-hmm #0
D#Oh yeah  you do have #0
B#Oops #0
B##0
D#Wow #0
C#Ah it's Yeah #0
D#You're trendy #0
A#Mm-hmm  so let's to think s so that yeah #0
D##0
C##0
A##0
D#Think about it #0
D#Yeah  okay #0
A#I think that could be in the component uh concept uh #0
A#It yes #0
A##0
D#Okay #1
A#Okay #0
D#Good we're done #0
A#So  yeah  let's go to lunch #0
C#Yeah #0
D#Right  thanks #0
C#Thank you  thank you very much #0

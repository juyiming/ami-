A#Well hi everyone again #0
B#Hello #0
C#Hello #0
D#Hello Mm-hmm #0
A#Um like before we uh I have to redo the meetings from n th the minutes from the last meeting and so here we go #0
A##0
A#Uh it was discussed in the last meeting uh which was opened by the presentation from the interface um designer that uh looks would be very important on this new remote and um it is to send messages of course to the T_V_ #0
B#Designer #0
B##0
D##0
A##0
A#It should have nine channel buttons  a next button  volume buttons  subtitle buttons  switch to control features  colour contrast  sharpness etcetera #0
A#It should have a memory switch  a mute button in case the telephone rings or something #0
A#Uh speech recognition is one of her very f favourite personal uh features she would like see d d to be integrated in this um in this new remote #0
A#Um #0
A##0
A#Should be child friendly design with few buttons  colourful maybe with s star shaped or other shaped buttons #0
A#Um she uh presented also an oversized remote which she guarantees nobody will ever be able to lose #0
B##0
A#Um that's right #0
D#And she was challenged on that point Okay #0
C##0
A##0
B##0
B#Yes #0
A##0
B##0
A#But uh her very f personal favourite really she  she would very much like to see a speech recogniser integrated in this remote #0
D##0
A#The industrial designer um presented her uh thoughts on the issue #0
A##0
A#She would like a special case made out of plastic that is very strong  not using any harmful materials  should be recyclable and should be colourful #0
A#Should have an integrated circuit board that's highly sophisticated and temperature resistant #0
A#She would like to see a timer and or alarm facility integrated #0
A#Uh technically this thing would also have a resistor and a capacitor  diode transistor  resonator  and if possible a rechargeable battery #0
A#Uh and of course a circuit board #0
A#And how it would works  you press the button  the chip is morse morse code related relays the uh to the generat to the generator amplification and uh the circuit board is very inexpensive to build and so she thinks this is a great feature uh to to to consider #0
A#She would like uh this whole thing should be push buttons with a simple chip uh scrolling method is more expensive and not that practical anymore #0
A#Should be battery operated and of course she would have the special cases #0
A#The marketing expert uh who has to finally come up with to to to market this product has been watching the competition  has done some research on the internet and also has used h her personal observations to come up with the fact that such a remote sh should be small  easy to use and it should be eye catching #0
A#From her point of view of course one of the most important facts is that we should get to market before our competition does #0
A#To do that uh maybe one or two features should be developed on which we could dwell on or in other words on which our campaign could be built on #0
A#Too many new features or too many points would only confuse matter #0
A#So we prefer to have one or two features that can be really uh driven home #0
A#Um it should have a fruit and vegetable design and should have a soft feel #0
A#She feels that's really what people want today #0
A#And the decision that we took last time was that uh the special feature we would like to see is a speech recogniser  the energy should be battery uh should be on a chip  should be trendy design  compact and strong  and should have buttons #0
A#And that concludes the presentation from the last minutes from the last meeting #1
A#Now uh we are ready for the presentation of the prototype #0
C#Yeah #0
B#Yeah #0
B#Just the look like  the button part I'll explain #0
C#Yeah #0
C#Uh so this is our what uh we have made #0
C#This is a model of the remote control which we are going to build #0
A#Mm-hmm #0
C#Uh this is us in a snail shape so uh it it is attractive um and it's it's blue in colour uh bright and uh it has yellow buttons and all the different colour buttons so it is a uh uh a looks-wise it is beautiful #0
D#Mm-hmm #0
D#Mm-hmm #0
D#Mm-hmm #0
D#Mm-hmm #0
C#Uh and also compact in shape #0
D#Mm-hmm #0
C#Uh um and also i it it will be easily fit into into the hands and you can access all the buttons easily #0
D#Mm-hmm #0
D#Mm-hmm #0
D#Good #0
C#Yeah  oops  sorry #0
A##0
D##0
C##0
D#You used to have all the buttons Oh that's good  no  that's nice and friendly #0
B##0
C##0
D##0
C#Um yeah and um uh the material which we are going to use for the case is uh plastic and uh w which which is s strong um uh and also uh for the Um the material is plastic and uh for the buttons it is uh s soft rubber um and als yeah #0
C#Yeah because uh uh you'll be touching the buttons more so it is soft when you touch it #0
D#Mm-hmm #0
D#Mm #0
A#Mm-hmm  mm-hmm #0
D#Mm-hmm #0
C#And then um uh for the for the led  for the light emitting diode it is a fluorescent green and it's a a it is a bulb like an ordinary infrared #0
D#Mm-hmm #0
D#Mm-hmm #1
C#And and the button button's part uh will be explained by F Francina #0
B#Okay #0
D#Okay #0
B#Now the um we decided upon including certain features on our remote #0
B#Now these features includes the s um signal emitting uh signal it's the led or L_E_D_ the infrared #0
D#Yeah  okay  mm-hmm #0
A#Mm-hmm  mm-hmm #0
B#Now uh we have included the switch on and off button #0
D#Mm-hmm #0
B#Now we have included another feature that is the mute button on the side of the model #0
D#Mm-hmm #0
D#Mm-hmm #0
B#Then we have included one to nine buttons for controlling the programmes the different channels #0
D#Mm-hmm #0
B#We have also included two buttons for increasing or decreasing the volume #0
D#Mm-hmm #0
B#And we have also included two buttons for scrolling up and scrolling down the programme channels #0
D#Mm #0
B#Now our our model also contains a button which is called as the menu button #0
A#What kind of button #0
B#Menu button #0
A#Menu #0
A#Uh menu th menu  uh one one #0
B#Yes  menu At the centre we have included a button which is fluorescent green colour and this is the menu button which will control the colour  sharpness  brightness of this uh picture #0
D#Menu button #0
D##0
D#Mm-hmm #0
D#Mm-hmm #0
D#Of the screen #0
D#Mm  mm-hmm #0
B#We have also included a button which is called as the swapping button #0
B#Now this is uh a special  special feature which we have included #0
B#Now this button is an elongated shaped button and this is slightly flexible so if it is turned towards the right it will take to the previous channel  if it is turned towards the right it will take to the next channel #0
B#It will take the user to the previous and the next channel so this is a swapping button #0
D#The next channel in the numeric pattern  or Yeah  mm-hmm #0
B#No  swapping is if if example you're you're watching the second channel and then you go to the tenth channel and if you want to go back to the second channel you can swap  this button #0
D#Mm #0
D#Okay  okay #0
B#Yes #0
B#And at the end  it this remote has inbuilt voice recogniser which c which will recognise the user's voice and then it'll act accordingly #0
A#Okay #0
A#Mm-hmm #0
A#Mm-hmm #0
B#So this is our proposed model #1
D#Okay #0
D#Mm-hmm #0
B#Now the marketing expert has to give her suggestion whether it'll be sellable or it'll be cost effective #0
C#Tell  yeah #0
D##0
C##0
B##0
D#Okay  well um what what I really like a lot about it is that you can reach the whole thing with one thumb  that you can really hold it in one h you don't need two hands and it's easily reachable even for somebody with a small hand  yeah #0
B#Yes  yes #0
C#Mm-hmm #0
A#Yes the buttons are all raised  right #0
D#The buttons are all raised and if you hold it in the centre of your hand you can even reach it over here so you don't have to turn it around  turn it upside down  move it up  up and down  I really like that #0
B#Yes #0
A#Are raised  mm-hmm #0
A#Right #0
A#Or have two hands to operate it  yeah #0
D#You really did a good job on that  my little designers #0
B#Mm #0
C#Mm-hmm #0
B##0
C##0
D#Um and um I like the idea that the on-off button is in a really prominent place #0
D##0
D#That's that's a really good good thing #0
A#Yes  and it sort of sticks up so that you really you don't have to g first go like oh yeah here it's on and yeah  mm-hmm #0
D#Yeah  that's great #0
C#Hmm #0
D#Mm-hmm #0
C#Mm-hmm #0
B#Abs okay #0
D#The colour's very attractive #0
D#Um the um these buttons uh around here are the mute and these mm-hmm On both sides they're mute #0
B#No  these the front buttons which are here  are the mute buttons #0
B#Yes  yes #0
D#So you can push either one #0
B#Yes #0
D#Okay #0
A#So if you're left-handed or right-handed it doesn't matter #0
D#And this brings the menu up on the screen #0
B##0
B#Pardon me #0
D#This brings the menu up on the screen and the orange ones are Okay #0
B#This is the menu yes  yes #0
B#A the the these these two are th to increase or decrease the volumes  and these two are to uh scroll the programme channels #0
D#F f okay #0
D#Right  very good #0
B#Scroll up or scroll down the channels #0
D#Uh it looks mm looks like something I can sell #0
B##0
C##0
D#Okay and now I'm supposed to yeah #0
A#Well  I have one question uh will there be anything written on the buttons  like that people know  or they have to learn that from a piece of paper which button does what #0
B#Yes  it will have uh these buttons will have the numbers and all the rest of the buttons will have symbols #0
C#Ah #0
C#Yeah  definitely #0
A#Will have symbols so that that that the user really knows you know and doesn't have to first learn it Yeah #0
B#Yes  which can be easily recognised #0
D#Yeah #0
C#Yeah #0
C#Yeah #0
C#Yeah #0
B#Yes #0
C#Yeah #0
D#Good point because we need the symbols 'cause we're going into an international market we can't have anything that's language dependent #0
B#Yes #0
C#Yeah of course  and also Hmm #0
B#Yes #0
C#Yeah #0
A#But anyway it would ha i i i it has to have some kind of of symbols  text or something so that people kn That's right #0
B#Yes #0
C#Yeah we can Text #0
D#Symbols on it #0
D#Mm-hmm  mm-hmm #0
B#Yes #0
C#Text that we can have on the case itself  we can it will be printed on the case and symbols as well as the buttons #0
B#Yes #0
D#Mm-hmm #0
A#Okay  yeah just wanted make sure of that mm-hmm #0
B#And and one more feature is we we have a holder for this remote which is an oyster shape #0
C#Yeah #0
D#Mm  'kay  mm #0
D#Mm-hmm #0
B#A shell shape #0
D#For the snail  yeah  mm-hmm  we have the snail shell #0
A#Right  mm-hmm #0
B#Yes  yes #0
C#Yeah #0
D##0
C#So it is yeah  yeah shell #0
B#Yes  snail shell #0
A#Mm-hmm #0
D#He goes right back into his shell #0
B#Yes #0
A#Right #0
D#Well you know I think we could do something really funny with this too because the snail is known to be slow and we could have some sort of little comic effect on our marketing about how this is a rapid snail or something like that you know that would  that would really work #0
C#Mm-hmm #0
B#Mm-hmm #0
C#Mm #0
B##0
B#Y Yes Yes #0
C##0
C#Yeah  of course  yeah #0
B##0
A#Now what  what are our special features for the marketing #0
D#So I think voice recognition is our big selling point 'cause nobody else seems to have that in in this price range #0
A#That's really the voice recognition that's really unusual And then  and then the other thing would basically be sh shape or practicality of use #0
C#Mm #0
C#Mm-hmm #0
C#Mm #0
D#Yep uh well I think that everybody's gonna say their remote control is practical #0
A#You know #0
D#I think we have to  we have to dwell on on on the appearance #0
A#Colours #0
A#Mm-hmm #0
D#We're really gonna have the be the cutest remote control on the block #0
A#Cutest #0
C#Mm #0
A#Yeah #0
D#So I think we have to play with the image  play with the snail image um play with the visual and then the voice recognition #0
D#I think those are the two things to push #0
C#Mm-hmm #0
D#The look and the voice recognition #0
D#They're gonna be our two selling points #1
A#Okay  now uh having said that No  now this was our evaluation criteria which we uh just have done #0
D#I'm supposed to make a little presentation  aren't I #0
D#Okay #0
A#Now we're gonna talk about financing #0
D#Ah  but in my instructions I think it said I was supposed to go to the board and do something #0
A##0
A##0
D#No #0
A#Well  there is a production evaluation #0
A#Is that you #0
D#Yeah  that's me #0
A#But that's after the financing #0
D#Oh  okay #0
A#See #0
D#Sorry  sorry #0
A#Fi see #0
A##0
D#Mm-mm #0
B##0
C##0
A#Um #1
A##0
A#Okay  we had looks and voice recognition #0
A#Okay now on the financing we bring up the mm there it is #0
A#Okay uh energy source we say that's battery  right #0
C#Mm #0
D#That's right #0
A#Okay  now #0
A#So we I guess we use one #0
C#Yeah #0
A#What #0
A#T cell or chart you are trying to change is protected #0
A#Well  that's nice #0
A#She told me I could just ch change it here and then it would It doesn't work #0
A#Hmm #0
D#Can you just fill it in in the yellow boxes #0
D#Or Well #0
A#Oh  okay yeah  okay  let's see #0
C#Yeah #0
A#Okay  one  okay #0
B#Yes #0
A#Oh go away #0
A#Um kinetic source so that's in the energy source that's all we need #0
C#Mm-hmm #0
A#Uh electronics  simple chip on print #0
A#Is that's what we're using #0
C#Yeah  yeah #0
B#Yes #0
A#One of those #0
A#Come on #0
A#Okay  one #0
A#Uh regular chip on print #0
A#No #0
C#No #0
A#That's all we need  the one case  uncurved flat  single curved  double curved #0
C#Yeah #0
D##0
C#This is a Yeah #0
B#Single curve #0
B#Mm #0
D#I guess it's double curved #0
A#Double curved #0
A#One of those #0
D#Mm-hmm #0
A#Case materi s supplements #0
A#Plastic we said  right #0
B#Plastic #0
A#Uh wood  rubber #0
D#Rubber  because we're gonna have the soft buttons #0
C#Yeah #0
A#Uh but  yes but That's just for the case material  so special colours though  we having that  right #0
C#I think uh that is uh f for rubbers that is uh yeah case material #0
B#Is this for the case #0
D#Oh okay  the mm-hmm  mm' kay #0
B#Yes #0
C#Mm-hmm #0
B#Yes #0
D#Mm-hmm #0
A#And then we have to interface push buttons #0
B#Yes #0
D#Mm-hmm #0
A#Scroll wheel  no #0
A#Integrated scroll wheel  L_C_ display #0
D#No  'cause we didn't put the clock in it after all  right #0
B#No #0
A#Button #0
A#No #0
C#Yeah #0
D#Mm-hmm #0
A#Uh  button supplement special colour #0
B#Speci Yes Yes d we do have special form #0
A#Special form #0
D#Yes #0
A#And special material  rubber  wood  yes #0
C#Yeah #0
A#Okay #0
A##0
A#Total seven point six whatever that means #0
C#Uh  I think that's the price #0
A#That's the price #0
B#One two three four five six seven eight nine Nine points  okay  yes #0
C#Maybe it is it just n Yeah #0
A#Mm #0
A#Eight  eight point two #0
A#That's hmm #0
A#Eight point two  right #0
A#So  we looks like we are well within budget #0
D#Mm-hmm #0
B#Okay #0
A#Okay #0
A#I guess I should save this I suppose  huh #0
D#Yeah #0
A#Oops #0
A#Uh-huh huh huh #0
B#On the desktop #0
A#I just tried that #0
A#My documents  computer #0
C#AMI #0
A#My compu Ah oh here it is  yes #0
C#AMI should for Yeah #0
A#Okay  fine #0
A#Save #0
A#Okay good  so that's the good news #0
A#We gonna be popular #0
D#Mm-hmm #0
B##0
A#Um #0
A##0
A#So that uh I think financing was pretty simple #0
D#Mm-hmm #1
A#Now we would like to have a presentation by the marketing expert on production evaluation #0
D#Okay  I'll take my file down so you can bring it up #0
D#'Kay should be able to get it now #0
D#'Kay  why don't you move just to the next slide right away #0
A#You wanna go to the next slide #0
D#Yeah right away #0
A#Okay #0
D#Okay  well uh obviously my method for uh s m the marketing of this thing is first to ask the big question  will it sell #0
D#And I think we should show this prototype to people from various age and socio-economic groups and see about any fine tuning that maybe little things we haven't thought of #0
B#Okay #0
C#Mm-hmm #0
D#We can't accept every suggestion of course  but maybe we just need to get a few #0
C#Yeah #0
D#And show the the prototype to consumer research groups  we don't s want somebody to suddenly come to us and tell us that this button is toxic and you know some child will swallow it and then we won't sell any #0
C#Mm-hmm #0
C#Mm #0
D#So we have to get some input from those people #0
C#Mm-hmm #0
D#And then after that we just have to go with our best intuition and you know we like it  we think it's good  we're gonna get behind it and sell it #0
D#Um  next slide please #0
D#Okay  now the things that I was thinking and th my wish list has really been realised in this prototype #0
D#I wanted the shape to be biomorphic  I didn't want anything with angles and all square  I wanted it to be comfy and roundy so we we've got that #0
D#The size is small  the colour's bright and warm which is what we wanted #0
D#We wanted the feel to be as soft as possible  we'll have the soft buttons and the way this is shaped  even though it's gonna be hard plastic  it feels good in your hand so that's nice #0
C#Mm-hmm #0
D#And functionality I put last on my list because people aren't going to use it before they buy it #0
C##0
D#So paradoxically the other features  in other words  the look  the feel um and the shape  that's what people are gonna get in the store #0
D##0
B#Fee selling #0
C#Yeah #0
D#They don't have a television in the store  they can't play with it #0
C#Yeah #0
B#Yes #0
D#Um so they'll be our main selling points #0
D#So um th those have been fulfilled by your prototype and go ahead to the next slide please #0
D##0
D#Okay  so um the shape um I think is a a one #0
D#That's really  really excellent shape #0
D#The size is small um and th these points are in the importance for the  for the marketing  these aren't i in how I feel #0
D#I think that it's I think that it's plenty small enough to sell but I think we're sort of right the scale is one to seven #0
D#I think we're sort of right in the middle as far as c other competitors #0
B#Okay #0
D#And our colour I think is great #0
D#The colours are bright and warm and we really do great job there #0
D#And given um the constraints that we had I think we got it as soft as possible #0
D#And then functionality um I think you did a really good job on functionality  obviously we could have ad added different functions but then we'd disturb something else so I would say that we got to a five out of seven on on functionality #0
D#So I think that basically we've got a great product and we can get off and running with it #0
B#Okay #0
C#Mm-hmm #0
C#Yeah #1
A#Um I just realised one thing #0
D#Yes #0
A#In the financing there was no room for our voice recogniser #0
B#Yes #0
D#Ah #0
C#Yeah #0
A#And uh I don't know how we can evaluate that or how we can include that  too #0
D#Well  um we had what  eight eight euros twenty as our cost #0
A#Eight  eight twenty  yes #0
C#Eight twenty so We have um four euros  yeah Yeah #0
D#And so we've got we've still got four euros to go to spend #0
A#I mean maximum we have another four point three euros I mean four thirty #0
D#Mm-hmm #0
A##0
D#Well um that's Mm-hmm #0
A#But I mean we have no way of presenting that to management as you know as a f finished  as a finished product and saying okay with the voice recogniser that costs so much #0
A#So um we just have to beware of that #0
A#I mean and know whether the four thirty will really cover that #0
D##0
D#Well as we know in today's technic technological world you can do just about anything at any price  the the the problem is quality #0
C#Yeah #0
A##0
C#Yeah #0
D#So we're just gonna have to settle for whatever quality that will buy us #0
C#Yeah  yeah #0
B#Yes  yes #0
D#And um it may not be the greatest quality but it may sell anyway #0
C#Yeah  still #0
A#Yeah #0
D#As we've seen with so many of these kinds of products #0
D##0
C#Yeah #0
C##0
A#I'm sorry to interrupt then but I just uh recog I just remembered that there was no that that was not um included uh that there was no room for any special features  okay #0
D##0
C#Included  yeah #0
C#Hmm  hmm Yeah  even my yeah  shape is one #0
B#Yeah mm #0
A#So to beware of that #1
A#You wanna go to this next slide  marketing expert #0
D#Uh  well I isn't this my last slide #0
D#Maybe #0
A#I dunno #0
D#Go ahead #0
A#Yes it is #0
D#I think that was my last slide  yeah #0
A#Mm-hmm #0
D#Um #0
D#Mm okay #0
D#And I'm supposed to present this scale on the whiteboard #0
D#Um and we're supposed to talk about those things as a team now  so if you put my last slide back up there #0
A#Oh #0
D#I'm sorry I've um forgot to do that  um #0
A#Why #0
A#Wh why you need that up #0
D#Hmm #0
D#Well because I can't remember what I put on there #0
D##0
C##0
D#Okay #0
D#Now I'm supposed to see how long my leash is here #0
A#I think you can make it there #0
D#Mm 'kay #0
D#You ready #0
D#So now we're all supposed to say what we think #0
D#Um okay so on shape I gave it a one #0
D#Wait what would you ra uh one being good and seven being the worst #0
B#Worse  okay #0
D#Um what do you think the shape is #0
B#One #0
D#One  okay  and Be Betsy #0
A#Yes I think uh shape is one #0
D#Okay  uh-huh one  okay #0
D#And how about on size #0
D#On size I gave it a four  yeah  I feel it's just average #0
A#You you gave it a four #0
A#Um  I dunno #0
A#I think I would give it at least a two #0
D#Okay #0
B#One #0
D#Mm-hmm #0
C#Yeah  even I think it is one #0
D#Okay #0
C#It's quite small #0
D#Okay you're the designer  of course you wanna give it a one #0
B##0
D##0
B##0
C##0
C##0
D#Um and then how about how we doing on colour #0
D##0
A#Colour uh I One #0
D#Colour  I gave it a one #0
D#I really like all those nice bright  warm colours #0
A#I I like the colours #0
B#One #0
A#One #0
D#One #0
B#Yes #0
C#Yeah  one #0
D#One  one  one  okay #0
D#And how about the feel #0
D#Taking into um consideration texture and comfort in the hand #0
A#Uh  I think I would give it a two #0
D#Okay  I gave it a three  two  yeah #0
B#I'll give three #0
D#Three #0
C#Uh maybe two  yeah #0
D#Two  okay #0
D#And the next is functionality where I I admit I was a little hard on our team here  but Yeah #0
A#Well it's also you can't really try it out uh the other things you have have more is are more tangible so from that point of view but um I'll give it a three #0
D#Mm-hmm #0
D#Okay #0
B#Two #0
D#Two  okay #0
C#Uh three  mm-hmm #0
D#Three #0
D#Okay #0
D#Well  um #0
D#It looks like we've got got ourselves a pretty good product #0
D#Um the functionality's the only place where maybe we have to think about m m maybe  heaven forbid  having another meeting #0
C##0
C#Yeah #0
C##0
D#But um otherwise I think we're we're ready to go to go with this product #0
D##0
D#Anybody else have any other comments or any other things that we feel we should evaluate #1
A#Uh #0
A#Here is what we looking at uh satisfaction on for example room for creativity #0
A#Um #0
A#Is there more room for creativity or are we absolutely happy #0
C#Maybe we can yeah  include some more buttons and uh um yeah features #0
B#We can always improve  yes #0
B#Yes  features #0
C#We can make the buttons few buttons smaller #0
C#Uh I think they are quite big  so I think I I mean we can just have small buttons and more buttons in that case #0
C#If we want to have more features than that #0
D#Mm-hmm #0
D#Well then again if we're gonna um do the speech recognition thing we're gon there gonna be some buttons that are gonna have to be added for that for the recording of the the speech #0
D##0
C#Yeah #0
C#Yeah  definitely #0
B#Yes #0
B#Voices #0
C#Mm-hmm #0
D#So that that's where we're gonna have to do maybe we can eliminate one of the mute buttons  instead of having two mute buttons #0
C#Yeah  definitely  yeah  two mu mute buttons #0
B#Yes #0
B#Yes #0
D#And um then maybe we can do something with the um the volume control #0
D#Maybe we can put that all on one button #0
C#Hmm #0
D#Um and a couple of other th maybe comp uh consolidate some of the usage an and see what we can do with that #0
C#Mm-hmm #0
A#Y um al always bearing in mind that right now we are of course well within the budget and that we still you know we probably can't  with this particular item  we probably can't just uh add a whole lot of more things #0
B#And maybe Yes #0
D#Mm-hmm #0
D#No #0
A#Uh um we need uh you know we need to leave space money-wise for the voice recogniser #0
C#Yeah #0
D#Mm-hmm #0
D#Mm-hmm #0
A#Um #0
C#Mm-hmm #0
A#So th the question really is how do we feel with the project process #0
A#Uh  um are we happy with the creativity that has passed here or we're not happy with the new product we created or that was created #0
A#Uh I think personally I think uh I'm pretty happy #0
D#I'm pretty happy with it too  yeah um  it's something I think I can market #0
C#Even I'm happy #0
A#Um an and then the next question is are we happy with the leadership of this project #0
C#Yeah #1
D#Yeah #0
D##0
A#Uh And I think team work I think was very very good  I think we really yeah #0
D#I think you've done a good job  Miss leader #0
B#Yes  yes you've done a good job #0
C#Yeah  yeah  definitely #0
C##0
D##0
D#Yeah I d I do too I think we worked well together as a team  yeah #0
C#Mm-hmm #0
C#Yeah #0
C#Yeah #0
A#Mm-hmm #0
D#Mm-hmm #0
A#And uh I think we are we happy with the means we used #0
A#We used whiteboard  we didn't use digital p well digital pens I guess are these things #0
D#Yeah maybe we could've used the whiteboard a little bit more  yeah  we didn't use that enough #0
B#Whiteboard more  yes  yes #0
C#Yeah  probably #0
A#Yes  we could #0
A#Uh  it's maybe not in the best position in the room um you know like sometimes it's positioned so that it's much better visible for everybody and I think from that point of view we sort of ignored it a little bit #0
D#Mm-hmm #0
D#And we used the slide because it was better positioned #0
B#Yes #0
A##0
A#Yes  I think so  I think absolutely  and fortunately we all had slides presentation which made it a little easier #0
C#Yeah #0
B#No #0
D#Mm I think that's true mm-hmm #0
C#Hmm #0
B#Yes #0
D#Mm-hmm #0
A#Um did we new did we find new ideas #0
A#I think we did #0
C#Yeah  many #0
D#I think we were we were very good  yeah  mm #0
A#I think we we did  uh in more than one respect and uh so I think we did very well here #0
D#Mm-hmm #0
D#Okay #0
A#Are the costs within budget #0
A#Yes  yes #0
A#Uh is the project evaluated #0
D#Yes  yes #0
A#Yes #1
B##0
C#Yeah #0
A#Um then celebration #0
D#Celebration #0
B#Cel celebration yes  yes #0
B##0
C#Ah #0
B##0
A##0
C##0
D#Today we have apple juice and after we sell m million of 'em we have champagne #0
D##0
A##0
B##0
C##0
A#So I I thank you all very much #0
D##0
A#Um  I think this was very good and um I think we did come up with a new product that's uh feasible #0
D#Mm-hmm #0
C#Yeah #0
D#Mm-hmm #0
A#Feasible from the production point of view and feasible from a marketing point of view #0
D#Mm-hmm #0
A#So  thank you #0
D#Okay #0
C#Yeah #0
B#Thank you #0
C#Thank you very much #0
D#Okay #0
D#Watch I I have my cord behind you here #0
B#Okay #0
D#Okay #0
A#I always get it on here  but getting it off is Ah yes we have time later but we don't #0
C#Do we do we have some time left #0
C#Uh you have Oh  alright #0
B#They say it's forty minutes #0
D#But we we were told we could end the final meeting at any time  whenever we felt we were finished #0
B#Okay #0
D#It'll take me the rest of the time to get my microphone out from my necklace #0
D##0
D#Oh  there we go #1

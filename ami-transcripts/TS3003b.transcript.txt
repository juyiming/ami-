A#Okay #0
A#Everybody found his place again #0
A#Yeah #0
D#Yes #0
A#That's nice #0
A#Okay so this is our second meeting #0
A#And uh still failing #0
A##0
D#Yeah #0
A#Uh now we're going um into the functional design #0
A#Um important thing of this phase is that we're going to uh try to get an agreement about the user requirements  technical function design  and the working design #0
A#So that we can move onto the second uh phase #0
A#But first this phase #0
A#Um first an announcement #0
A#There's a little adaptation in the air conditioning system #0
A#So There's our ghost mouse again #0
D##0
A##0
A##0
A#That that means that you can have a little trouble with  little trouble with the air conditioning  that's because of this uh It's in wing C_ and E_ #0
B#Okay #0
D#Okay #0
A#So it should be over in a in a while  couple of days #0
A#But it's going to be cold anyway  so I don't think you're gonna need it #0
B##0
A##0
D#No #0
A#Then our agenda #0
A#Now first the opening #0
A#Uh this time I will take the minutes #0
A#Uh you're going to have a presentation #0
A#All of you #0
A#Um and we've got forty minutes for the whole uh prese for the whole uh presentations #0
A#So uh I suggest we take about seven minutes per presentation  and then we can have a little discussion about the new project requirements uh which have been sent to me #0
A#And then the decision on the control functions uh which we wanna include and those which we don't wanna include #0
A#So we've got forty minutes for all of it #0
A#So I suggest um let's start with the first presentation #0
D#Yes #0
A#Um who wants to be first #0
D#Think I'll go first #0
A#Okay #0
A#So Just maybe it's easier if you um Yeah I think you will tell your presentation as well #0
D##0
A#Just which function you have and what you're gonna talk about #0
A##1
D#'Kay #0
D#My name is Freek Van Ponnen #0
D#I'm the Market Expert #0
D#But you already knew that #0
D#Um I've done some research #0
D#We have we uh have been doing research in a usability lab where we observed um users operating remote controls #0
D#Uh we let them fill out a questionnaire #0
D#We had one hundred of these uh test subjects #0
D#Uh in addition we did some market research #0
D#Uh see what the market consists of #0
D#What ages are involved #0
D#Well these are three quite astonishing results  I thought #0
D#Um remotes are being considered ugly #0
D#F uh seventy five percent of the um people questioned uh indicated that they thought their remote were was ugly #0
D#Um and an additional eighty percent indicated that they would spend more money on a fancy-looking remote control #0
D#So Um in addition remotes were not very functional #0
D#Fifty percent of the people indicated they only loo used about ten percent of the buttons on a remote control #0
D#And fifty percent of the people indicated that their remote tended to get lost in their room #0
D#So some things #0
B#Mm #0
D#Then we did some research to the most relevant functions #0
D#Channel selection and volume selection um both got a ten on a scale of one to ten for relevancy #0
D#The power button got a nine #0
D#And teletext got a six and a half #0
D#So these are the most most uh important functions on a remote control #0
D#Then there are some one-time use function #0
D#That's what I like to call them #0
D#That uh audio settings  video settings  and channel settings buttons #0
D#Which are not really used very frequently  but are still considered to be of some importance #0
D#Um channel selection was also indicated to be used very frequently #0
D#One hundred and sixty eight times per hour #1
D#Then these are the This is the market #0
D#Um sixty percent of the market consists of users between the ages sixteen and forty f six #0
D#Um Main characteristic of this group is that they're very critical on the remote control #0
D#Um they like to use new f new functions #0
D#But they also are very critical #0
D#They won't spend their money very easily #0
D#So Um the users of forty six to sixty five years cons The make up forty percent of the market #0
D#They are not really very interested in features #0
D#But they do tend to spend their money a lot easier #1
D#Um What I think this indicates for our um design #0
D#I think we should make a remote for the future #0
D#And this means we would um have to focus on the age ages sixteen to forty five #0
D#Uh this also makes up most the biggest part of the market  so that will also be where our main profit would be gettable #0
D#Um this would mean we would have to make a fancy design #0
D#Um The results also indicated that um about one quarter of the people questioned thought that the remote control caused R_S_ R_S_I_ #0
D#Um this is certainly something to take into account #0
D#And thirty four percent thought that it was hard to learn a n how to operate a new control  remote control #0
D#So these are two factors that I think should be included in the design #0
D#Besides of course that the remote must look very nice #0
D#And the functionality As a lot of people indicated  they only use about ten percent of the buttons  I think we should make very few buttons #0
D#Uh this will also be uh beneficial to the design of the remote #0
D#Uh I think the most frequently used buttons should be emphasised #0
D#Especially the channel selection and audio uh selection buttons #0
D#'Cause they're used most and so they should be robust #0
D#They shouldn't break down easily #1
D#Um Then as mo as a lot of people indicated that their um remote got lost in the room  it might be and I say might be because it would um certainly boost the uh production costs a lot #0
D#But it might be a good idea to make a docking station #0
D#And this would  could get a button in it which would send a signal to the remote which would then beep #0
D#So you'd know where it is in the room #0
D#And in addition to this it could um recharge the batteries in the remote if you put it in #1
D#Then um a surprisingly great deal of people w indicated that um an L_C_D_ screen in the remote control would be preferred #0
D#This was um mostly people in the age of sixteen to twenty five #0
D#But up till forty five it remains feasible #0
D#This would also greatly increase the production costs but I think these are just some small factors we could consider #1
A#Okay #0
D#That would be all #0
A#Thank you #0
A#So anybody have um any questions until now #0
D#Any questions #0
B#Mm-hmm #0
A#About functional requirements #0
C#No #1
A#Okay that's clear #0
D#'Kay #0
A#Now to the second #0
D##0
A##1
B#Uh okay #0
B#Um I've been looking at uh the user interface of it #0
A#Yeah #0
B#Um f for the techno f functions uh of of it #0
B#Um Okay #0
A#Yeah you can take your time #0
A#We've got uh plenty of time  so Yeah #0
B#Mm #0
D#Yeah you should go to the top thingy #0
B#Oh #0
B#Uh #0
D#Slide show #0
D#Oh yeah #0
B#Okay #1
A#There it is #0
A#Yeah #0
B#Um yeah #0
B#I think uh we uh must use the general functions uh of the uh remote control #0
B#Uh uh I've do I've uh done a little uh research on the internet and not much information about it  Um about uh interface but uh Uh Yeah I I've been thinking about a simple manner uh to put a lot of functions uh uh in one um in one uh remote control #0
B#Uh so uh you've got a lot of devi uh devices like uh D_V_D_ uh uh television  uh stereo #0
B#So um But uh it must be uh user-friendly #0
B#So um uh you c you can't put a a lot of uh functions uh in one uh Yeah #0
B#Uh uh uh Yeah #0
B#In one um remote control #0
A#One remote #0
B#But um Yeah #0
B#Um yeah #0
B#Got uh many functions in one uh remote control  um but um yeah you can see  this is uh quite simple uh remote control #0
B#Um few uh buttons but uh This uh re uh remote control got a a lot of uh buttons #0
A##0
B#Um uh people uh don't like it  uh so um Well what uh I was uh thinking about was um uh keep the general functions uh like they are #0
B#So uh like uh the on-off uh button #0
B#Uh keep it uh yeah l like a red button #0
B#Uh everybody everybody knows it so uh uh you don't have to change that #0
B#Um My personal uh preferences um #0
B#Use a display for uh specific uh functions of the different uh device #0
B#So um Wh what I was th uh thinking about was um you've got um Uh this the remote control uh and uh you got here the general functions  uh like uh the on-off button uh sound uh I dunno um And um here you've got a s kind of a display #0
A##0
C##0
B##0
B##0
B#It's a touchscreen #0
B#So um yeah you got a general f uh f the functions of the device uh for a D_V_D_ player or uh so um the pl yeah um f for uh playing uh reverse uh #0
B#And um you got here uh real buttons for uh selecting uh a device #0
B#So um this button is for a D_V_D_ or So um for every um device you've got a uh a f a b a part uh display of a part buttons #0
B##0
B#So uh you you never got uh all the buttons uh on w one device #0
B#So uh that's uh my uh idea about it #0
A#Hmm #0
A##0
A#'Kay #0
B#Um yeah and Uh let's see #0
B#Uh yeah #0
B#So a touchscreen #0
B#Uh and um th the buttons uh the real buttons uh we have to use um #0
B#We better c um uh use uh quite uh large buttons uh for um yeah #0
B#Everybody uh have to use it so Uh ol even even old people um young people #0
B#So uh we must keep uh buttons uh quite s uh simple and quite large #0
B#So uh Um Yeah #0
A#Yeah #0
B#Uh yeah #1
B#That was uh my uh part of it #0
A#Okay #0
B#So N I I don't think so #0
A#Anybody has questions about the technical functions #0
C#Well I think if we are gonna use a touchscreen uh we're gonna go way above the twelve and a half Euros #0
A#Yeah #0
B#Uh you got uh quite a cheap uh touchscreen #0
A#Touchscreen #0
B#S um it's uh not uh in colour or something #0
B#Uh it's just uh um one colo Uh yeah #0
B#Uh I seen uh w uh something on the internet uh not today but uh a few uh weeks ago #0
B#Uh you got uh yeah quite an uh a kind of uh touchscreen um and it's uh for uh twenty uh Euros or uh less uh #0
B##0
A#Huh #0
B#So it's possible #0
D#Hmm #0
A#'Kay #0
A#That's nice #0
D#Well it would certainly make a fancy design #0
A#Uh Yeah #0
B#Yeah #0
C#But the It wouldn't be very robust #0
D#So That is true #0
C#It's very fragile and you can get scratches on it #0
A#That's right #1
B#Yeah that's true #0
D#We would have to look into that #0
A#Uh maybe we can first um listen to your presentation #0
A#Uh And then we have a little discussion about the requirements and uh design #0
B#Uh #0
D#Yeah #0
C#That's #0
C#Okay #1
A##0
D##0
A#I think it's going to Uh it's not too much #0
A#Okay #0
C#Okay I've got a presentation about the working design #0
A##1
C#Um first about how it works #0
C#It's really simple of course #0
C#Everybody knows how a remote works #0
C#The user presses a button #0
C#The remote determines what button it is  uses the infrared to send a signal to the T_V_ #0
C#The T_V_ switches to the frequency  or what function it is #0
C#So we've got um the the plate #0
C#It gots conductive disks for every button #0
C#When the user presses a button  a signal got sent  goes to the LED and transmits tranmi transmits its to the T_V_ #0
C#It's a very simple device  technically speaking #0
C#So this is a schematic overview #0
C#You've got the buttons #0
C#The power source #0
C#And uh when a button gets pressed  its goes to the chip #0
C#The chip uh controls the infrared bulb and perf perhaps a normal bulb #0
C#When you press a button you can actually see your pressed button #0
C#Well um I think we should use default materials  simple plastics #0
C#Keep the inner workings simple  so it's robust #0
C#Uh I think we should focus on aesthetics  the design and the user interface  because if you're going to use high-tech materials the price is going to go sky-high #0
C#And uh you only have to design a remote once  and if you use high-tech materials it come back in every product #0
C#So it's  in my idea  it's uh it's gonna be smart to invest in di in design and not in uh in the product itself #1
C#That's it #0
A#Okay #0
A#Thank you #1
A#Okay #0
D##0
A#Uh Okay now I hope everybody has a little bit more insight in the functions we all have and what we are doing right now #0
A#Um I'm the Project Manager so I'm here to mess things up and uh tell you some new uh requirements #0
B##0
C##0
D##0
A#Um that's  we've uh got to design a um remote which is only suitable for T_V_ #0
A#Um that's because uh it will be too complex and the time to market will be too big  if we wanna have it uh for more functions #0
B#Okay #0
A##0
B#Mm #0
A#So it has to be simple #0
A#Uh another point is we have to skip the teletext  because in the world of uh upcoming internet uh we think teletext is going to be uh a thing of the past #0
A#And uh it's a function we don't need in our remote control #0
D##0
A#Um internet is also mentioned in a function we can use #0
A#Uh maybe also on televisions it will be available as well #1
A#Another one is uh the customer is uh forty plus #0
A#Uh that's the the market we have to to to target  because we are going to develop a new product which is specially designed for the younger customers #0
A#Um this is uh a bit pity for the Marketing uh Expert #0
D##0
B##0
A#Because he was uh aiming on the the younger persons #0
A##0
A#So we have to find a market which is above forty plus uh but which will suit our uh remote control  and the other way round #1
A#And we have to be very uh attent in uh putting the corporate image uh in our product #0
A#So it has to be visible in our design  in the way our device works #0
A#And uh we have to be uh very clear on this point as well #1
A#So I suggest let's have a discussion on the control functions #0
A#Yeah #0
D#So is there any discussion possible about the new product requirement #0
A##0
B##0
A#Uh we can see if we can find a way uh between the functions we wanna use and the market we wanna reach with our product #0
B#Mm #0
A#Um Against the no teletext #0
D#Yeah 'cause you're you're saying that teletext is gonna be an old feature and it's not gonna be used anymore anyway pretty soon #0
D#And new T_V_s will have internet access on them #0
A##0
D#But I think if you're targeting people of forty plus  the chance that they will have a T_V_ with internet access within the next like twenty years is very slim #0
D#In addition people indicated that teletext simply was an important feature for the remote control #0
B#Yeah #0
D#So I think it's pretty dumb to put no teletext feature on it #0
D#I'm pretty much against it #0
B#Mm #0
D#Yes #0
D##0
A#Um Yeah it's it is Standard remote #0
D#Besides that  I think the market for forty plus is like pretty small #0
D#But I mean if I s if I see this  it's I think we're just gonna go for another pretty and not innovative remote control #0
B#forty Yeah #0
A#No I think we can I think we can do a lot with the design and the simple buttons which were also mentioned #0
A#Uh if we put a lot of effort in those  we can make a remote control with uh just two or three buttons #0
A#Or just a remote which is suitable for the market we wanna reach because it is forty percent of the market #0
A#And um if you look in Holland at the whole generation of forty plus  fifty plus  it it's the the biggest share of the of the whole population now #0
D#Yes but it's not the biggest part of the market #0
A#No #0
D#And besides that  they're not very critical so I mean they don't really care what the remote control is like #0
D#They'll just pretty much take the first thing they see and which looks acceptable #0
B##0
A#But don't you think that if we make a remote which is uh typically made for this market  that people think the people think that's the the device I've looked for although I didn't realise it #0
A#So let's try it #0
D#No #0
D#I think that would be the case in the sixteen to forty five age category #0
D#because they are critical and they they want to have a fancy remote control #0
B#Yeah #0
D#People of forty plus  I mean they want it to work  but as soo as soon as it works it's okay with them #0
B#Mm #0
A#Yeah #1
C#I think that if we're If we put our marketing right um we can sell this just like um I don't know if you've heard about it in the news  the the elderly mobile phone #0
D#So I haven't heard of it #0
A#Yeah #0
A#It's a big success #0
C#Yeah if we if we make a remote control just l with that idea in mind  we could make tons of money  I think #0
A#Very big success #0
A#Yeah #0
D#Hmm #0
A#Uh #0
B#Mm #0
C#We don't have to focus on on on the on the design then but on functionality #0
A#I think so as well #0
C#We just change our focus on the project  and I think we can uh we can sell this #0
A#Uh I simply think um uh that the new products we are gonna make  uh spef specifically design  are designed for uh younger people  uh so maybe we can focus ourself on the elderly people #0
A##0
A#And I think we have to um see what requirements we need for those um remote controls #0
A#'Cause what you told is the channel selection is important #0
A##0
D##0
D#Yes #0
A#Volume selection  power and teletext #0
B#Mm #0
D#Yes #0
B##0
A#Okay #0
A##0
B#Yeah #0
A#Um No we we haven't voted yet  so Uh I think teletext can be uh um can be a function as well #0
D#But obviously the board tends to disagree #0
B##0
A##0
A#But only if uh if it won't higher the the cost  because I don't know if it will be a lot more money to implement teletext as well  but I don't think it will be a problem #0
A#Or is teletext a Yeah #0
B#But um deaf people need uh teletext for uh for subtitles #0
D#Yeah  also #0
B#So it's Yeah #0
A#Yeah #0
A#So I suggest uh Yeah #0
D#I think it'd definitely be a bad idea not to include teletext #0
B#It's Mm #0
A#Is anybody um really against teletext #0
C#No #0
A#No #0
A#Just that  that we just keep the teletext #0
A#I think that's a good idea as well  especially for the subtitles #0
A#Maybe we can make that um another point of advantage in our remote control  if we uh make a k a button ex for example for big subtitles  which is instantly on the remote control #0
B#Yeah yeah #0
A#For elderly people they can think  oh I wanna have subtitles  and they push the button and they get the big subtitles #0
C#Uh that's a good idea #0
B#Yeah #0
B#Yeah #0
A#Um so I think teletext can v can be very useful in our advantage #0
A#Um Functionality should be few buttons  you said #0
D#Yes #0
A#I think uh that's very important we have a few buttons #0
B#Yeah #0
D#Mm-hmm #0
A#So to keep it simple #0
D#But I don't think that's really an issue any more 'cause Well might be #0
B#If it's only for televi Yeah #0
D#But I mean it  if it's only for T_V_ you're not gonna need a lot of buttons anyway #0
B#Yeah #0
D#You need a one to zero button  next channel  previous channel  volume up  volume down  and some teletext buttons but I think if you if you only l Nah #0
A#No #0
B#Yeah #0
A#Yeah #0
A#But do you need But do you need the buttons for one to zero #0
B#So we can s we can skip the display  so uh we don't need it #0
A#Maybe c we can Maybe we can use uh No  maybe we can implement the scroll button #0
B#Uh Yeah #0
D#Think if you're gonna include teletext you do #0
D#I think many people like to use that #0
D#'Cause if you should  if you want to switch from channel one to like thirty five  you don't wanna push the next channel button thirty five times #0
B#Yeah #1
A#Or a joystick like #0
B#Mm #0
A#There are other ways too #0
A#Just look if you look at telephones #0
B#Yeah #0
A#The Sony telephone has a scroll button which is very useful in searching names or That's right #0
B#Mm-hmm #0
B#Yeah #0
D#That's true but um I don't think there are many T_V_s that can switch channels that fast #0
D#And so you would need like the T_V_ would need an a function where you can actually view all channels and scroll through it #0
D#And I dunno if many channels would do have that #0
D#If many T_V_s have that #0
C#Yeah #0
B#Mm #0
C#And besides that it's um If we're gonna focus on elderly people they'll have to adapt #0
C#They're not used to using scroll buttons #0
C#So perhaps we should s stick to the basic layout #0
D#Mm-hmm #0
B#Mm #0
A#the numbers yeah #1
A#Yeah they can see how much buttons there are going to be on on the display  and if it's too much we can uh reconsider it #0
A#But I think there won't be very much buttons #0
B#Yeah #0
A#Or there don't have to be a lot #0
D#But I don't think I think if you're gonna make a remote control only to operate a T_V_  you there's not much you can gain on um having as few buttons as possible #0
D#'Cause I think there are pretty many remote controls that can only operate a T_V_  which already only have the minimum number of buttons #0
D#I don't think there's much to be gained in that area #0
A#The number of buttons #0
B#Hmm #0
D#Yeah #0
A#I think it's very important in the in the design #0
A#You can make a very fancy design uh with putting the buttons on the right places #0
A#And if you have less buttons you can do a lot more with To operate only the T_V_ yeah #0
D#That is true but I think there's simply not much to gain on the competition when you when you're making a remote control only for to operate only the T_V_ #0
D#'Cause if you have a a remote control only to operate a T_V_ there's simply not a lot of buttons required #0
D#There's not a lot of functions required so most existing remote controls simply don't have a lot of buttons either #0
A#No #0
B#No #0
A#So #0
D#So I think it would be very hard to actually gain on the competition here #0
A#'Kay #0
A#So we can Yeah #0
D#That would that would cost a a big marketing expedition which was one of the arguments to make it only for the T_V_ because we didn't have the time to market a lot #0
A#That's right #0
A#Yeah #1
A#So you suggest we could better um focus on for example the docking station #0
B##0
A#Uh uh like other functions #0
A#Instead of f of less buttons #0
D#Maybe #0
D#Well yeah I think  mean we obviously need a good way to position all the buttons and But I don't think we should spend very much time in that #0
A#Mm #0
A#No #0
A#Do you think the docking station will uh is allowed in the budget we have #0
C#It should be possible yes #0
A#'Cause it can be No #0
C#If it's not too fancy #0
B#No #0
C#And if the remote stays rather small  it should be possible yeah #0
A#Yeah #0
B#No #0
A#Because I think that's uh That's a good advantage point as well #0
A#If we have a fancy-looking docking station or very That's a nice requirement #0
C#Yes #0
B#Hmm #0
A#Docking station #0
D##1
C#So we're just gonna focus on the extras #0
A#I think so #0
C#Yeah #0
B#Mm #0
D#Yeah I think maybe we should do some research into what elderly people like to have in a like to have extra in a new remote control #0
A#Yeah #0
A#That's a good point #0
A#Um You said they easily get lost as well #0
D#Yes well fifty percent of the people indicated that remote control tended to get lost #0
A#Yeah #0
A#So maybe we should implement the audio sign  or something #0
D#Yeah that was what I suggested #0
A#Yeah #0
C#Like with your key-chain  if you whistle it goes uh it makes a sound #0
D#You have it on Yeah you have it's on some phones too  which have a docking station #0
A#Yeah #0
B#Hm #0
C#Yeah #0
D#And you just press a button and the phone goes ringing #0
A#Yeah #0
A#So audio signal should be possible as well #0
D#So you know where it is #0
A#I think it's not too expensive #0
B#No #0
A#Uh another point is the L_C_D_ screen #0
A#Um I don't know if that will rise the cost too much  because Yeah #0
C#Y i um I think we'll have to choose between the docking station or the screen  'cause it's uh Well I don't think they have different television sets uh in uh every country #0
B#Mm #0
A#It will be too much as well #0
D#I think since a lot of people indicated that a new remote control is hard to learn  and we're focusing on elderly people here which tend to have a hard time understanding new devices  it might be a good idea to have just a little screen on it  which would explain a button if you press it #0
D#Which would tell you what it does #0
A#Yeah #1
D#And it wouldn't have to be touchscreen or a very expensive screen  but Just a small screen with two Yeah #0
A#Based #0
B#Okay #0
B#Yeah #0
A#Just the L_C_D_ #0
A#Oh just the normal screen #0
A#That's a good idea #0
A#So Some extra info #0
A#Feedback #0
A#Yeah #0
B#Yeah #0
A#I think that's a good idea as well #0
D#But I dunno if that would that would fit into the costs #1
A#As the small screen #0
A#Extra button info #0
A#I think that should be possible as well #0
A#Um let's see what did we say #0
A##0
A#Mm #0
A#More #0
A#Should be fancy to  fancy design  easy to learn #0
A#Few buttons  we talked about that #0
A#Docking station  L_C_D_ #0
A#Um general functions Yeah #0
A#'Kay #0
A#And default materials #0
A#I think that's a good idea as well  because um elderly people don't mind if it's a titanium cover or just a plastic one #0
B##0
A#So that doesn't really matter #0
D#No #0
B#Mm #0
A#So I think we nee Uh let's um specify the target group #0
D#I think probably elderly people would be a little bit more careful with their remote controls than youngsters #0
A#Because are we talking about elderly elderly people or people from forty to eighty #0
A#Because I think what we're going to design now is for people above sixty  maybe #0
B#Mm #0
D#Yeah #0
B#Yeah #0
A#Uh what do we want #0
D#I think I think that would be a If we should do something like that it would be a  I think it would be really good for uh for the image of the company #0
A#If we want um a with uh for example the the telephone for elderly people  we can target the real elderly people #0
A#Yeah #0
D#And I think  I think there would be a good market for it #0
A#So that's the Now you're talking about sixty to eighty for example #0
D#If we're able to really bring an innovative product #0
D#Yeah the really But I'd have to look into that a little more #0
A#Yeah #0
B#Mm #0
A#Sixty #0
A#Okay #0
A#And different cultures #0
A#Are we Okay #0
B#Mm #0
A#No #0
B#No #0
C#'Cause Yeah #0
A#We've got five minutes left just now #0
B#So 'Kay #0
A##0
D#'Kay #0
A#Small warning #0
D#And with uh the little screen in it  which explains the buttons #0
A#Should Yeah #0
D#You could I think we n it would be a lot easier to adapt it to different cultures #0
A#In different languages  you know #0
D#Yeah #0
A#Yeah #0
B#Yeah #0
A#or you have to put a language button in it  but that will be a bit unnecessary I think #0
B#Right #0
B#No #0
B#Yeah #0
D#Yeah #0
A#It's better to put it on different markets with it all #1
B#Yeah #0
C#Yeah #0
A#Okay #0
A#So that's the the target #0
A#Uh then a few small things #0
D##1
A#Uh okay #0
A#I will put the minutes in the project uh project document folder #0
A#Um what we're going to do for the next meeting is the Industrial Designer will do the components concept  User Interface Designer the user interface concept  and the trend-watching #0
A##0
A#So just keep in mind the things we've said about the target group  uh requirements  and the trends which are uh going on #0
A##0
A#And uh specific instructions will be sent to you by uh the personal coach #0
D#'Kay #0
A#So um I thank you for this meeting #0
A#And I think we have a lunch-break now #0
D#That's good #0
B#Yeah #0
A#So that's a good thing #0
D##1

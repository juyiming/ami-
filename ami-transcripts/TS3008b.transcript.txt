B#Here we go again #0
C##0
C#My mouse is not working anymore #0
B##0
B#Oh #0
D#Oh #0
C#He's uh when I put it in  is is going to beep beep beep #0
C##0
B#Okay #0
D#Oh  I got a nice little screen here over here #0
B##0
D#I got like this big black border uh on every side #0
C##0
A#Okay #0
B#Mm  okay #0
D##0
A#Everybody ready #0
D#I'll I'll fix it #0
B#Yeah  it's okay #0
D#Yeah  whatever #1
A#Welcome at the functional design meeting  again presented by Maarten #0
D##0
B##0
A#Uh this is the agenda  the opening #0
A#Uh  we've got three presentations #0
A#And I'm gonna show you some of the new projects requirements that were sent to me #0
A#And we're gonna make a decision on the remote control functions #0
A#We have uh forty minutes #0
D#Yes #0
A#Oh  well this is the the closing already #0
B##0
A##0
A#So uh well we start off with the th the first presentation then #0
A#Uh  I think um in uh we have to do it in uh in right order #0
B#Okay #0
A#Maybe the Well  it Oh that #0
D#I don't know what the right order is #0
D#So Mm-hmm #0
B#No #0
C#Huh #0
A#It won't doesn't Maybe we should start with the the technical functions #1
C#Okay  how can I get this on the whiteboard #0
A#Yeah #0
A#Well it's you dumped the file in the uh in the sh in the project document folder #0
B#In project #0
C#Okay  I've done that #0
A#You've already done that #0
B#No can that open #0
C#Yes #0
A#Well let's close this one #0
A#We'll just uh open a new one #0
B#Open it there #0
A#Uh  well #0
A#Yes #0
A#Uh-oh #0
A#New thing #0
A#Oh yeah  uh I have to say something #0
A#Uh  due to some uh technical problems I haven't uh digitized the last uh the meeting minutes #0
A#But I'll uh make sure that uh happens next time #0
B#Okay #0
B##0
B#About the get Okay #0
A#And I'll get this one uh in digital uh form too #1
C#'Kay  we're going to um uh talk about working design #0
C#Um  the method of the remote control is uh electrical energy  it activates a chip uh in the remote #0
C#It's an electrical circuit which compose uh messages in the form of uh uh infrared signals to control the television #0
C#Mm  it's a nowadays very uh known  a known uh uh technology #0
C#Um  the known technology can make a cost very low #0
C#Uh  it's a wild uh a wide sale uh of uh remote controls in the world #0
C#And and the components are very uh very cheap #0
C#Um  Uh  diodes  uh bat batteries and uh uh LED lights  they're needed and they're uh everywhere available #0
C#Uh  again  it's a fair price #0
C#It's a common uh technology uh  like I told um Uh  the circuit board  it's the most um important uh um part of the remote control #0
C#Uh  we can use for that uh fibreglass with copper wires  it's it is uh can be made as fast as printing paper #0
C#It's uh it's all very uh Yeah  they're making it uh all the time #0
C##0
A#Okay #0
C#Uh  and it's not uh very specialised uh technology #0
A#Okay #0
C##1
C#I haven't come to here  but um I've got uh some uh images of uh remote controls #0
C#They were not uh very uh trendy or just uh just a remote control like everyone knows #0
C#So I don't know uh why I should put it here #0
C#Uh Yes  but uh I uh haven't made it because uh of the time #0
A##0
B##0
A#Okay #0
C##0
A#But it's the technical side of the remote control #0
A#Oh #0
A#Okay #0
C#But What #0
C##0
A#Well  we'll we'll have to skip that part then #0
C##0
A#But you don't think it's a problem um to design uh the technical part of the remote control #0
C##0
C#No #0
A#It's gonna be easy #0
C#Yes #0
A#Okay #0
B#But nothing restricted for user interface #0
A#Yeah #0
B#With technical I don't know #0
C#Um  no  it's uh it's just a part of uh a known technology  yeah #0
A#M Okay #0
B#'Kay #0
C#Remote control is nothing special nowadays #0
A#R regardless of what type of functions we want to implement #0
B#Okay #0
A#Doesn't really matter #0
C#Um I don't think so  because of the all the televisions uh there are a few maybe a couple of televisions with the new functions  but is it useful to put them on a on a standard uh remote #0
B#But I kind of uh Yes #0
A#Okay #0
A#Yeah  okay #0
B#Uh Well  the technical functions #0
A#Okay #0
A#Well  we'll see #0
A#We'll see later on #1
B#Um  well I don't know if you got the same uh pictures as I got  but uh I got these two  and I think they're we have to focus uh on uh the uh one hand the expert view or the novice user #0
A#No #0
C#No #0
B##0
B#th I think it's it's very much uh depending on the user requirements  I don't s uh know who's doing t Yeah  but it I think that's very important to watch uh what kind of functions there we want to uh put in a remote control #0
A#Well  uh will there be some uh user requirements later on #0
A#The ones I I've uh received from the account manager #0
A##0
A#Yeah #0
A#Well  we'll keep this in mind  and then discuss it later on #0
B#Yeah  well y we can put functions in it when uh yeah  when we uh get the user requirements uh and we can update it #0
A#Uh-huh #0
A#Okay  but this real this uh big d uh distinction between this type of remote #0
A#we should we should choose one uh we should not compromise but uh really choose for uh expert viewer or novice v Yeah #0
D#Yes  I agree #0
D#Yeah #0
A#Well  what that's what you want trying to say #0
B#Well  yeah w if you want try a a a huge market  if you want to reach a huge market  uh like elderly people and we have to choose for novice user #0
A#Okay #0
A#Okay #0
B#But I don't know #0
B#It's it's really um depending on how how how far the the the remote controls are already in n um in use #0
A#Yeah  well some of these Uh  yeah #0
A##0
A#Well  some of that will Yeah  but i but it will be more clear when we come to the uh u uh some of the new requirements #0
B#Yeah  probably  yeah #0
D#Yes #0
A#So Mm-hmm #0
B#Uh well  there are l at least uh basic functions  uh like just th the channels uh one till nine  uh on and off switch  which must be clear with a red button or something like that #0
B#Um  most standard uh have volume  of course  and a mute function  and  of course  the next and previous channel #0
B#I think that's just basic what we need #0
B#And from that on we can user requirements what we need more #0
B#Uh Yeah  I just um um I thought Joost was looking at the trendy the trends in the markets  and I don't know if there uh are any um uh if you put more functions  more buttons  maybe it's com becoming less trendy or something like that #0
D#Yes #0
D##0
B#M you can just you can k you can keep it in mind that #0
D#I haven't really found a conclusion like that #0
B##0
B##0
D#Yeah #0
B#I don't know #0
B#Uh  I th I thought the the with less buttons you can make a more trendier uh yeah  more trendier design  I think #0
D#Design #0
B#I think #0
D#Sounds interesting #1
B#Yeah #0
A##0
D##0
B#Uh  well  that's all I have to say  I think #0
A#Okay #0
B#Yeah  that was it #0
D##0
D#Alright #1
A#Well  then the Marketing expert can uh tell us something about the current market #0
D##0
D#Yes #0
D#Yeah #0
B##0
D#It's alright #0
D#Um Alright  I've done some research for functional requirements #0
D#Um yes #0
D#The working method um there were hundred uh uh w h one hundred people  uh how do you say uh  f watched using remote controls in the usability lab and they also uh filled out a questionnaire uh with a few questions #0
D##0
D#Uh  I've lined them up here #0
D#Uh  ask whether um common remote control looks good or not  about willingness to spend money on remote control  about zapping behaviour  and uh and stuff like that #0
D#I uh have found some interesting things #0
D#We do we do got a market #0
D#Um  three out of four people claim m uh to find remote controls ugly #0
D##0
D#So if we make a trendy design  we sure have seventy fi seventy five percent of the market  which you can reach #0
D#Um  three out of four users uh zaps a lot  as I uh quoted here from the uh results #0
D#Zap buttons are used one hundred and sixty eight times per hour #0
D##0
D#That's quite a lot #0
D#Um  relevant options are  of course  power buttons #0
D##0
D#Although  only used once per hour #0
D#Uh  channel selection  volume and buttons for text  and the more um  yeah  other functions  like audio settings  video settings  sound settings are not said to be very important and uh very much used #0
D#Furthermore  fifty percent says uh they only use ten percent of the buttons on a remote control #0
D#That doesn't say we got we can leave ninety percent off #0
D#But it sure um says we shouldn't make it too uh complicated #0
D#Fifty percent also claims uh to have lost a remote control very often in the room #0
D#And um an important thing here  the most important customers uh  which is over seventy percent of our market  is in the age range of thirty six to sixty five years old #0
D##0
D#And uh elderly people  our market  are less interested in uh nice features  but more willingly to spend more money on remote controls #1
D#So  what I was thinking oh  wrong side #0
D##0
D#We shouldn't implement too much features on uh on our remote control  because elderly people will get th lost #0
D#Group features for a higher usability  uh what I was claiming in the previous meeting #0
D#Um  all the settings  about audio settings  video settings and channel settings  which are not very often used  we could group them uh on one button and make them accessible uh in one menu button or whatever  because they are used very rarely and well  it uh there are a lot of options there  so we can really make uh yeah  how do you say  we can spare at buttons over there #0
A#Okay #0
D#And um  if you want to implement V_C_R_ and D_V_D_ options  group them in the button  not too uh Yeah #0
D##0
D##0
D#Small buttons  so they won't be very um  how do you say Yes  won't be very present  thank you #0
A#Visually presents #0
D#And a trendy look  well uh  although seventy percent of the market is uh consists of elderly people uh who don't really care for trendy looks or whatever  I guess it can do no harm to make it trendy for the other thirty percent #0
D#That was kind of what I found #1
A#Okay #0
B#Okay #0
D##0
D#Okay #0
A#Well  then we I'm gonna show you some of the new project requirements and then we gonna discuss on uh what features we find important #0
D##0
D#Yes #0
A#Uh  well some of the uh new requirements make some of your findings quite uh irrelevant  I think #0
A##0
D##0
A#Because um uh s decided to put They have decide to put two additional requirements forward #0
A#Well  now I see four #0
D#Two #0
A#That's kinda strange #0
D##0
A#Well  they say tele teletext becomes outdated since the popularity of the internet #0
A#Well  I think that may be so  but well  we can't just leave the teletext button off #0
B#Well No way #0
D##0
D#No uh  I agree  I agree #0
A#It's impossible  I think #0
A#So the compromise we could make is just to make one teletext button  you know  like on and off  and don't make a lot of special put a lot of special features on it to make it transparent or You know  it's just you want you want to be able to make use of teletext  but not to play with it that much #0
D#Not too much  no #0
D#Yeah #0
A#So we have to think of that #0
A#The remote control should only be used for television #0
A#Otherwise  the project becomes more complex  which endangers the time t t ma uh the time to market #0
A#So maybe we should leave all D_V_D_ and V_C_R_ related features off completely #0
B#Yeah #0
D##0
D#Okay #0
A#I don't know #0
A#I think that uh that's what they're trying to say #0
D##0
A#Uh  our current customers are within the age group of forty plus #0
A#New products should reach a new market with customers that are younger than forty #0
A#So you uh talked about the elderly who were willing to spend more on a remote control and who were interested #0
D#Mm-hmm #0
A#But  well  they're not relevant because we are aiming at a younger Yeah  but it is it's is a dif it's a fact that the th th that bigger market you're talking about  we already cover that #0
D##0
D#I don't really agree actually  to be honest #0
D#It's a very small market which we will approach then if we uh want to reach customers younger than forty #0
D#It's only like thirty percent of the total market #0
D#Mayb yeah #0
A#Our company already sells remote control to the older people  but we we also want  you know  a new customer group #0
A##0
A#That's the one we haven't covered yet #0
A#So I think that's what the problem is #0
D##0
A#We haven't got remote controls for uh Well I think  yeah #0
D#Okay #0
D#Maybe maybe we can compromise a little bit #0
A#Yeah  I think so #0
A#Maybe if it's no no  but I think we have to just keep in mind what the older age group wants #0
D#Not too much then  bu alright #0
A#So maybe we can make a remote control that's primarily interesting for the younger group  but isn't that bad for an older person either #0
D#Yes #0
A#Uh  our corporate image sh should stay recognisable in our products #0
A#Our product's corporate colour and slogan must be implemented in the new design #0
A#Okay  something else nice to know #0
A##0
B#But what's our slogan #0
A#Sorry #0
A#Yeah  you will have to look that up #0
B#The slogan uh Puts fashion in electronics #0
A##0
B##0
D##0
A##0
D#Yeah  I'll have a look #0
A##0
D#We put the fashion in electronics #0
A#I think it's something about the Oh  okay #0
B##0
A#I thought it w might be  let's make things better or something  but Okay well  let's go back to the the agenda #0
A##0
B##0
D##0
D#Sense and simplicity #0
B#Sense simplicity #0
B##0
D##1
A##0
A#So we've now had to the three presentations #0
A#We know about the new project requirements #0
A#That means we can uh well d yeah  discuss on the remote control functions #0
A#Well  if I can uh make a start  I think it's becoming more clear what kind of remote control it's gonna be  and I already talked about the maybe you have a f familiar with the rem remote control that has the the can opener underneath it #0
A#I think we're we're looking for some Yeah  we we're looking for a really simple remote control with only basic T_V_ functions #0
D##0
A#Y well  that Yeah #0
B#Yeah #0
D#Well  to be honest  if um our uh aim group is uh till forty  not older than forty  maybe that's not very uh yeah  we don't really need to have a simple remote control #0
A#Yeah #0
D#I think we can implement more functions then  because um basically uh the younger people are more able to adapt to new technology and therefore will be a more Yes #0
B#Yeah  but wha But Yeah  but you sai you said that that a lot of functions aren't used #0
D##0
A#Yeah  yeah #0
A#M yeah  that's why um well a lot of um the use the requirements the the account manager sent me  I think they are are c are contradicting each other  because they want a simpler design  and no uh other uh s functions than just T_V_  but they s do aim at a younger Well  ma But I think uh you we can make some discuss uh distinctions in uh what kin in the  know  th th in functions you have Y Well you have different kind of uh equipment in your room  like a t T_V_ and a D_V_D_ player #0
D##0
D#Yeah #0
B#So why should j we put this function in #0
D##0
D#Well  I think Yes #0
B#I think more I think uh people younger people are more looking for just a trendy look than uh more functions #0
C##0
A#You can uh  know  you you can make very d you can put very detailed functions regarding the T_V_ set on your remote control uh with the with uh the  you know  audio settings and uh v uh screen settings #0
A##0
A#We don't want that #0
A#I think that was that became clear #0
A#We don't want #0
A#But w maybe we should put some func uh  I know that the younger people will most likely have a D_V_D_ player they want to  you know  they want to uh control  remotely #0
D#Yeah  control #0
B#Yeah  but uh you said Yeah  d yeah  but th the functions are not in the remote control we're making #0
D##0
A#No  yeah  th th the user requirements of the the The new project requirements told us not to But maybe w Yeah #0
B#It's n Yeah #0
B#Yeah #0
A#I think we maybe should Yeah  well we should uh put some functions for other maybe for other equipment on it #0
A##0
A#But just the basic functions #0
A#Maybe like rewind and wind  or n what d what do you guys think #0
C#But you can put them under the same button #0
D#Not much more than that #0
D#Yep #0
A#Yeah  if as far as possible #0
D#Or we can u u we could put 'em behind the flip-flap or whatever #0
D#So t Yeah #0
A#But what do you think #0
A#Do th should we implement features that uh uh or functions that to control other devices #0
B#No #0
D##0
A#No  you don't think so #0
B#No  new requirements say no #0
A#Yeah  the new requirements say so #0
B#So Yeah  but I i if it's too simple uh th they won't use the remote control  they use their own th th with lot more functions #0
C#But you can put a play and stop and and rewind #0
A#Well  maybe it's maybe there there there is something th m most of the time these functions don't support the particular device as well as their their own remote control #0
D##0
A#You know  a lot of D_V_D_ players have some tricky settings with Yep  exactly #0
C#Yes  but we Huh #0
D#Y yes #0
A##0
B##0
A#That's that's wha No no  you don't No no  you don't need it #0
D#But but for for example  V_C_R_  that's better example in this case #0
D#I think on a remote control for television you don't need to be able to programme uh the V_C_R_ to start recording at three P_M_ or whatever  just play  stop  rewind and uh fast forward #0
B#No  no #0
A##0
B#Exactly #0
A#No  no #0
A#Okay  but we have to think uh w we have to think D_V_D_ I th uh  I guess  so um but uh from my experience it's kinda a lot of D_V_D_ players  you know  like forwarding  goes differently #0
D#Yeah  I know  but uh Yes #0
A##0
A#Uh  you get two speed or eight speed or sixteen speed #0
B#Mm  yeah #0
A#It's c sometimes a bit difficult #0
A#Maybe if we just leave the D_V_D_ functional m uh Well  I was thinking about putting it in  but concerning the project requirements and what you just said  I think we m should focus on the T_V_ then #0
D##0
B#Yeah  but just keep it simple and look more at th No #0
B##0
A#And uh and it's just an uh a complimentary remote and not a universal one #0
A#If you want to keep it simple  you can make a universal remote #0
B#It's only for television #0
A#Okay #0
B#So Yeah #0
C#Yes  but there are there are But how wi how will you be able to handle a whole market #0
A#It's just a s it it should be something that is like a gadget on your coffee table  and it's just for when you you you jump on the couch  you pick up the the the nice remote  the simple one  just to put on the television  nothing more #0
D#Yeah #0
B#Yeah  nothing more #0
A#Okay #0
B#Exactly #0
D#Alright #0
A#Um  but what televis Yeah  we we'll make w this one trendy #0
C#There are uh a dozen of uh dozens of of remote controls that have Yes  but you could put on the z on the zap buttons you can put it to uh to uh access all the same pages #0
B##0
C##0
B#Yeah #0
A#And simple #0
A##0
B#The user interface is easy #0
B##0
A#Well  we we will come to that  but ju first on the on the functions #0
A#So we should put uh zap buttons on it #0
A#Um  also numbers  to uh to go to the specific channels #0
B#And the basic yeah  basic functions  yeah #0
D##0
D#Yes  definitely  yeah #0
A#Uh  Okay  a t a teletext button should be there #0
B#It's too much integrated in the other #0
A#But just one big teletext button  on and off #0
B#Yeah #0
A#Probably #0
D#Yeah  and maybe two or three other options  but not nothing more than that #0
D#I think stop function is very useful #0
D#If you have a uh a page which consists of more pages  and you are not a very quick reader  then I think it's very irritating if the next page shows up  but Well  I use it very regularly  the action #0
A#Oh  okay #0
A#Yeah  but uh I think that becomes too difficult  it's not a very common function and people will have to read up on their remote then #0
D##0
D#I re I use it quite often #0
A#Yeah  yeah  but maybe you s yeah yeah  maybe y you do  but I've never heard of it in the first place #0
D##0
B#Will you look Look at the market #0
D##0
A#And we have to well t yeah  and t and teletext is becoming outdated #0
B##0
C##0
A#We just want to see what programmes are on and what time it is probably #0
A#Yeah #0
A#I don't know #0
A#I think that that's kin getting too complex for our remote #0
B#Well Yeah  I uh  it's Sunday I always use it for the uh yeah  for the soccer uh Well  uh when you uh uh when you look uh for example um  a couple of weeks ago I looked at the for the flights  and there are a lot of flights in one page  so if if th Yeah  but True #0
A#I don't know what you th guys think #0
D#Might be #0
D#Might be #0
A#Yeah  but do you like to have a such a s stop button #0
A#Or do you think it uh I think it's a kind of uh uh very rare and special function #0
A#Yeah #0
A#Okay  it goes Yeah  but that's kind of stuff we should do on the internet right now #0
A#That's why it was uh said in the in the use in the r new requirements #0
A##0
B##0
C#If you have seven pages  you can go up and down #0
A#Well yeah uh  lots of new televisions can store pages  you know  and then you can just skip manually through them using I think we should just put one teletext button on it #0
C#Yes #0
A#Then we meet uh the new requirements #0
A#we also meet the other thin y you sh you just re we have to choose for the the simple design  I guess #0
B#A simple yeah #0
A#Okay #0
A#Um  well  what functions do we have to decide on #0
A#Or do we uh I dunno if we have t stif specifically name all the functions we n we want #0
A#We have the zap and uh the volume #0
A#Should we do m make them very big #0
A#The the the zap button #0
A#D d Th that's that's that's considered to be trendy also #0
D#I think the plus and the minus button should be uh quite present  yep #0
C#Yes #0
B#But trendy  yeah #0
B#I don't know #0
A#Or maybe you should place them on a uh  in a special way #0
C#Maybe we can make uh a kind of a joystick #0
A#Yeah  something or uh somethin special way to to zap through the it has to s it has to be yeah  and quick #0
A##0
A##0
A##0
B#Original #0
A#You have to use it very quickly #0
B#It was uh True #0
D#Yep #0
D##0
D#If you grab the remote  your hands should be on top of the plus #0
A#Yeah  and it the buttons should make it um possible to to zap through your channels in a rapid at a rapid pace #0
D#Yes #0
A#Oh  what should we decide on then #0
A#I think in a in a case of this simple remote control  the technical aspects which uh weren't worked out already  but it w shouldn't be a problem then #0
C#But No  it's o just signals uh and the television d uh does the rest #0
B#Yeah #0
A#Yep #0
B#Maybe uh Uh maybe we uh uh the batteries maybe #0
A#No  okay  but we don't have to uh  when we don't want to uh control other devices  I think it makes it even more simple #0
C#No #0
B#If you use large batteries or small batter batteries #0
C#The most standard batteries #0
A#I think I think that we should use uh d yeah  not not uh the b the watch kind th the most uh Well  it has to be simple  and I wi Which are most likely to be found somewhere in the house  you know #0
D#I think double A_ #0
B#Yeah #0
B##0
D#Yep #0
B#Yeah #0
D#The most ordinary uh batteries #0
B#Okay #0
D#Yep #0
A#Oh #0
A#How much time do we ha we have left uh #0
D##0
A#M m m more than thirty minutes #0
D#I think about twenty minutes #0
A#Uh ten twenty minutes #0
C#But i in a way we have to be uh uh special #0
A#Well  uh these these shouldn't Yeah #0
A##0
B#Early break #0
A#Yeah  okay #0
A#But that's that's Do you uh have you have you think about tha thought about that #0
A#How we can what the extra touch can be #0
C#Maybe th m For the remote control #0
A#Do you suggest design or the shape or some gadget f f kind of feature or Well  it was something about how we lose them #0
A#Maybe it should be a remote control when you you clap you hands it makes some noise or some gadget kind of thing #0
D##0
D#I think that's n that's more for a for an age range or uh ten to twelve or whatever #0
B#To find him #0
A#Yeah #0
B#That's maybe yeah #0
A##0
D##0
C##0
B##0
A#I don't know #0
A#I don't know #0
D##0
A#Nah  um a lot of people like to have such kind kind of kind of gay kind of things #0
A#It w it w should be like a birthday present or something that you give someone  and it is i has something nice #0
D#Yeah  yeah  that's good one  yep #0
B#Yeah #0
A#Or maybe it w should have a big uh light that can flash or something on it  or maybe it should or an or the like the the can opener #0
A##0
A#Maybe it contains some feature that you don't normally link to a remote control #0
A#I think it's very impor because we're gonna make such a basic remote control  we have to do something to make it special #0
B#Yeah #0
A#It's gonna cost twenty five Euros #0
D##0
D#Ye I think the can opener i is a brilliant idea actually  because television and beer is not a rare combination #0
A#Yeah  but the well  it's already been done #0
B#Yeah  but that's yeah #0
A#Nah #0
A#Yeah  that's true #0
A##0
D##0
A#But and I think it's gonna be uh very uh it has to be sturdy or something  so maybe with with bouncing pads so that you can just throw it on the floor or something #0
D#Yes #0
A#it has to be used something special  and you really it has to  you know y not s people  when they buy it  they have to think  well this one lasts for a long time #0
B#Yeah #0
D#Yep #0
A##0
A#We're really gonna use them #0
A#Not some thing you you throw away next week  you know #0
D#No  that's true #0
A#So maybe uh that's i I think that's when uh when we decide on these type of functions  know  basic functions  uh it's very important to find something like this #0
A##0
A#So there's a very important task for you #0
A#And maybe we can all think about it #0
B#Be original  yeah #0
A#Uh  also for you maybe  when t you it's very nice when you can be entra when you can be trendy  and and uh and al as in a friend use friendly as well  you know #0
B#Mm-hmm #0
B#And use friendly  yeah #0
A#So big buttons  flashy design  and maybe some kind of gadget kind of thing #0
B#Yeah #0
D#Yeah #0
B##0
B#Must brain-storm #0
A#Uh-huh #0
D#Yeah  a swapable front or whatever #0
A#Yeah  or just different colours would be uh I don't know if people also wanna spend more money on fronts for their uh remote control #0
B#Well Why not #0
A#It could be be Yeah  you never know  but But it and I think we have to make it quite big #0
B##0
D#Yeah #0
C##0
B#More money for us #0
D##0
D##0
B#Yeah #0
B#Quite big #0
D#Yes  definitely  definitely #0
B#Yeah  you think #0
A#Yeah  people So  and and also because uh it is expensive #0
C#That's to be uh a formed for your hand #0
A#If you want it to be something  you know  it's ha doesn't have much functions want to be you don't want to get it l make it Mm-hmm #0
D##0
D#Yes  it it it should be f be visible nearly anywhere in the room #0
D##0
D#As I uh as I said during my presentation  fifty percent uh o Yeah #0
A#And shou and should ni look nice when you put it on a table #0
A#I I think you m might wanna put it uh yeah  that it it it it stands up #0
B#A standard or something #0
A##0
A#Yeah  you have to put it on its So it's like a vase or uh something you put on a table #0
D#A face #0
D#Or uh yeah  yeah #0
A#no no  put stuff inside it #0
D#Yeah #0
C#More like a joystick then #0
B##0
A#But  it's like like a statue or something Yeah yeah  but yeah  but you also can put it somewhere near the window in That it's it's fashionable #0
D#Yeah  yeah  I see what you mean  yep #0
B#It's like you have uh four phones #0
B#Something like that #0
D#If you do that  but I don't know if that's possible within the production cost of twelve and a half Euros #0
B##0
A#I I don't Oh  yeah #0
D#I in in the base we could like make uh a button  and if you push it  the remote control itself s makes noise #0
D##0
D#That's probably stupid  but uh as I found here uh  fifty percent  was it fifty #0
C#But that's that's fun for the first time  and then the second Or you can Yeah #0
D#Uh fifty percent fifty percent often loses remote control #0
A#Yeah  but but when you when it gets lost  how can you press the button to make it Oh  okay #0
A##0
D#No uh  of the base the the the the the the the thing you put it in #0
A##0
B#On the television #0
B#Oh  like this #0
A##0
A#Uh  that's kind of nice #0
D#If an a button in in that uh Yeah  you can ma make rechargeable one  yeah #0
B##0
A#And then also you don't even need batteries  because you can make it uh chargeable #0
B#A char chargeable #0
D##0
B##0
A##0
B#Yeah #0
C##0
A#Yeah  that w yeah  but yeah  the pro No  well I think that it might be t p Well  nee but we don't Yeah #0
D#Why not #0
B#Why not #0
A#Maybe you  but we don't know much about production cost  but when you you can imagine that when you spend twenty five Euros on a remote control and it's a basic remote control  then the then the money there must be money to spend on that kind of st you know  rechargeable units #0
A##0
C#With recharger #0
B##0
D#It should only cost twelve and a half Euros  of course #0
D#Aye #0
A#Yeah  but we would d ma we'd do it in Taiwan and So  it's not gonna be that expen Uh I I think it's a great idea #0
B#Production #0
A##0
B##0
D##0
D#Yeah  okay #0
B#It should be possible #0
D##0
B#I think it's a good idea #0
D#Yeah #0
A#S some kind of be I've never seen that before  and you make it uh um be uh  you see it with uh the mo the mouses nowadays #0
B#To make a base or something #0
D#Yes #0
D#Yes  definitely #0
D#Yeah #0
C#Yes  but is that handy #0
A#Well  I well it's really ch you can recharge it  so you ha never have the battery problem #0
B#It's it's it's it's it's not the purpose to be handy  it's Mm #0
A#That's one #0
A#And uh you can always find your remote control up Uh  okay #0
C#But but remote controls remote controls nowadays can can last uh two years  three years  with with t two batteries #0
A#Well  maybe yeah  you could when that's when it's too costly  you could probably skip the recharger  but you will you do need uh also an uh  also you would need a battery in the the base unit as well  you know #0
C#And then you Yes #0
A#Does it makes it kinda Yeah  yeah #0
D#Well y you you could connect that to two hundred to twenty volts  of course #0
C#Or But you pay for it #0
D#I don't know #0
A#Okay #0
B#Yeah  but that's not it's ugly  I think #0
A#Yeah #0
A#Yeah  but then it's very easy to make it also a rechargeab I don't think that is gonna cost much to make it also a recharge function in it #0
B#No #0
D#On the other hand  if you don't do it  we can also make a nice bay #0
D#I mean  it looks trendy and still still put a bleep function in it  but um I think the bay is definitely uh Sure  why not #0
D##0
A##0
A#Yeah #0
A#I think it's a good idea #0
A#And make it  you know  we we um Well  we uh it's it isn't a t a most uh costly uh remote control #0
A#We can save on the on the functions #0
A#We just put some simple button in  make it big and sturdy  nothing more  and just make s sure there's some noise that it can make  or probably some kind of cheap light thing around it or that it uh that it lights up  it's also nice #0
A##0
D#Yep #0
A#And if you put it away  I think it's uh w we have to we uh that's uh it's not a easy market #0
D#Yeah #0
B#Yeah #0
A#We have to something special #0
D#Yeah #0
A#And for twenty five Euros people want something remote c special from your mote control  and we can't deliver that in r with uh regards to the functions  because we aren't gonna put Yeah #0
B#True #0
B#Yeah #0
D#Definitely #0
D#With eye candy  ear candy  whatever #0
D#Yeah  definitely #0
A#And then uh when make it  you know  nice looking shape and this and then you also you got the stand-up thing #0
A#Yeah #0
A#I think I think it's a good idea #0
D#Yeah #0
B#Yeah  it must be must be a gadget to have #0
A##0
A#Yeah #0
D#Definitely  yeah #0
A#Oh  if it let's well  we will see what's possible concerning the the costs  and if it's possible we'll do that #0
B#Yeah #0
A#And we even try to save up on other stuff to make sure we can do such a thing #0
A#And the first thing we the most likeable thing to to n to skip is then probably the recharge function or something #0
A#If that's too expensive  we won't do that #0
B#Yeah  we c Or just give a beep when the battery's out or uh down #0
A#But it would be nice #0
D##0
D#It would be nice  yes #0
A#It's the idea #0
A#I know that batteries last long nowadays #0
A#And and what people just think about  well  I'll never have to buy any r batteries again  so y b because it's very annoying when your battery is empty #0
A#And you know then when you haven't batteries around  and probably for two weeks  your remo I've experienced that that Yeah  okay #0
D#Most televisions break down before the battery pack is empty  so yes #0
D##0
A#So  easy functions #0
A#Well  we will we will I think we'll work that out  zapping  numbers on it  bi Yeah #0
D#Yeah  why not #0
A#But it's also annoying #0
B##0
A#'cause as long as it stays as it ke keeps working  you're not very motivated to do something about it #0
D#Hmm #0
B#Yeah #0
B#true #0
A#Then it beeps all the time and #0
D#No  that's true #0
A#You don't want to have ever have those problems  and you won't have if if you have the rechargeable #0
A#And you don't have to use the unit  you can also put it on the side if people don't like it #0
A#Uh  i i in the in the ma Yeah  but it w I mean  if if they pay for it because they think  oh  that's a great idea  I'm gonna use it #0
D#Why not #0
C##0
A#And when it  you know  when time goes by and they think  well  I'll never put him in the recharger  I think last long enough  then they put it on side and they can use it now and then #0
A#Then when they look get m I I I know for sure that everybody who buys this remote control  within a couple of months of they will be in the situation that they they're seeking for the remote control  they wanna see something quick and uh just push the button and th uh  I think it's brilliant #0
B#Yeah #0
D#Bleep bleep bleep  oh there it is  yeah #0
A#Yeah  I've never it's so simple  but I've never seen it #0
B#Yeah #0
D#Yeah #0
D#No #0
B#And you can leave it just there #0
D#Nearly #0
A#M maybe we should really do this #0
A##0
D##0
B##0
B#Yeah #0
D#Yeah #0
D##0
A#Okay now  well  how much time have we got left #0
A##0
A#These clocks aren't uh synched #0
D##0
B##0
B#Yes #0
B##0
A#Oh  now I've put uh well  it is twenty p Okay  so we have ten minutes or something #0
B#Yeah  I'll Uh fifteen minutes #0
D#Something like that  yeah #1
A#Yeah  but we're uh we're done #0
A#I think #0
A#We've decided on the functions #0
A#Well  there is some oh #0
A#There is a closing sheet #0
A#We have lunch break  and then we have thirty minutes of individual work #0
B##0
D#Oh #0
A#Oh okay  I'll make sure I'll I had some problems with uh the digitising the the first minutes  the the s the next minutes won't be a problem  but I'll try to make sure the first one will be in the folder too  but maybe it won't work  but you'll see #0
A##0
D##0
A#I think these are more important than the first ones  so #0
D#We'll see #0
B#Can you make an uh uh a part of f folder for the minutes maybe #0
B#That not not everything in one one uh folder #0
D#Maarten  five minutes #0
A#Oh  five minutes #0
A#Yeah  because uh I I d I did uh the first minutes I did were were were a bit scratchy  you know #0
A#Then I did a s second one with a nicer layout  which I could uh  know  use for the other ones well  but uh I d think uh I forgot to do put done under the first one  and when you go write a second uh it's get it's not working when you try to write second uh paper or something #0
B#Yeah #0
A##0
B#Maybe #0
D#No  that's true uh  yeah #1
A#And then you you had to overwrite it or someth I don't know #0
A#Becau I d uh  it was not my uh pen #0
D#Should we by the way draw um on our nice whiteboard  um a little uh idea of yeah #0
A#this kind of looks you like #0
A#Of the shape #0
B#Or the sh Do you get an idea of the shape #0
A#Yeah  probably  it would be nice #0
D#I dunno #0
D#Has anyone got um a little bit detailed ideas about the shape #0
B##0
D##0
D#I don't  for one #0
C#Maybe like this pen #0
D#A bit bigger I guess  but The shape is nice  it's um something different  and we want we want that #0
A#No  bigger #0
D##0
B#A little bit bigger  yeah but Oh  uh look uh look at the pictures #0
C#It has to feel nice in your hand #0
A#Well  I I I have to say  I have this uh can opening remote control in my head most of all  or I think some maybe we should no  that will be too costly #0
D#Yeah #0
A##0
B##0
A#We shou we could also  that was a would also be an idea  but I don't think it I don't know if it exists already  you should like make Alessi or something design it #0
D##0
D#Okay  yeah #0
A#That would also be nice #0
A#But that's gonna then you c then you don't Yeah  but then you don't have t yeah it that's not something i that's in the production cost a one it's a one time  you know s was it's a single cost #0
D#Yeah  but twelve and a half Euros #0
D##0
D#Uh Yeah #0
D##0
D##0
A#Yeah  m but but then you can nah  I don't thin I think that it would be more expensive  because I've bought the Alessi stuff more often and even small pencil holders or something are more expensive #0
D#Yeah  that's true #0
A#Would be a nice idea though #0
A#I don't know #0
A#I think it uh has to be a r it has to have round forms or something #0
A#Like something like that or so or so And on th and then uh s a base unit underneath it #0
D#Something like that is very ergonomic #0
D#So Yeah #0
A#It's also round #0
A#Put it in there uh wire on it #0
D##0
A#Maybe uh  I don't know  some some lights  a big but well #0
B#Yeah  flash lights at the side #0
A#Volume and programme  yeah #0
B#At the side  or something like that #0
D#Yes  volume and programme should be there I guess  because you hands wi uh y your hands will be in the smaller part #0
A#And some of the extra funct Some of the extra functions over here #0
B#Yeah #0
D#Yeah  and the numbers on top  I guess #0
A#Numbers #0
B#Yeah #0
A#And and lights #0
A#How we're g well  maybe uh s a ring of no  no  you have to Maybe on the side of it #0
D#Maybe ro roun rounds uh uh l sorry #0
B#Yeah  side of it #0
A#Along the side uh strip of yeah #0
B#Just two LEDs or something on the side #0
D#Maybe lights also around the volume and the p the plus minus programme buttons #0
B#Yeah #0
A#Yeah  but I also meant the the blinking li w you know  the ones that also blink when you try to uh uh locate your remote #0
D#Oh #0
D#Okay #0
B#Yeah #0
D#Yeah #0
A#Well  theys have to be Yeah #0
B#Well  uh probably at the side #0
B#You know look at the front  but Yeah  exactly  and then there is yeah #0
A#Yeah  yeah #0
A#Exactly #0
A#When you you see it from the side  then it would look just like that #0
A#And then you have a strip of uh lights or something #0
B#Yeah  something like that  yeah #0
D#Okay  yeah #0
A#Well  uh I think it's nice  for one thing #0
A#maybe put something on top of it or  you know  like that's looks funny #0
A##0
B##0
D##0
B#No #0
A#I don't know #0
B#No #0
B##0
A#Or some bump #0
D#I think I think that'll be too big tha too big then #0
A#Maybe some Yeah #0
B#Bumper or something #0
A#We'll have to think about it #1
A#I think we're we're done #0
D#Yeah #0
B#Yeah #0
D##0
D#Yes  we are #0
A#We can save this one #0
D##0
B#Lunch break #0
D#Alright #0
D#Yes  I guess it's lunch time #0
B#Okay then #0
A#Mm mm #0
D##0
D#I don't know #0
D#Half and hour #0
D#I thought our next uh next individual round was half an hour #0
B#Okay  five uh Oh #0
A#Yeah  that was what uh Mm  we'll hear about it #0
D#I don't know about the lunch break #0
D#Well #0
B##0
B##1

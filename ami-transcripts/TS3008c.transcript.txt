A#Okay  all set #0
A#Welcome to the conceptual design meeting #0
B#Uh  okay #0
C#Yes #0
A#The agenda #0
A#The opening #0
A#I'll again be the secretary and make minutes  take minutes  uh and it will be three presentations  just like the last meeting #0
A#So um  who wants to start off #0
A##0
A#Technical uh designer again #0
B##0
B#Again #0
A#Okay #0
B#Hmm #0
A#Uh  yeah #0
A#Uh  before we begin it  I want to say I've I've put the minutes of the uh second meeting in the shared folder  but they're still not uh quite okay #0
A#It uh it uh still some technical difficulties so the the first part of the minutes are very hard to read  because there are two documents that uh were layered over each other #0
B#Mm-hmm #0
D#Okay #0
A#So But uh  from now on I won't use my pen anymore  so will be p just ordinary keyboard #0
B##0
C##0
D##0
A##0
B#Uh  may be better  yeah #0
D#Keyboard work #0
A#I think it will will be more uh easy for you to read the minutes #0
D#Yeah #0
B#Yeah #0
D#Alright #1
C#Okay  when we talk about uh components design  um it's really about the material and the and uh uh really the stuff we build uh the remote controls of #0
C#Um  a remote control consist of uh components and the components of a remote control consist of uh properties and material #0
C#We have to choose th uh these uh wisely and it could affect uh uh a kind of grow of in uh in buying uh the remote controls #0
C#Um  the components of a remote control are of course uh the case #0
C#Uh the properties of the case  um it has to be solid uh in hard material like uh hard plastic uh with soft rubber for uh falling and and uh uh yeah  it feels uh good in your hand #0
D##0
C#Mm the buttons has to be uh solid too  and the material is soft rubber #0
C#Uh I've got a uh email from the possibilities of Real Reaction #0
C#Um uh they're telling me that um when we build uh a remote control of um of plastic or rubber  the uh buttons have to be uh rubber too #0
C#Mm It's okay #0
C#Yeah #0
C#I when we use a rubbled a doubled curved case  we must use a rubber push-buttons to uh the the rubber double-curved case is a is a t uh three-dimensional uh curve in the in the design  which is uh necessary when we want to be trendy #0
C#Uh Um the energy source  uh I've got a lot of possibilities for that too #0
B#Oh #0
C#Um  uh the basic battery  which I thi prefer because of its uh its non uh non-depending of of of uh um Uh here you have to have a hand uh yeah  kinetic uh energy #0
C#Also in uh this one  like in the watches  but a remote control can lie on a table for a day  and then you push uh a button and so you don't have to uh walk with it all the all the time #0
C#Mm  solar cells are also uh a bit weird for uh remote controls #0
D##0
A##0
A##0
C#Um uh also the case material  uh I think that plastic is the is the best with rubber  because uh wood or titanium would also be a bit weird #0
D##0
B#Oh titanium is probably trendy  I think #0
D#That's true  I guess #0
B##0
D#Yeah #0
B#Well  maybe a little bit expensive #0
D##0
B#I don't know #0
D#Huh #0
C#Uh  they don't tell anything about the cost of uh titanium #0
C#Um the chip uh the chip set uh and the board is uh all off the shelf #0
C#Also  the speaker in the remote control  when we want to retrieve it #0
C#Um  the base station is also off the shelf  all the materials and the components are uh just available in uh in our uh factory #0
C#Mm  I've told about uh the three first points #0
C#Mm  the simple electronical chip uh is is available uh with the LED transmitter uh transmitter #0
C#Uh  it's all uh off the shelf and even the speaker and the wireless retriever are all uh available in our company #0
C#Um  another possibility #0
C#I uh yeah  I looked up on was uh the L_C_D_ displays #0
C#Could be uh something special to our uh remote control  and it's possible  but it only cost a bit more  but maybe it can be uh within the limits of twenty five Euros #0
A#Twelve and a half #0
C#Ah yeah #0
A#Actually What are those  t tooth uh brushes  or so But it's actually kind of uh well  it resembles the design I had in mind for this proj You know the the cartoonish Alessi kind of design #0
D#Yeah #0
B#Yeah  production cost #1
C##0
C#I th I got an email with uh some examples and it these were were the most trendiest one #0
B##0
C#You see uh a covers  which can be Um  I don't know #0
B##0
C#Um Yes #0
D##0
B#I Different colours also #0
D#Yep #0
A##0
C#Yes  maybe we can uh bri uh bring a couple of uh couple of types of uh maybe a kind of uh whole uh um a whole set of uh different uh remote controls #0
A#And we can we can steal their ideas #1
D##0
C#Maybe we can bring a whole line uh with uh with a huge variety of uh uh house uh stuff #0
A#Huh #0
A#Well  it's a possibility  too #0
C#Like uh maybe radios and uh television also uh in this in this in the same style  but Yes  because we have to uh we have to we have to bring the logo and all the stuff uh back into it #0
C##0
A#Uh-uh #0
D##0
D#Yeah  that'll be for the future  I guess #0
A#Okay #0
B##0
A##0
D##0
A#Next time we're here #0
D#Yeah #0
B#Yeah #0
B##0
C##0
D##0
A##0
A#Oh  okay #0
D#Yeah #0
D#Definitely #1
A#Okay #0
B##0
C#Thank you #0
D#Alright #0
B#Okay #0
C##0
D##0
B#uh Ah #0
D#Yeah #0
D#That's okay #0
D##0
B#Well  I shall go to the next slide #0
B#Um um  I still don't have any information about user requirements #0
D##0
D##0
B#I was thinking about just uh the basic functions and I got uh Yeah  but but then wh I don't know when there are new user requirements #0
A#Oh  we decided upon that in the last meeting #0
A#Didn't we #0
A#Oh  okay #0
B#I ha I ha I have the I have nothing #0
A#Well  tha I didn't receive any new requirements or somethi Just no  but we decided to use only b basic functions only #0
B#Well  I have here a couple of basic functions I could think of #0
A#Okay #0
B#I dunno if they're maybe a little bit more  but Yeah #0
A#Well we maybe we can think of that later #0
A#W just these are the ones you already summed up in the Okay #0
B#Yeah  I I uh well  I pointed them out here  just to make it a little bit easier #0
B##0
B#Um Another function uh is of course we already discuss it on the side #0
B#Um  I don't know what costs of it #0
B#Uh  I've no idea about it #0
B#Uh  I was also looking for what you said  for I got an email uh uh about uh L_C_D_ in in in front of the remote control #0
B#I don't know if that's a good idea  or maybe it's a little bit too much for twelve and a half #0
B#Production #0
A#Yeah #0
C#Uh-huh #0
B#If we got already uh something like a base #0
A#That might get redundant also maybe #0
A#I don't know what kind of information it would Yeah #0
B#Yeah  I don't know #0
B#I d I uh ju I was just thinking about it #0
D#Mm yeah #0
B#Then I got a pop-ups to go to the meeting #0
A#Yeah  it's okay #0
C#Maybe we can bring t uh uh teletext to the t to the remote control #0
B#But The remote control #0
D##0
C##0
B#a little uh too A little bit A little bit too big  I think #0
B##0
A##0
D#Then you and then you've got a flag s Very big R_C_ #0
A#Okay #0
B##0
A#That's not It was not a good idea #0
A##0
D##0
C##0
D#Yeah #0
A##0
B#Exactly #0
C##0
D##0
A#Okay #0
B#Um  yeah #0
B#Well  the functions are are not more to discuss  I think #0
A#No #0
B#It's it's just the base things we already discussed that the no V_C_R_ or that kind of uh  so that's very easy #0
A#No #0
D#No #0
B#Um Mm-hmm #0
A#But you do mention the next and previous uh button #0
D#Next channel  previous channel #0
B#Well  that's next channel #0
A#Oh  okay  o okay okay #0
B#I mean next channel #0
B#Uh Um oh  I I got an email with with an uh a remote control with a base #0
B##0
A#Huh #0
B#So  it's uh just an idea #0
B#And I um uh thinked of the button sizes and I'm not sure uh if they have to be big or uh just small Well  I'm not a e I'm the expert for user-friendly  but not for trendiness #0
B##0
B##0
C#But you're the expert #0
D#I think it depends on the function #0
C##0
A#Mm-hmm #0
B#Maybe it Well uh okay  that's your point #0
D#Well  if you save uh Perhaps uh s tiny buttons aren't user-friendly  then we wouldn't im implement that of course #0
B##0
B##0
A##0
C##0
D##0
A##0
B##0
B#Um  yeah #0
B#Yeah  okay #0
D##0
B#Yeah  I've nothing to s Uh  with a little bit larger  yeah #0
B##0
C##0
A#Well  w when we only use basic functions  we have the possibility to make the buttons larger #0
D#Oh  that's right #0
D#Yeah #0
B#I thought so  but maybe with the Yeah  that groups #0
A#Well  I think we already agreed upon the fact that the the the skip buttons and the cha and the volume buttons  th th those two have yeah  they have to be large #0
A#Uh  I mean th th the the two two basic buttons  you know  the to skip channels and to uh I think yeah  I don't know why  but I think that is that's t trendy too  because that's the mo it it you know  it's uh acc acc um accentu uh  how do you say it #0
B#Large #0
B#Yeah #0
D#Yep #0
B#Most the most used uh buttons #0
D#Those are probably the the th Yes #0
A#It puts an extra accent on the the on the simplicity of our remotes to j to make these two most basic functions extra big  like t Yeah #0
B#True #0
B#Yeah #0
D#Those are probably the b four most most used buttons on the th in the remote control #0
C#You did the research #0
A#And you want to acc accentuate that  you know #1
B#Yeah #0
C##0
D#Sorry #0
C#It's from your research #0
D#Yeah  sure #0
A##0
D##0
C##0
B#Okay #0
A#So Okay #0
C##0
B#Uh  that was all y uh personal preference I didn't have #0
B#I didn't had any time left #0
B#So Yeah  #0
A#No uh  that's coo it's cool #0
C##0
D##0
D#You don't care #0
D#No  sorry #0
D##0
C##1
A##0
D##0
B##0
D#Yeah #0
B##0
D#Oh #0
D#Go away #0
D#Come on #0
B#It's there #0
B#Yeah  click on it #0
D##0
B#Couple time #0
D#Oh  great #0
D#Well  I've done some research again about trends on the internet #0
D#Um I've done some investigation  and uh well I uh got some information from fashion watchers from Paris and uh Milan #0
D##0
D#Some uh some findings the most important thing is fancy look and feel of the remote control #0
D#Uh  well  we were going to imply that  so that's nice #0
D#The second important thing is uh innovative technology in the R_C_ #0
D#Uh  our market really likes really likes that #0
D#And uh the third point there in this uh order if of importance  the third point  is a high ease of use #0
D#And uh  well  for the idea  I've put some trends uh for the market of elderly people #0
D#Dark colours  simple recognisable shapes #0
C##0
D#So we probably won't do that #0
D#The younger market likes uh Well  the themes of of this year are uh surprisingly fruits and vegetables and spongy material #0
B##0
C##0
B##0
C##0
D#I found this image  which is uh Well  it symbolises the idea of fruits and vegetables #0
D#I don't see the spongy part in it #0
D#But with a little bit of fancy Exactly #0
A#Well maybe c then we have to do something with Sponge Bob then #0
A##0
C##0
B##0
D##0
A##0
D#I got some ideas Uh well  yeah  pictures isn't really good word  but um some symbols of fruits or vegetables maybe #0
B##0
C##0
D#Uh  catchy colours #0
C##0
D#Fruit is uh yellow  green  red  whatever #0
B##0
D#So  remote controls in in catchy colours #0
A#It doesn't stroke with the with the dark colours #0
D#Uh  no  we don't want dark colours #0
A##0
A#Not the dark colours #0
A#Okay #0
D#No  I just put them there to uh  yeah  uh for general idea #0
D##0
A#Okay #0
D#And uh  the docking st uh I think the spongy material is is very irritating for the uh remote control itself #0
C##0
D#But to Yeah  the To implement some spongy thing  maybe we can do it in the in the docking station #0
C##0
D##0
D#At the bottom of the docking station or whatever #0
D#And uh  we could bring one line with a dark colour uh to um uh p uh yeah uh uh v how do you say #0
C##0
D##0
A#For diversity or something #0
D#Yeah  also a bit for elderly people who are a little bit crazy and want maybe want a little younger design but still the dark colour #0
A#Uh No  but I I I think that uh our design already resembles so a piece of fruit #0
B#Well  how uh But how do we use uh fruits and vegetables in Christ's sake with remote control #0
D##0
C##0
D#I mean it it it reaches a different market uh  but it it it doesn't cost really much effort to b to uh bring uh like a black R_C_ on the market or whatever #0
D#Yes #0
C##0
B##0
B##0
D#Yeah  there's there's always a Well there there's always empty space of course on a remote control #0
B#Uh  make it a banana #0
B##0
A#It's like a pear or something #0
D#I mean I think this part of the R_C_ uh well the upper the upper part or whatever is uh is not not used with buttons  I guess #0
A#No  I don't think you have to do it like Yeah  but it that doesn't have to remind you  you know  like explicitly of s our f of a of a specific piece of fruit  but just  you know  like the the the the round curves #0
C##0
D##0
B##0
D#So you you can put some fruity things No  of course not #0
C##0
A#And so y I I think this y it already sem resembles uh something like a pear to me or something #0
D#Especially i Yeah  yeah #0
B#Yeah  but th yeah  but that Yeah  but that's Yeah uh uh But d don't we need a creative artist or something like that to m make it to feel like a a a a vegetable or fruit #0
B##0
C##0
D#Yeah  exactly #0
B##0
D#If we make it little bit greenish #0
A#Yeah #0
A#You do get the idea  eh #0
A#The fruity kind of round 'Kay #0
D#A and we could use one of these for the uh w what is it #0
D##0
D##0
A#Yeah  uh yeah  I don't know #0
C#Grapes #0
D#Uh Isn't Wha whatever #0
C##0
A#Uh  this is a b yeah #0
A##0
C##0
A##0
A#Yeah  yeah #0
A#Of course we have uh we have a very big uh the s Of d design team  yeah #0
D#Yeah  sure #0
A##0
D#Yeah #0
D#Well  w we can uh w we can we can produce multiple uh multiple things #0
C#For a big team of artists #0
B##0
C##0
D#This is then the uh pear #0
D#I don't know the English word  so forget it #0
A##0
A#Yeah  but It's pear  I guess #0
A##0
D#And um  maybe  yeah  a b a banana is uh is n not easy for a remote control  but m yeah #0
D##0
C#But uh but I think we don't have to make we can't make all uh ten designs #0
A##0
D##0
A#No #0
C#We have to make one design I th I I think #0
A#No  but I think it's it's already what we were were up to #0
D#Mayb maybe two or three #0
B#Yeah #0
A#Uh  it's it doesn't have to resemble uh what I already said  a specific piece of fruit  but just  you know  like a fruity thing going on #0
D#Yeah #0
D#No sure  but but B but that's great  and and and what I was what what I was saying  the catchy colours Yeah #0
B##0
A#And it's it looks fruity to me #0
A#And uh  but I do like the yeah  I do like uh the f uh to the idea of making a a y uh  a catchy colour design and a d because I do I think a dark colour would be nice too #0
C#But pictures of fruit  vegetables vegetables Yes  you can put a logo on top of it #0
A#Maybe it's too much  you know #0
B#But  we we have to um There have to be the the the the firm colours  our own uh colours has to be in it #0
D#Yeah  uh not really #0
D#Pictures was a was a bad word  but Well we c yeah #0
A#Okay  but what are the This is yellow #0
B#Yellow  a Real Reaction #0
D#Yeah  sure #0
A#But I don't think our our company colours are this fashionable #0
B#Uh  yeah #0
D#Maybe we can if if we got our docking station over here #0
B##0
C#Yes  it's really fruity #0
B#We uh f A yellow do Uh  yeah #0
C##0
D#I can't draw with this thing  but I'll try #0
D#If this is our docking station  we can make our logo over here #0
D#It doesn't work #0
D#And then Well  the button button over here or whatever  I don't know #0
B#Yeah  and the button then #0
C#With a strawberry on top #0
A#Yeah  on uh n uh on the bottom of the remote you can do Okay #1
B#Okay  yeah #0
D#On the front  of course  because else you can't find it #0
B#Okay #0
D#Well  that were my ideas a little bit #0
D#I'll close 'em down #0
D#Um  go away #0
A#Okay  you can you open the conceptual design presentation #0
D#Conceptual design  yes #0
A#See what was on the agenda #0
B##0
B#Lazy #0
D#The agenda #0
B##0
A#This is his own remote #0
A#Because um  maybe we can start with the technical uh functions  but I don't think it's there uh  yeah um  do we want to um use an L_C_D_ display  for example #0
B#Yeah #0
D#Well  it's nice  of course #0
C#Only if we Maybe maybe we can make a T_V_ guide on it  for the channel you're on #0
D#But I don't I don't know what to display on it #0
A##0
D#I mean Yeah  but it should be li like this big  and I don't think Yes sure  but it it has to to show an entire title of a programme or at least a q a quite quite large part of it and then you get a very large L_C_D_ screen  because Yeah  I don't know #0
A#Me neither #0
A#Yeah  but it's so I don't think we should do it #0
C#No  no  only the T_V_ channel with the with uh with uh four programmes #0
C##0
C#You can uh zap through them with the page up page down button #0
C#Yes  it can On your No  on your mobile phone you can y you can read text also #0
C#So why not on your remote #0
A#Yeah  but no #0
A#I do I think it's a bit redundant  actually #0
A#And it's also not I don't th even think it it looks s like sexy or something  it's But you're already watching the T_V_  you're not gonna watch your remote control #0
B#Well well what would you display on it then #0
C#Uh  programme uh information or or or or g or a guide on t on teletext  yes #0
D#Programme information #0
B#But is it isn't that a already on T_V_  a lot of new T_V_s #0
D#But Well a lot a lot of T_V_s indeed show uh when you uh zap to a But then we also uh w need to bring out a line of T_V_s which we were planning to  but whatever #0
C#Also on the internet #0
C#But Yes  but you also want to know what's next #0
B#Yeah #0
B#Yeah  and we also have to yeah #0
A##0
D#Because the T_V_ has to send information back to the R_C_  and I don't know if that's possible #0
C#Yes  that's uh really possible #0
D#Yes  yes  o of course it's possible  but you gotta uh implement it in the T_V_s  and I don't think everyone's gonna buy a Real Reaction T_V_ within a month after the release of our uh remote control #0
A##0
B##0
C##0
C##0
C##0
B#And I also Yeah  I dunno #0
A#I really understand you want to make your job more exciting by putting an L_C_D_ in it  but I I really don't think it's a good n goo because it also doesn't stroke with we wanted uh c When we talk about the materials  uh it's a good idea to use these plastic materials with soft rubber stuff on it #0
C##0
B##0
D##0
A##0
A#It was our idea  you know  to give it a more sturdy look and that you ca like you can throw with it #0
D#Yeah #0
A#But I don't think a L_C_D_ display fits in that image #0
A#You know  it's like more vulnerable  and it adds nothing really  you know #0
D#That's true  that's true  it breaks f yeah  it it it's not very solid  it's uh frag fragile #0
A#Yeah  yeah #0
A#You could make it  but it's just it it doesn't I don't think it it's coherent with the design we're after #0
D#No #0
D#No #0
D#I don't think so ei either #0
A#But that's my opinion #0
A#Well  you you y Okay  we can vote for it #0
C##0
A#You want the L_C_D_ display #0
C#No #0
A#I don't want to and he doesn't  so it's up to him #0
A##0
B##0
C##0
D##0
D##0
A#If we wanna And I've read somewhere that I've got some kind of veto veto uh rights #0
A##0
C##0
D#Ah #0
D##0
B##0
B##0
C##0
B#Oh  okay #0
D##0
D#Bastard #0
A##0
A#So I can also say But did we skip the Yeah  you could do m but what what i so what i but do you think we should We're not even sure what what information we want to display on it #0
B#We can you away #0
D##0
D##0
B##0
B#Yeah  I don't know #0
B#Uh  uh I i if it's it's a simple p No  that that's right  and uh I also have to think about new functions  maybe buttons or something like that to control it #0
C#No uh um Y yes  you can use uh buttons uh uh w that are already uh on the remote control for double functions #0
A#So Nah  that's not gonna work #0
B#Kind of L_C_D_ or something or But how does it display then #0
C##0
D#Yeah  I guess #0
B#W when I go to the second channel  what what does it show me #0
C#Uh  then you push a button #0
C#The title and the information about the programme #0
B#About that programme #0
C#But but uh yeah  what he said was right  about the televisions  they have to be uh customised to the But maybe in future it will be a giant hit  and when you are the first you have the biggest uh Yes  you can put uh a little L_C_D_ display on it with uh with lots of information #0
D#No #0
A#Yeah #0
A#Oh  well uh I've seen it done before #0
A#Do you know th like the the bigger rem uh universal remotes  they have d L_C_D_ displays  but then it's very functional to indicate which what uh uh device you are controlling #0
B#Yeah #0
A#So it's that that's what I've seen #0
D#Yeah  that's true  if you uh Yeah #0
A#But it just it j it doesn't doesn't match with the our whole basic concept #0
C#But uh I haven't thought about it #0
C#But whe but when you put a a a transparent uh plastic uh uh screen on top of it  it i it isn't vulnerable #0
A#Well yeah  yeah  okay #0
C#You can throw with it and When when you put uh maybe a colour L_C_D_ t uh screen on it  it's very special and very trendy to have uh a remote control from But uh I've got a the email with uh with the possibilities #0
A#That's maybe not the most important  but it's just I don't think so #0
B#Is it fashion #0
B##0
B#I don't know #0
B#That's not up to you #0
B#That's up to market if i if it's trendy #0
A#Yeah  well do you ha do you have to You haven't looked after the trendiness of L_C_D_ displays  have you #0
D##0
D#No #0
C##0
D##0
B#Because our our motto is we put fashion Yeah  if you want to be trendy you have to be coloured #0
B##0
B##0
D#Well  I think it's uh I think it's pretty trendy  to be honest  uh but um I don't know if if if well  I'm coming back to the costs again  but I think uh we gotta build a b pretty cheap design to to stay within our limits #0
D#And I think uh especially colour L_C_D_  which is indeed pretty trendy #0
D#But I don't think Uh  I think it will be too expensive #0
A##0
B##0
C#And L_C_D_ was a possibility for the remote control #0
C##0
A#Yeah yeah yeah #0
C#So why don't we use it #0
D#Uh  did it say a price also uh for for uh monogramme uh L_C_D_ or uh coloured L_C_D_ #0
A#Yeah  but we're gonna if it Then uh then you better don't yeah  d Uh uh I really don't feel the whole idea of an L_C_D_ display #0
B#Coloured If you have black and white or something  or grey  that's It's too much uh maybe uh with with the L_C_D_ and the docking station and With one thing special #0
D#Yeah really  if y if you c i I in in two thousand and four you can't uh put something on the market which is a monogramme #0
A##0
B##0
D#Really #0
D##0
C#No  but it doesn't sa say anything about a colour or But  mm  I alf I also got a possibility to put uh a scroll button on it #0
D##0
C#But I didn't think that Yes  but o on the No  when y But it look Yes  but that remote controls are already on the market #0
C##0
C##0
A#I'm sorry #0
A#It can't co you cannot convince me #0
A#I don't know how well how to with you guys  but I don't really feel it #0
A#We already we're uh Yeah  we already have the the th th th base station gadgets  and want and it uh uh  do it has to be a simple design  which sturdy  which soft I don't think I j uh  and really  I don't see how the the L_C_D_ display is gonna add anything  you know  on a design level #0
B##0
D#Yeah #0
D#W we've we've gotta find a balance  of course #0
D#And I think We have a pear #0
B#Not a whole package of specialty #0
A#Uh  I think it's slicker to have no L_ CEDs #0
A#Y we want to it's simplicity  w you have two big buttons and you can do whatever you want with these two buttons  so you don't need an L_C_D_ #0
A#It doesn't fit in our philosophy uh behind the whole remote #0
C#The simple Yes  but but when you want to have something special Yes  but you had a picture of it from another company #0
A#Yeah  but we already have the docking station  which is It has to be developed  but no  but it that's that's our that's our killer feature #0
C##0
B#And uh the Yeah #0
A##0
B##0
C##0
C##0
B##0
C##0
A##0
B#It's just an it's just an idea #0
D##0
A#That's our what makes it special #0
B#It's a it's True #0
C#Yes  it was already made #0
C#Tha the remote control on the docking station #0
A#Yeah  we're gonna develop our own r n docking station #0
D#Is that so #0
D#Was it it wasn't just a prototype #0
C#Yes  he have a picture of it #0
B#Well uh I uh Yeah  I dunno #0
D##0
A##0
C##0
D#Exactly  I've never seen it in a store #0
C##0
B##0
A#Uh  but re we really have to cut this off  I re I know you I I I I I get the idea you really like it  you know  the the L_C_D_ thing  but I I think it's it's not a good idea  and we have already mentioned all the arguments #0
A##0
A#I don't uh  do you guys agre How do you guys think #0
A#I d Okay  we s skip the L_C_D_ display #0
B#No  it's too much #0
D#I think it's a little too much  yeah #0
B#It's overdone #0
C#Okay #0
B##0
A##0
D##0
C##0
D#Okay #0
A#I'm sorry  maybe you can do something if we are at your own place  or make it make it make it happen in your basement or something #0
C##0
B##0
B#Democratically #0
D##0
D#No #0
B##0
D##0
C#Mayb I will rule the world with it #0
D##0
C##0
D##0
C##0
A#But Probably so #0
A##0
D##0
D#Yeah  yeah  yeah  yeah #0
A#Okay #0
C##0
A#But for the technical part #0
A#The m material  I think uh it was a good idea to use the plastic and uh the rubber #0
C#Yes  maybe a bit of a cushion is Yes #0
A#Uh Yeah yeah yeah  p Exactly #0
A#This is what it w Yeah  but it it was already what we're uh we're after  you know  to give it uh  you know  the soft touch in your hands and also to  know  like Yeah  that is y the b airbag kind of thing #0
D#Yeah  for the spongy uh feel #0
B#With a spongy Bob feel #0
D#Yeah #0
D##0
B#Yeah #0
C#Like a b yes #0
A#You can st throw it at your little brother's head #0
D##0
B#Yeah  you just can drop it #0
D#Yeah  airbag #0
B#Yeah #0
D#If you drop it if you drop it the airbag comes out  yeah #0
B#Yeah #0
A#Yeah #0
D##0
A#No no no  not that comfy #0
D##0
C#Maybe it but then we have to look that it uh w uh will not um be too childish to see #0
B##0
A#Yeah yeah #0
A#Yeah #0
A#Okay  that's a that's a good point #0
A#And that's why I like the dark t col dark colour bit  you know  because it may be the design uh  it's uh maybe it is a bit of the it's a bit nineties maybe  what we're what we're up to rat fun to this point #0
D#Yeah #0
B#But not black I think #0
D#No #0
B#Well if if it's fruit and vegetables  it have to be colourful #0
A#Yeah  that's that's true  but but it has to be a little big solid #0
D#Yeah  b yeah  that's what w I I was pointing at #0
B#But can we ge uh uh Can we combine it or something #0
A#It mustn't be too  n you know  th too overwhelming  then when you put it on your just Yeah #0
B#Uh with uh yellow and black #0
A#Yeah  maybe so #0
B#Make it a bee #0
D#What #0
B##0
B#A bee #0
D#Oh  a bee #0
D#Oh #0
D##0
B##0
A#No  uh I don't like the yellow and black combination #0
A#But it is our company colours #0
C#Yes  real real good colours #0
A#Apparently #0
B#Yeah  it's our yeah #0
A##0
D##0
A##0
B#We we have to use yellow #0
C##0
D#Yeah #0
D#Hmm #0
C#Hmm #0
A#I don't like yellow  and uh maybe I don't know #0
D#Well  we can as as I draw really nicely over there #0
C#But that's not really fruity #0
D#We can put the logo on our uh on our base station #0
D##0
D#Uh  yeah #0
D#And maybe very very tiny on the remote control itself #0
A#But Okay  but what uh  what are other tef technical things we have to discuss #0
D#But  i Yeah #0
B##0
C#Uh fronts of the We can have uh different uh uh fronts of the telephone #0
A##0
A#Should we do that #0
A#I don't think you we should do that #0
A#Maybe just bring it out in different colours  but not af that you can switch fronts afterwards  that's also too much #0
B#Different fronts #0
D#I guess that's that's enough #0
A#People don't wanna spend more money on their remote control  I guess #0
D#That's way too Nokia #0
B#Yeah #0
A##0
B#Uh  you can you can l uh let choose the customer which colour he wants  yeah #0
C#Are these designs #0
A#Yeah  yeah #0
D#Yeah  definitely #0
B#Yeah  Three three or four uh four uh colours  or something like that #0
D#Just bring more designs on the market #0
A#But uh  without gon uh okay #0
D#Why not  yeah #0
A#So  are we through the technical part then #0
C#Yes #0
A#Okay #0
A#So we uh agreed upon uh n uh well  not u unanimously or how you call it  but Yeah #0
C#It this a real uh young young and dynamic uh uh styles #0
B#Well  yeah  the Three to one #0
B#That's And tita uh titanium  is uh is is it a no #0
B##0
A#The materials you uh mentioned in your your personal preferences were all were quite okay #0
C#Yes #0
A#O o only only the last point your no titanium's not not out of question  I guess #0
C#Yes  But also w Yes  b bu but when we use s soft mm Mm #0
B#Is It's just like that  th this titanium #0
A#But is it possible to use both the the plastic and so uh soft things and t p titanium  as well #0
D#Sure #0
A#Makes it in a homogeneous uh design #0
B#No  not all  not all of them #0
C#But it it then it uh you can't throw it it #0
C#It will uh make a huge noise or break other stuff when you throw with uh titanium with your remote control #0
A##0
A##0
A#It will it will break other stuff w when it's plastic  as well #0
B##0
C##0
A##0
D##0
C#No uh  titanium is a bit uh it's a bit harder #0
A##0
A#Yeah #0
A##0
D#Yeah  that's true #0
A#No  but uh uh  you should ma Yeah #0
C#But also on the colours  the young Yes  but a titanium remote control  when you're uh watching T_V_ uh or your hands are a little bit sweaty  and the Yes #0
A#Okay  think of the possibilities and make it in completely titanium #0
A#Well would it be more trendy #0
A#More chic #0
D#Yeah  I think it I think it does #0
B#Uh  I think titanium nowadays is way more often used than plastic #0
B#In trendy things #0
D#Yeah  o On the other hand  if you want to make fruit fruity stuff with uh Yeah  that's true  that's true #0
B#Yeah #0
B#It's cold in the winter #0
A#Yeah  but I I really like the idea of the the the plastic and the big kind of thing #0
B##0
C##0
B#Yeah #0
A#But the question is i then it's  you know  is is it fits in our s philosophy to make it uh sturdy and simple and uh  know  like uh When you make it titanium  it becomes more like some kind of gadget you actually don't need #0
C#Sports and gaming #0
C#Define #0
A#And when it's big and plastic  it's like some fun stuff you can always have around #0
A#It's always fun to have something big and plastic around #0
D#Yes #0
B#You have that uh M_P_ three player of Nike  I saw #0
D#Yeah #0
B#Isn't that titanium with a little bit of rubber #0
C#Yes  it's w but it is uh plastic #0
B#Isn't it Is plastic #0
B#Well  it's titanium looking #0
C#Yes  w we can do that on the on the We can make this as a style too #0
D#What #0
B#Yeah  he is #0
B#Here he is #0
B#Uh  the I don't know if you know the M_P_ three player of Nike #0
B#'Kay  uh that that's very uh with rubber  so it's very rough #0
D#Oh  yeah #0
D#Okay  yeah #0
A#Yeah  that's beautiful #0
D#Yeah  I see #0
D#Yeah  but but but Yeah  I th I think that's difficult  because uh that's different material  and then you gotta have like uh uh uh two material lines of of of Yeah  if it's just a colour uh which you uh which you change then  I guess it's it's nice to have one of these #0
C#Uh  this is uh just a No  we c we can make it from the same kind of plastic #0
A#Oh  maybe th maybe this is an No  I do like the idea of maybe a t titanium kind type of body w and then with s plastic colouration around it #0
A##0
D#Uh Yeah #0
C#Uh I don't have the information #0
A#You know  like the the soft stuff  but I don't know if it's possible #0
C#Uh  I I didn't got it Yes #0
A#But you can't make the plastic give uh the ti titanium look #0
B##0
B#True #0
A#But make it just like shiny #0
D#Mm-hmm #0
B#Yeah yeah  true #0
A#Maybe we should uh shou And uh and maybe we sh should we t I don't know if we should talk about uh  how how much time have we got left #0
C#Like the M_P_ three player #0
B#Yeah  maybe that's good idea  yeah #0
B#But if you want to la uh yeah  last longer than two weeks or something like that  you can maybe I don't know #0
D#Yeah #0
D##0
C#Uh  in a lot of other uh in a lot of other product uh categories like uh even in b in bags industry #0
B#What time does The rubber #0
A#Forty minutes #0
C#Uh  they began with uh t typical uh leather bags  but then they became stylish  with all all si all sort of colours  and w kind of fon of uh of fronts  like we can use on the telephone and it Like Eastpack uh began a revolution with it with all this uh kind of bags and and colours and and Yes  and and styles #0
D#Yeah #0
A#Okay #0
A#You putting in different colours #0
A#Okay #0
C#They have uh also uh a kind of uh um uh roses on it  a and uh Then we can always uh use the same design for a greater resemblance  but with new uh with new colours  new yes #0
D#Uh yeah  yeah #0
A#Okay #0
A#Yeah  but w yeah #0
B##0
A#Well  it is #0
A##0
A#It's a possibility #0
A#But  let's think about the bas Mm-hmm  mm-hmm #0
D#Yes #0
D#New prints on it #0
D#Yep #0
A#But wha th our basic idea y I mean  you gonna we're probably gonna have like two type of materials  like the d d b the plastic uh enclosure and then the the pads that surround it #0
A#And and pro and lights #0
A#We have to incorporate the lights too #0
A#But  uh do w gonna gonna are we going to give it a two-tone colour look  like the the plastic mould is in in one colour and the s the cushion pads around it are in another colour #0
A#Is that the idea #0
A#Is that a good idea #0
D#How do you mean #0
D#Th th the uh base in a in another Yes #0
A#How many colours are we how many colours are we gonna we're uh uh f uh f Only five minutes left  by the way #0
A#How many colours are we gonna give it #0
A#Like two-tone colour #0
C#There there are three uh components three components type #0
A#T I think maybe the case itself should be in one colour and then the rubber of the buttons  and the cushions as well should be in another colour #0
B#Yeah #0
B#Uh no  not too much I think #0
C#You have the buttons  the the case uh itself  and the rubber and th Yes #0
D#How the buttons yeah #0
A##0
B#Or you just make uh one colour  uh maybe with a a z a kind of like a big wave or something like uh In in another colour #0
D#Yeah #0
A#Okay  but not more than Well  yeah  it's No #0
B#Not more than two colours I think #0
D#No  definitely not #0
B#It's a g a little bit too flashy #0
A#Maybe we should talk about it on a l in a later meeting #0
D#Yes  definitely #0
C#Yeah  or or when you use the buttons as black  it you can use two colours as well uh But then you have to put uh up and down and uh left and right Okay #0
A#Okay #0
A#But we have to uh think of some other uh important things #0
C##0
A#Uh oh yeah  the the functionalities of the the buttons #0
B#The funct yeah  I was I was thinking about th the st do we still want a joystick idea #0
A#No #0
D#No  I think that's too vulnerable #0
A##0
B#Yeah #0
A#I think this is okay  the so we have the basic #0
A#Then we have the numbers #0
A#We have the power button #0
A#We have we have a teletext button #0
B#The volume  teletext and Yeah yeah yeah yeah  b Yeah  but wha what kind of menu #0
A#And maybe want to access a a menu or something #0
D#Yeah  but that's that's I was thinking that's gotta be on the television #0
A#Most T_V_s have a menu #0
B##0
A#Yeah  but I think you ha I really need a menu button #0
A#That's just i the only button only You know  I No  I think most T_V_s have an uh a menu nowadays to access the uh uh screen settings #0
B#Is uh isn't that different from every television #0
A##0
D#Mm #0
D#Yeah  if it's c if Yeah  I think it's okay to to add a menu button for uh and if the T_V_ doesn't have a menu  then Yeah #0
D##0
A#And so But that that covers all the all the other settings #0
B##0
A#It covers everything then #0
D#Yeah  you can put that on the two eight four and six or whatever #0
A#No  you can use the And you al can also use the normal skip buttons for that #0
A#Th in that way we have like only the numbers  the power button  skip and volume  and then uh uh ten uh rem uh yeah  mute #0
D#Mm  yeah #0
D#A mute and a teletext and a menu #0
B#Mute #0
A#A teletext and a menu  and then then i that's it #0
D#That's all #0
A#It's all we need #0
B#Yeah #0
D#Hmm #0
A#Okay  uh another stuf some stuff about the the the design of the docking station #0
D#Great #0
D#Yeah #0
B#Okay  that's not mu not much functions #0
B#So Okay #0
A##0
A#Something important about a s uh  no  uh which sh uh should remind us of the remote itself  I guess #0
D#Yeah  definitely #0
A#Uh  in one colour #0
D#Are we gonna do something with the uh spongy thing there #0
A#Just use I think the spongy thing already um comes forward in the in the in the cushions  pads and things on the s uh side #0
C#Uh What are we going to do now #0
D#Yeah  that's true  that's true #0
A#And we will make it spongy and and uh and uh well  the fruity thing is just the shape should be fru i did I think this is kind of fruity  you know #0
B##0
A#Just round shapes with uh Yeah  but we're gonna have to we really have to think I think colours is very important  because it has to be flashy  but and but it d it doesn't have to be annoying  that when you uh  know  some things is just over the top  and when you have it on your table for more than two weeks  you it just gets annoying  because it's so big and flashy #0
D#Yeah  it's kinda fruity  and with th with catchy colours uh uh w Yeah  definitely #0
A##0
A#Uh  it has to be some level of subtlety  but we have to still have to think of how we manage to uh to get to that #0
D#Yeah #0
A#Okay #0
D#Okay #0
A#Guess we're through then #0
D#I guess so #0
A#But we I think also we just so we have to do something with colour but also  I I think we have to keep the dark colour thing in mind #0
A#I think that's uh adds to the too much colour maybe m um But our des design experts will uh work that out #1
D#Yes #0
B#Yeah #0
B#Too much colour  i it uh when you got it in a living room  it's too much maybe #0
D#Yea yeah #0
B#It has to be Yeah #0
A#Okay  well I think the meeting will be over within a minute #0
D#Yep #0
A#So we will wrap up #0
D#Something like that #0
A#Or is there anything we'd like to discuss #0
D#I guess not #0
A#That's right #0
A#Okay #0
D#Do you  guys #0
B#No #0
D#No #0
A#Okay #0
A#Well  you will read the minutes uh in the you can find them in the pro probably #0
D#In the shared folder #0
B#Oh  okay  yeah #0
A#Yeah uh no  for su for sure because I'm will now type them out #0
A#Uh  y yeah #0
D#You'll see in you email  I guess #0
B#Yeah #0
C##0
A#I think uh But toilet paper roll and uh Okay #0
D#Yeah  I don't know #0
D#I don't know #0
B#I hope so #0
B##0
B#And the other thing is that you don't have kind of prototype or something like that #0
B#You see a kinda prototype you can a little bit more uh Yeah #0
C#I will make one in the next uh twenty minutes #0
D##0
D#Construct one  yeah #0
B##0
B#With you laptop #0
D##0
B#Yeah #0
B##0
B##0
C##0
B#Oh my God #0
D#Alright  shall we get back to work #0
A#Yep #0
D#Great #0
A#I was waiting for the l last message  but Mm yeah #0
B#Well you are #0
D#Yeah #0
B#We're not #0
D##0
D#Bastard #0
C#Back to the pen #0
B##0
D#You lazy #0
C##1
D##0

A#So welcome #0
A#The first kick-off meeting #0
A#What shall we do #0
A#First the opening  then the rest #0
A#What are we going to do #0
A#We m have to make a new remote control #0
C##0
A#It has to be original  trendy and user-friendly #1
A#So we will get back th on that #0
A#First we have to make a functional design #0
A#After that we have to make a conceptual design  and then after that a detailed design #0
A#So we'll discuss that later #1
A#First we have a look at #0
A#So first to we have to make a small painting #0
A#What have do we have to do #0
A#First you can save the documents #0
A#We have to do that every time we make something #0
A#You can print it #0
A#No #0
A#And we have to use the pen and the eraser #0
A##0
A#So Now #0
C##0
A#We all have to use this one #0
A#You have to make your own favourite animal #0
D##0
A#So I'll make an example #0
D#Yep #0
C##0
A#First don't touch that things #0
A##0
A#You can use the pen #0
A#And then you can make um something #0
C##0
D##0
A##0
D##0
A##0
C#Nice #0
A#Um you can change some things #0
C##0
A#Um format  line  and change it #0
A##0
A##0
B##0
D##0
A#And you can change the colour #0
C#An elephant #0
A##0
A#So that's it #0
B##0
D##0
A##0
C##0
A#So So and after it you have to save it #0
D##0
D#Okay #0
A#Now we can make a new one #0
A#You have to paint now #0
D#Oh #0
A#So you're next #0
A##0
D##0
B##0
C#'Kay #0
D#Well we will try #0
D#Where it going #0
C##0
B#Hmm #0
B#That's uh strange #0
B##0
C##0
A#What is going on #0
C#pop-ups #0
A#What are you What #0
D#Hmm #0
A##0
C##0
D##0
A##0
D##0
C#What is this  Pictionary #0
D##0
C##0
D#Uh Mm #0
A#Um Is a It is a It is a A duck #0
C#Uh a bird #0
C#Bird #0
D##0
C##0
A##0
D#So Now save #0
C#Yeah #0
A#Yes #0
A#Hmm #0
D#Now uh blank #0
A#Blank  yes #0
B#Yeah #0
D#Yeah #0
A#Okay next one #0
D##0
B#Okay #0
C##0
B#Let's try this #0
C#Whoo #0
A##0
B#Uh Um #0
D#Yeah  yeah #0
B##0
B#Mm-hmm #0
B#Mm #0
A##0
C#Oh not #0
B##0
C#Oh #0
A#Oh #0
D##0
A##0
C##0
C#Okay #0
D##0
C#Okay #0
C#Yeah #0
C#No problem #0
C#Shit happens #0
D##0
D##0
D#I'm not getting anything uh on my screen now #0
C##0
D#Okay #0
C#A parrot #0
D#Wow #0
C#Ish #0
D#Oh #0
A##0
D##0
C#He did it before #0
D##0
B##0
C##0
D##0
C##0
A##0
B#Uh No  no #0
A##0
A##0
C##0
B#Yeah #0
B#Okay #0
D##0
D#Nice #0
C#Oh #0
A#Very good #0
B#Uh blank #0
C#Thank you #0
A##0
A##0
B##0
D##0
A#Okay #0
A#Very good #0
A#So um you can always go back #0
A#So That's it #0
D##0
A##0
A#So that was two #1
A#Now next #0
A#The budget #0
A#The b Uh we will sell the t at twenty five Euros #0
A#And we have only twenty of twelve and a half Euro to make it #1
A#So now we have to think about what we will make #0
A##0
A#First I wanna hear from you #0
A#Uh what are your experiences with remote controls #0
A#So F first #0
C#Uh I will start #0
C#Uh Big one  they are uh not easy to use #0
D##0
C#Um I have one set and uh a remote control  when I dropped it  uh it broke #0
C#So that won't be uh our goal  I think #0
B#No #0
C#And uh g big buttons  m uh that's easier to use than uh I think #0
C##0
C#Not all the small buttons  you don't know Big buttons  positive #0
A#Is this positive or negative  that uh big buttons #0
A#Positive #0
C#All all small buttons like when you have uh like a hundred buttons on your remote control  you won't know what they're working for #0
A#Okay #0
A#What are your experiences #0
B#Uh well I think the the the goal of a remote control is that it's it it has an influence on the T_V_ set #0
B#And that it controls the channels and the the volume #0
A#Mm #0
B#And uh I I I think it's positive if there's a a LED uh uh a LED on the corner of the of the remote #0
B#So that you know it s it still has batteries on it in it #0
B#And that if you push the button the LED uh gives a light  and uh and you see that it's working #0
B#And uh yeah #0
B#Uh Yeah  but No no no #0
A#So and do they always have that #0
B#But I my my experience is that it it it's convenient to have that #0
A#It's easy to you #0
B#Yeah #0
A#Okay #0
B#Yeah #0
A#'Kay #0
D#Uh at home we have a T_V_  a video uh recorder  a D_V_D_ player  and a satellite receiver #0
D#We have uh four distinctive remote controls for that #0
C##0
C#Thank you #0
D#That's not really ea easy #0
C#Help also #0
D#So it would be nice if we have one for all #0
C#Thank you #0
C##0
D#And we also had a remote control for our radio set #0
D#But um i it it had a lot of buttons on it  and you didn't know which one was what #0
D#And it was uh uh v not easy to use #0
D##0
D#So we n barely used it #0
A#Okay so they have too much #1
A#So next #0
B#Hmm #0
A#For our own remote control we have to think how do we make it #0
A#So what ideas do you have for it  for the new remote control #0
A#What what does it have to have #0
A##0
C#The weight #0
C#Not not too heavy #0
A#Not too heavy #0
C#Not much buttons #0
A#Yes #0
A#Yeah #0
C#Bust-free #0
C#That when you drop it  it won't break #0
C#Like uh some kind of rubber on it #0
C#Or hard uh hard plastic #0
C#Uh buttons not too small #1
C#Uh something like when you uh lose your uh remote control  sometimes it happen #0
A#Yes #0
C#Uh it between the couch and you can't find it #0
C#When you push a but a button on the T_V_  then you hear some uh some sort of bleep #0
B##0
A#Like a phone #0
C#And then you uh  hey there there's remote control #0
D#Yeah #0
A#Okay #0
C##0
B##0
C##0
B#Yeah #0
A#So  that's #0
C#Next #0
B#Yeah well that's that are good ideas #0
B#Uh Yeah well the LED on the corner  that that indicates that it's working #0
B#If you push a button #0
B#Um Yeah #0
B#And looking on the budget  not too expensive uh material #0
B#So probably plastic or something #0
B#Uh Mm no #0
A#Okay #0
C##0
D#Yeah I think it uh from a marketing point of view  it also has to look nice #0
D#Or you won't sell it #0
A#Yes #0
D#And um yeah uh on our website we can see what products we already have #0
D#And it should work with as many uh as possible of them #0
A#Okay #0
A#This is It has to be compatible with other things #0
D#Yes #0
A#Okay #0
C#I have one more idea #0
C#Just popped up #0
A#Yes #0
C#Uh it it won't take a lot of batteries #0
C#So you don't won't have to change the batteries uh once a week or uh once every two weeks #0
A#No battery use #1
A#So more ideas #0
A#No okay #0
A#It's only the first ideas #1
A#So uh what are we going to do now is Next meeting is in half an h hour #0
A##0
A#Uh Okay #0
A##0
A#Next meeting  half an hour #0
A#Um  what you have to do #0
A#Well look on your #0
A#And Next instructions you'll get in your email #0
A#So This is the first meeting #0
A#See you later in half an hour #0
B#Yes #0
C#Okay #0
D#Okay #1
C#Thank you #0

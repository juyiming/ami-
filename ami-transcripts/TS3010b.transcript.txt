A#Okay #0
A#So welcome back #0
D##0
A#What do do we have to do #0
A##0
D##0
A#So first #0
A#I want to say I'm the secretary  so I make the minutes #0
A#You find them in your in the map in the From the group #0
A#There's the minutes from the first meeting #0
A#You'll find the next minutes also there #0
A#Then I wanna hear from you  what you've done #0
A##0
A#And after that I have some new product requirements #0
A#So And after that we have to make decisions  what we will do #0
A#And then we're ready #0
A#We have forty minutes for this meeting #0
A#After that we'll have lunch #1
A#So first I wanna ask the Industrial Designer to tell what he did #0
A##0
C#That's my task #0
A#So Yeah on your computer  or the What's the name #0
C#Okay #0
C#Uh I've Where have I put it #0
C#My Documents or not #0
C#Hmm #0
C#I've save it on my computer  my presentation #0
C#But where #0
C#Uh uh uh It was about the working of the remote control #0
A#What's the name of it #0
A#It's the technical function or the functional requirements #0
C#Nope #0
A##0
C#Not a of Wait #0
C#The working design #0
C#But I've saved it #0
A#Working design #0
C#But now I don't know where it is #0
C#Hmm #0
A#Working design #0
A#What is this #0
A#Product documents #0
C#Yeah #0
C#And I import this until One more #0
A#On the desktop #0
A#Up #0
A#up #0
A#Up #0
A#Up #0
A#Up #0
C##0
A#Yes #0
A#My Documents #0
A#Nope #0
C#What the fuck is this #0
A#Gone #0
A#Well you Um Nah #0
A##0
C##0
A#Nah  nah  nah #0
A#PowerPoint #0
A#Working design #0
C#Yeah that's the empty one #0
A##0
A#And Presentation of working design #0
C#I had one #0
C#Uh-huh #0
C#Open it #0
C#Okay here it is #0
A#Save as #0
D##0
A#Uh it's Project #0
D#Desktop #0
C#Project #0
D#Yeah #0
A#Save #0
C#Okay #0
D#Okay #0
A#Very good #1
D#Well #0
C#A little later but here it is #0
A#Okay #0
A#So Now have ten minutes to tell it #0
A##0
D##0
C#So okay #0
C#It's a little difficult what I'm gonna tell you #0
C#It's about the working of the remote control #0
C#I just had an half an hour j to study it and I don't get it #0
B##0
D##0
C##0
D#Make it #0
C#Ten minutes to tell it #0
C#Okay #0
C#I think it will be a few minutes and First uh I will tell you something about the findings  what I discovered about the remote control #0
A#Okay #0
C#The working bout it uh of it #0
C#Uh then I'll have uh some kind of map  and it's the top of the remote control #0
C#With a little bit of science  uh you I will show that uh in in a few minutes #0
C#And then uh what I'll think about it #0
C#First  the findings #0
C#The remote control is a very difficult uh thing to uh to explain to just all of you wh who haven't seen a remote control uh inside #0
C#Uh there's a lot of uh plastic on it  um because its uh not so expensive #0
D##0
C#And there are uh a lot of uh wires  uh which um connect the components in it  the battery  and there are um switches and things like that #0
C#There's a lot of small uh electronics #0
C#So it won't be um uh too expensive to build it #0
C#Only twelve Euro fifty I think uh we will make it #0
C#Now And here I have the top of the remote control #0
C#Uh here's some kind of chip #0
C#Uh on top of this  there are uh the numbers #0
C#Uh you have all on your remote control #0
C#And uh the teletext uh button #0
C#And uh here's the battery #0
C#And when you push the button  it will uh will be sent to the chip #0
C#And the chip will um send it to all kind of sub-components #0
C#That's what I said  it's very difficult #0
A##0
C#And after that it will be sent to the infrared #0
C#And that will send it to your television #0
C#That's a short h uh how it works #0
C#Uh I think I can uh make it uh difficult  but we all we all don't get it #1
C##0
C#My preferences #0
C#It's uh it won't be uh We shouldn't make it too big #0
C#Uh also for the cost  uh we should only put one battery on it #0
C#A long-lasting battery #0
C#Uh also for the cost  uh use only plastic #0
C#Not other materials #0
C#Also because of the cost  uh not too much buttons on it #0
C#We can also make uh a button uh with a menu uh button #0
C#And then um that that you will see it on the T_V_ #0
C#And on the T_V_ you can uh switch into the menu #0
C#That's I think it's easier #0
B#Mm-hmm #0
C#And the bleep signal  y uh you told us #0
C#Uh but we can also use it uh a bleep like something  when the battery's empty  then there is a bleep #0
C#Then you'll have to change it in a in a week or something #0
C#And also the bleep  when what I told you about uh when you lost it  and you push a button  and then you hear bleep bleep  and we will find it #0
C#This is uh just uh Yeah #0
A#Oh oh #0
A#Two questions #0
C#Yeah #0
A#The battery #0
A#You say one battery is cheaper #0
A#Why #0
C#If we w if we use only just one uh small pen-light  then it will be cheaper than when we use two #0
A#Yeah but when you use two  you can use it two times longer #0
C#Yeah but then we'll have to make the um remote control uh long lasting #0
A#Okay so it's the size of the remote control #0
C#Just Yeah #0
A#Okay and the buttons #0
A#When you use it on the television  you've you need the television  wh which can use it #0
C#Yeah #0
C#But uh I think this our remote control is for the televisions we uh we sell in our company #0
A#S Okay #0
C#Or is it also for other company uh for other televisions #0
A#I think we have to use it also on other televisions though #0
C#Then this is an option #0
A#So Yeah but I don't I think it They are two different things though #0
C#Maybe just a menu button to use it on our televisions #0
C##0
C#And then we make it easier uh for our televisions #0
C#And on the other tele televisions  you can also use it  but then we won't use the Yeah #0
A##0
A#We have to choose one #0
A#It has to work on o uh all televisions #0
B#Mm #0
C#Okay #0
C#Then I think uh the menu button uh will only work on the newer televisions #0
C#And we will uh look forward and don't make a remote control which for the older televisions #0
D#Hmm #0
A#Okay #0
C#And I just uh have one more idea #0
C#Uh maybe it's one of your tasks #0
C#But Uh  to have a trendy remote control  we can also um make something like the Nokia um mobile phones #0
C#To change covers #0
C#So if you have uh a trendy half with all red  uh yellow and something #0
C#And then you can put a red cover on it #0
D#Hmm #0
C#And also different things #0
A#Yeah #0
A#Good idea #0
D#Will this will this add to the cost #0
C#Yes #0
C#Uh then it won't be uh will have just one cover on the uh original one #0
C#And then you can buy the covers #0
D#Yes but you have to m uh be able to change it #0
D#D does it make it more difficult to design #0
C#I think it will be a little more difficult  but not too much #0
A#Mm-hmm #0
D#Not much #0
D#'Kay #0
C#Just like with the Nokia uh mobile phones #0
A#Yeah but there are much more Nokia telephones than um these ones #0
C#Just one #0
C#Yeah but then we'll have to to just um put five covers on it  and see if it works #0
A##0
C#If it won't works then we'll get something else #0
C#Then we uh won't g uh go further with it #0
A#Yeah but are their profits bigger than their cost #0
C#Uh a p a a cover made in uh in China  it it won't be I guess so expensive I think #0
C##0
A#Yeah but there are also design cost #0
A#I don't think When you have a remote control  do you change the cover #0
C#Maybe #0
A#Would you change the cover #0
C#I wi I won't #0
C#But maybe I think trendy people or like children where you can paint on it  and uh the the children think  oh this is my remote control  uh I made a picture on it #0
A#No #0
A#N yeah but I think that too less people would change it for good profit #0
C#Uh Yeah #0
A#So Yes it is but I don't think we have to do it #0
C#Okay #0
C#And the other people #0
D#Um Mm #0
C#What do you think about it #0
C##0
B#Yeah it's a good idea #0
B#But If if it Yeah  I don't I'm not sure if it will make profit enough to uh But it's uh yeah it's uh original idea #0
C#Okay #0
B##0
B#No #0
C#Okay #0
C#You're the Project Manager #0
D##0
A#Okay #0
A##0
C#Yes #0
C##1
C#That's it #0
A#That's clear #0
A#Okay thank you #1
A##0
A#So now the User Interface Designer #0
B#Oh #0
B#That's me #0
B#Uh Come on #0
B##0
B#Ah #0
D#Yeah #0
B#Yes well uh uh I shall give a short talk about the the technical function design #0
C##0
B#Um I thought the the the technical function design was uh to uh for a remote control to to to have some influence on the T_V_ set #0
B##0
B#Uh both audio and vide video uh in a cordless way #0
B#No cords attached #0
B#And uh well  it all by pushing a button on the remote #0
B#That was from my own experience and uh and uh the previous meeting #0
B#Uh I find some uh some interesting quotes on the web #0
B#Uh well the same idea here #0
B#Uh message to the television #0
B#And uh and and and well basic uh operations like on and off  and uh switching channels  and uh and maybe uh teletext or something like that #0
B#Uh well these are two uh remotes  and that's our uh our dilemma I think #0
B#Uh We just heard from the Industrial Designer how uh difficult it is #0
B#But uh shall we make a basic remote control  uh just uh swapping channels and volume and uh power button and well nothing much more #0
B#Or uh uh more functions on the remote #0
B#Uh maybe more devices you can influence #0
B#Uh a radio or a v a video recorder  uh V_C_R_ #0
B##0
B#Yeah well that's our dilemma #0
B#Um any ideas about that #0
B#Basic or multifunctional #1
A#We'll got back on that later #0
B#Okay yeah #0
B#Yeah well the that was just on my mind #0
D#Yes #0
B#So uh I didn't know what uh what way we would go #0
B#Mm yeah well that was my uh functional uh talk #0
B##0
C#'Kay #0
A#'Kay  thank you #1
A#Then it's your turn  the marketing expert #0
D#Okay #0
D#Uh um m Yeah #0
D##0
D#Um yeah okay #0
D#This bit too far #0
D##0
A##0
C##0
B##0
D#So So I'm uh gonna have a presentation about um the market  about um yeah what people think #0
D#Uh we did a usability lab-test with a hundred persons #0
D#And we looked at uh several um things #0
D#Uh among them design  uh d d how d did they like the use of it  uh what frustrations they had while using remote controls #0
D#Uh well what what will be our market #0
D#And uh we asked them if we had some new featu features #0
D#If um that would be a good idea or not #0
D#Well our findings #0
D#Uh our users  they disliked the look and feel of current remote controls #0
D#Um uh they especially found found them very ugly #0
D#And um th they also found them hard to to learn how to use it #0
D#Uh well they also zap a lot #0
D#So uh zapping uh should be very easy #0
D#And uh fifty percent of the users only use ten percent of the buttons #0
D#So a lot of unused buttons #0
D#There is more findings #0
D#Uh on the buttons #0
D#Which uh buttons find users uh very important and which which not #0
D#And how much would they use them #0
D#Well uh the most used button is the channel selection #0
D#And uh we asked them how uh relevant they think uh the buttons are #0
D#The power  volume and channel selections are very relevant #0
D#Uh teletext is uh less relevant but also important #0
D#Uh not important they found the audio  uh that's not the volume but uh specific the the pitch  or the left or right #0
D#Uh the screen and the brightness #0
D#And uh channel settings #0
D#Uh th and they also are not used very often #0
D#Then we have a few um graphs about the market #0
D#Uh here we can see what the market share is of uh several groups #0
D#Um as you can see  most users are uh between thirty six and forty five #0
D#Um the the the younger group between sixteen and twenty five is not very big #0
D#And to come back on the the swapping uh things  uh I don't think uh  I I think the younger will be most interest in it #0
D##0
D#But uh they are not a very big group #0
D#Um in the we asked them  uh how would you like a s a new feature #0
D#If you have an L_C_D_ on the remote control  what would you think of it #0
D#Now you can clearly see young users say #0
D#I will that would very nice #0
D#And older user think uh they will be scared of change I think #0
D##0
C##0
D#And they won't like it #0
D#And another thing  how would you like to have a speech recognition on it #0
D#Well here we see the same #0
D#Young users uh think that's an interesting idea #0
D#And old users not #0
D#Uh well we uh found out that there are two several markets at which we can aim #0
D#Uh the first are the younger  the age between sixteen and forty five #0
D#Uh they are highly interested in the features  as you can see uh here #0
D#And um they are more critical on their money spending #0
D#Uh the second group is the older group #0
D#Aged between forty six and sixty five #0
D#They are less interested in uh new features #0
D#But uh they spend their money more easily #0
D#Now if we look back at this graph  we can see that among the first group is about um sixty percent #0
D#And the second group about forty percent #0
D#So the the first group is bigger #1
D#Well then I come to my uh personal preferences #0
D#Uh yeah the first question is uh also we have to ask is at the which market do we aim at #0
D#Uh of course n uh saying we aim at the young group doesn't say that old people won't buy it #0
D#But less of them will buy it #0
D#Um well I uh Okay #0
D##0
D#What I thought  um even young people say it's hard to use  remote control #0
D#So if you make a remote control that is uh very easy to use  that's especially aimed at this group  even uh the young group will also be more interested #0
D#And um we can make special features #0
D#But uh I think it looks nice in the first time #0
D#But when use it  uh I don't know what's uh good thing of speech recognition #1
B#Mm-hmm #0
D#Um well th uh that's my second point #0
C##0
D#Uh less important functions should be discarded from the remote control #0
D#It's about discussion we had earlier #0
D#Um You can find most functions on a T_V_ set #0
D#So uh you don't have to have a lot of audio options  or screen options to change the brightness #0
D#And such things #0
D#Um well the design is very important #0
D#One thing I did not say I think  is that a lot of users also said then I would uh buy a good looking uh remote control if there will be one #0
D#But they found most remote controls very ugly #0
D#So the design of our remote control is very important #0
D#And uh yeah it should be very zap friendly  as most users use it for that #1
D#That were my findings #0
A#Okay thank you #1
C#Yeah #0
C#I have uh one question #0
C#If we aim for the younger people  um and there will be uh a lot of features like L_C_D_ or the the the speech uh f recognising  uh the cost will be a lot of h uh a lot higher #0
A#Yes #0
D#Yes #0
B#Mm-hmm #0
C#Uh I think we don't have that in our budget #0
B#No #0
C#Do you think #0
D#No #0
B#And I don't uh I don't think twenty five Euros for a remote is really cheap or something #0
C#Like No #0
C#No #0
B#So it's Yeah  it's hard to uh get the younger group #0
C#Uh-huh #0
A#I think uh the L_C_D_ is cheaper than speech recognition #0
A#So I think that can be an d good option #0
B#Mm-hmm #0
A#L_C_D_ #0
B#Just the L_C_D_ #0
A#Yes #0
A#Only the L_C_D_ #1
B#Mm-hmm #0
A#So But we'll come back on that #0
D#Okay #0
A#Now Oh  go on #0
A##0
A#What d d d um Um Uh we go back on the decisions later #1
A#Now we have a few new product requirements #0
A#First  teletext #0
A#We have internet now so we don't need the teletext anymore #0
C##0
A#So not necessary #0
A#Next #0
A#Only for the television #0
A#So we don't look at the other things like the radio or something #0
A#Only the television #0
A#Third #0
A##0
A#We look at the age group of forty plus #0
A#Uh no  younger than forty #0
A#Is a g big group  and like you showed  n not very much people buy our stuff #0
A#Fourth point #0
A##0
A#Our corporate colour and slogan must be used #0
A#Very important for the design #0
A#So you can see it on our site #0
D##0
A#Next #0
A#Um no #0
A#We have to make our decisions  what we want to do #0
A#So like you said  we need the #0
A##0
A#Maybe it's good to put it in a document #0
A#Now we have to decide what controls do we need #0
A#So maybe you can tell us #0
D#Yeah maybe we can first have a discussion uh on the the product requirements you just uh said #0
A#Sorry #0
D#The the requirements you just said  maybe we should first have a discussion about that #0
A#Yes #0
A#Yes  it's okay #0
D#I uh personally think uh teletext is a good option #0
D#Uh not everyone um who is looking T_V_ can go to internet when they want to see the latest news #0
A#Yeah but we don't use it #0
A#It's a new requirement #0
D##0
A##0
A#So  it's not my requirement #0
C#'Kay  we'll just have to do that #0
A#We have to do this #0
C#Okay #0
D#Okay #0
C#No discussion about it #0
D#Okay sorry #0
D#Then uh Mine #0
A#No #0
B##0
C#Okay #0
D##1
C#Unfortunately #0
C##0
A#So what controls do we need #0
A#Who first #0
B#Well a power button #0
D##0
A#Okay #0
A#Uh power #0
B#Uh the well um I think separate channels #0
B#So But then both the the separate channels #0
A#Uh mm channel #0
B#So so uh zero to nine or something #0
A#Channel Zero to nine #0
B#Uh volume #0
A#Volume #0
A#Maybe it's easy to pick #0
A#What was w your one #0
A#Techno Okay #0
D#It's the functional requirements #0
A#We had w uh no no no no #0
A#Where was that example of the Johan #0
B#Oh mine #0
A#That was the the the the the technical Hallo #0
B#Technical #0
A#Okay #0
A#What do we need #0
A#On-off #0
A#Zero to nine #0
C#To change to the next channel  just one button #0
C#To move up  move down #0
A#Yeah that's the channel #0
D#D Yeah #0
D#Do we make a menu #0
A#Menu #0
A#Uh yes the n newer televisions ha do have menus #0
A#Uh M Menu #0
D##0
D#Uh I think it will be um q quite easy to use  to have uh uh four arrows #0
A#I think um the only one or two numbers #0
B#Mm yes #0
A#And Hello #0
A#That's ch Yes #0
A##0
D#Up-down for channel selection  and left-right uh for volume #0
D#And uh a menu uh button #0
D#And if you press the menu button you get into the menu  and you can use the same buttons #0
D#But the then to scroll through the menu and to change the options #0
A#On the L_C_D_ screen  you mean #0
D#Uh well yeah that depends on if you have uh the menu on the T_V_ #0
D#Or you get the menu on the L_C_D_ screen on the remote control #0
A#Think it's better to have it on the remote control  'cause it it has to work on all televisions #0
A#So we need N Yes #0
D#Yes #0
C#But then we come to the costs #0
A#But if we have this So now we don't have a lot of buttons #0
D#'Kay #0
D#But well if you aim at the younger market  um a as they as uh s uh as we seen in the usability uh lab  uh they will buy a nice looking um remote control #0
D#And also to find the easy to use uh part very important #0
D#So if we have a L_C_D_ sh uh screen  and uh not too many buttons  I think that will incre uh uh even when it's a bit more cost  it will still sell #0
A#Is this enough #0
B#Mute #0
A#Mute #0
A#Maybe in the menu #0
B#Um Yeah but then it's always uh more than one uh thing to do #0
D#Mm #0
A#Mute #0
A#Mm-hmm #0
D#Yeah #0
A#Okay #0
A#Maybe more #0
A##0
A##0
A#No #0
A#Well #0
A#Then that's all #0
A#This will be the buttons #1
A#And I think that's enough for the next phase #0
A#So we can go on to Yes #0
C#But now we have only the buttons #0
C#And uh we don't yet have to decide what the remote control would look like #0
C#Or Okay #0
A#No that's for the next phase #0
A#Um Phase two is the conceptual design #0
A#So then we'll have the concepts #0
C#Okay #0
C#Okay #0
A#That's for the So uh next point #0
A#Now we have lunch-break #0
A#After that we have t thirty minutes for work #0
A#And you can find the minutes in the Project Documents folder inclusive the uh buttons #0
A#No #0
A#Your individual action  you can find them in the email #0
A#So now it's time for lunch #0
C#Okay #0
D#Okay #0
D#Good idea #0
A#Thanks for coming #0
C##1

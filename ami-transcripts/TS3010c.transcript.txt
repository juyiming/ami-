A#Okay #0
A#Well  let's start #0
A##0
A#What are we doing #0
A#Oops #0
A##0
B#Hmm #0
C#Ah  pinball #0
A##0
C##0
B##0
A#Okay #0
A#Okay #0
A#Not doing #0
C##0
D#Mm #0
A#Uh Very good #0
D#Ah #0
B#Oh #0
D#Hey #0
D#Ah #0
D##0
D#Now I have my screen back too #0
C##0
D##0
A##0
A#Okay #0
C##0
D#Yeah #0
A#we have presentations #0
A##0
A#So first  it's your turn #0
B#Mine #0
B#Oh  great #0
A#Yeah #0
A##0
C#Huh #0
A#Isn't it amazing #0
D##0
C##0
D#Yeah #0
A##0
D#Very interesting #0
B#Uh Yes  well uh let's uh talk about the interface uh concept #0
D##0
C##0
B##0
A#Industrial Designer #0
A#Interface concept #1
B##0
B#Uh  first I'll uh I'll uh discuss the buttons we just chose  uh show you some samples  uh uh discuss some colours and design maybe  already #0
B#And uh my personal preferences #0
B##0
B#Well we chose the power button to switch the television on and off #0
B#The bu uh the mute button to switch the volume on and o on and off #0
B#The channels buttons  one to nine  and uh off uh uh zero to nine  and the uh button to choose uh higher channels than nine #0
B#Uh the volume and channel quadrants  uh left and right  up and down arrows  to uh do the volume and channel #0
B#And the menu menu button to man manipulate the L_C_D_ uh display #0
B#Um  I found some uh interesting uh uh samples #0
C##0
B#Examples #0
B#Um  well uh what's pretty standard is uh that it's that they're all pretty uh uh high uh Yeah #0
D#Large #0
D#A lot a lot of buttons buttons #0
B#Yeah #0
B#Large and and and pretty thin and uh and long #0
B#Um  power buttons are mostly at the top uh left or right #0
B#Um  well we see the the the same uh arrows #0
B#Like there #0
B#And uh Yeah  well arrow b buttons can be blue #0
B#And what's interesting is the the the icons on the buttons #0
B#Some buttons have icons like the play and stop  but we don't use that #0
B#But uh  these we we have to choose the right icons  or or letters #0
B#Uh this is the V_ for volume  but they're both uh a V_ #0
A##0
B#So it's it's not really very uh clear what's the function of that #1
D#Yeah #0
B#Um Yeah #0
B#So  that's Uh  well Yeah #0
A##0
A#Can you go back one page #0
A#For the uh menu  what do we use for that #0
A#We don't have buttons for the menu #0
A#Or we may have to use channel of the volume and channel #0
B#I thought that was our uh idea #0
A#Okay #0
B#So  uh how Like this #0
A#But uh You have to put it on the Yeah #0
B#Or or this #0
B#And that the menu button is okay #0
A#Yeah but  has to be clear that you can use the arrows #0
B##0
B#Yeah  okay #0
D#Yes #0
B#Uh  so the The icons on the arrows  as well  you mean #0
A#Mm-hmm #0
A#Yes #0
A#The second one #0
B#Yeah #0
B#Uh  well that's something to uh think about #0
B##0
A#Okay #0
C#Uh  maybe I'll have something in my uh presentation #0
C#And you will see it #0
B#Okay #0
D#Okay #0
D##0
B#Um  well I don't I don't know if we have to discuss this already  or in the next uh meeting #0
C##0
B#But uh  as we have to uh to to design the the case and the whole uh remote control in our uh our our corporate uh company uh uh colours and the logo  I would uh recommend a yellow case #0
B#Uh  round edges #0
B#The logo at the bottom #0
B#And uh  well maybe each each uh set of buttons uh has uh has his own colour #0
B#So  it's good #0
B#Uh  recognisable #0
B#K so  I think #0
A##0
A#Not too much colours #0
B#Uh  no #0
B#Not too much #0
B#But uh No  no  no #0
A#No  it's not flower power #0
B#But this has to be has to be trendy and uh and Uh  yeah so good uh good icons on the buttons  and uh and big buttons is my uh personal uh opinion #0
A#S okay #0
B##0
A#Okay #0
B#That was that #0
A#Thank you #1
A#So  you're next #0
C#I'm next  okay #0
C##0
C#Yes #0
C#No #0
C#Here we go #0
D##0
A##0
C#Uh  at first we will uh I will f uh say something about what younger people want  our uh group w uh w uh we want to uh sell our remote controls to #0
D#Okay #0
A##0
D#Mm-hmm #0
A##0
C#And then  I'll discuss what my opinion is about the costs  about uh what battery is in it  what kind of buttons also #0
C#First uh  the younger people  they want like soft mat uh materials and primary colours #0
C#Like  totally yellow  totally red #0
C#Uh  so it's visible #0
C#Uh  the shapes are curved and round  like uh you also said #0
C#Maybe it's nice to uh get a remote control not like all the other ones  straight and uh flat and long #0
C#But to give him the shape of your hand  so you it's easier to use or something like that #0
C#But that's just an idea #0
C#And then  I'll have to discuss about the costs uh of all the things for the remote control #0
C#The battery  there are few options #0
C#Uh  I think the best option is to use uh the basic battery #0
C#So  everybody can buy it uh at the at the supermarket #0
C#Or use uh a k uh kinetic battery like uh within a watch #0
C#When you uh shake it a few times  it it's loaded #0
C#Uh  the the form of the remote control  I think it's also nice have it curved #0
C#And maybe like it's hand-shaped #0
C#Uh  so uh you take it here in your hand and here are the buttons #0
C#Uh material  you use plastic #0
C#Hard plastic uh because uh it won't have to burst uh like in the in one time #0
C#And also rubber because the younger people like that  what we see in the research #0
C#Uh the push-buttons #0
C#We have one new thing uh discovered #0
C#It's a scroll push uh thing like a mouse #0
C#Maybe it's uh easy to use uh for the channels #0
C#When you want to go m move up  you just scroll up and click on the button  if you wanna see the next  uh if you wanna see that channel #0
C#And also for the mouse  uh for the volume  it's also uh easy to use #0
C#Just scroll a bit up  scroll a bit down #0
C#And that's also easy just w when you have a thing like this  and you get it here #0
C#You can do it with your thumb #0
C#And with your l left hand you can uh push the buttons uh if you push uh channel one  you can see channel one #0
D#Hmm #0
C#The electrics um with a scroll push uh button  we must use regular uh chips #0
C#There are also uh simple chips #0
C#They are uh cheaper #0
C#Um  but then we have just a basic uh uh remote control  and I think there are a lot of those uh things  and people won't buy it any more #0
C#They have seen enough of it #0
C#And you have also advanced um chips #0
C#But that's with the L_C_D_ uh screen #0
C#And the costs will increase a lot more #0
C#And I think our budget is too low to use and an L_C_D_  and the chip who is more expensive #0
C#And maybe it's also then uh thoughtful if we u uh use uh as um different kind of uh shapes for the for remote control  that we then use the primary colours #0
C#Like  you get a yellow uh remote control  red one  blue one  et cetera #0
C#You have any more questions about this #0
C#I think the main thing is we look at the costs #0
D#Yep #0
A#Hmm #0
C#And not too basic  not a basic remote control  who everybody already has #0
D#Yeah #0
D#But  thi i uh This is with an L_C_D_ #0
D#No  not #0
C#Not with an L_C_D_ #0
A#No  isn't #0
C#No #0
A#But the L_C_D_ is easy when you use the scroll uh buttons #0
A#Then you can scroll  you see what number  and then you push #0
C#Yeah #0
C#But then  what I say  the costs will uh get a lot higher #0
A#But then it's not easy to use scroll uh wheel #0
C#Th then you'll see it on the television #0
A#If you don't Hmm  yes #0
A#But then #0
A#Yeah  then you go one down one up #0
A#When you scroll #0
C#Yeah but l when you see a menu uh on the television  it's like you see uh one to twenty  you go uh uh s scroll up  and push number tw twenty #0
A#Yeah but like we said before  it has to be used on every television #0
A#So you may not be uh No #0
C##0
A#The television must do that #0
C#Mm-hmm #0
A#So Yeah but young people have to have all their uh room #0
C#Yeah  I think the younger people will have newer televisions  which can provide our uh remote control #0
A#And mostly they are smaller #0
C#Yes #0
A#So Most the times that are not advanced televisions #0
C#But that won't be a problem #0
C#I think #0
C#No  but then we'll get to the regular uh remote controls #0
D##0
C#And I think  what I said  everybody has them uh has them already #0
C#And they go to a uh supermarket and buy them uh for two Euros #0
C#Uh  and ge and get the most cheapest uh thing #0
C#And I think we must look further to uh to devel d develop something news #0
A#Okay #0
D#Uh  can you give an indication in b uh in the cost difference between uh the chip with L_C_D_ or without #0
C##0
C#I got it on my screen and it was uh higher #0
C#But I don't know uh how much higher #0
D#'Cause it I think if we have an L_C_D_  it will also sell a lot better #0
C##0
A#That's important #0
C#That's true #0
D#And that might uh bring back the costs uh Huh #0
C#But then we'll I think we must discuss who uh what will be better #0
C#If we have a better shape of the um remote control  or better options on it #0
C#With a scroll menu  a w scroll thing  and a L_C_D_ #0
C#And then a flat um remote control #0
C#Or  a more hand-shaped remote control  with scroll  without L_C_D_ #0
A#Yeah #0
A#Maybe you can look how how much it is for the L_C_D_ #0
C#I can uh look on my uh Uh Note that the push-button requires a simple chip chip #0
A#It's very important #0
A##0
C##0
C#A scroll wheel requires minimally a regular chip  which is a higher price range #0
C#The display requires a advanced chip  which is which in turn is more expensive than the regular chip #0
A#Yeah  more expensive #0
A#But how much #0
B##0
C#Doesn't say #0
A##0
C##1
A#Oh #0
B#Hmm #0
A#Okay #0
C#That's from my manufacturing division #0
A#Okay #0
D#'Kay #0
C##0
D##0
A#Well  thank you #1
A##0
D#My turn #0
C#Yes #0
A#Next #0
C##0
D##0
D#Mm #0
D#So So Yeah #0
D#So  my uh presentation is about trend-watching #0
D#Uh  I did some trend-watching #0
D#It's very important to uh keep up with latest trends #0
D#'Cause if you don't  you won't sell #0
D##0
D#So  well how we did do that #0
D#Uh  well we made an investigation of the market  by Trendwatchers #0
D#They uh watch in uh cities like uh Paris and Milan #0
D#Of course  well known for their uh trend uh trends #0
D#And well  uh what did you find #0
D#Uh  we have two groups  young and trendy  and the old and rich #0
D#Well th and the young and trendy  they uh they starting to like uh fruit and vegetables uh as a theme for n uh clothes  shoes  and also uh products #0
D#And um  material #0
D#That should feel have uh a spongy feeling #0
D#And to get a feeling for what it is  uh here is an image of it #0
D#Then the old and rich #0
D#They like uh dark colours  and simple  recognisable shapes #0
C##0
D#And um  they also like uh familiar material  uh especially wood #1
D#Now  another picture #0
B##0
D#To get a feeling for this #0
D#Well  uh then already come to my personal preferences #0
D#We uh aim at the younger market #0
D#So  we should also be uh look at their uh trends #0
D#However  with trends it's always if there's it's now #0
D#It it it might last one year  and next year it be uh can be totally different #0
D#And I think we want to sell our product for longer than one year #0
D#So  we m must not just only look at what the trend is now  as it might be totally different next year #0
D#So  that's uh one thing to keep in mind #0
C#Changing covers #0
D##0
B##0
A##0
D#Yeah #0
C##0
D#Any questions #0
C#Nope #0
B#No #0
A#No #0
A#It's clear #1
A#So now  it's uh Ah  let's see #0
A##0
A#Now  w we have to decide Well  we have to decide on the concept #0
A##0
A#So  we have to look at 'S next #0
A#Components and user interface concept #0
A#So Now  we have to make some concept #0
A#Maybe one of you can paint it on the board #0
A#First  uh user interface #0
C#Uh  uh-uh #0
C#How w how we how we make it #0
C#Uh Shouldn't we first discuss about like what w we all Yeah  but if I paint with More like something M like Yeah I can't dr I can't draw it #0
A#Yes  a concept on uh Just Yeah  but maybe we can paint it #0
C##0
D##0
A##0
D#Yeah #0
A##0
A#Uh  what do we want #0
A#I'll paint #0
C##0
A#Okay #0
D##0
A#Well Something like this #0
D#Mm #0
A#Or Shapes or What do we need #0
B#Mm  yes #0
D#Can make several uh concepts #0
B#What #0
A#Yes  okay #0
D#We have this  and we had the idea of an um a more uh uh uh like sh in the shape of your hand #0
A#Okay #0
D#Yeah I I I uh yes #0
B##0
D##0
D##0
C#Uh I have to #0
A#And you have to #0
B##0
C##0
A#Yeah #0
C#I'm not a designer #0
C#It's more three D_ #0
C#Like  um when you have a part here #0
C#This is the remote control #0
C#And then you have something like th this under it #0
C#So  it's easier to get it like this #0
B#Mm #0
B##0
A##0
B#Yeah #0
D#Yeah #0
C#It's like a gun #0
C##0
C##0
D#A g Mm #0
B##0
A#So  it has to be soft #0
C##0
D##0
C#And it has to be soft  yeah #0
D##0
A#Okay #0
C#So  you can squeeze in it and Sorry #0
A#And uh  the buttons #0
A#Buttons #0
C#Buttons on top of it #0
C#And here #0
C#The scrolling #0
C#You can do it with your thumb #0
A##0
C##0
B#But but i that's the only scroll uh button on it then #0
C#But now we use one scroll button and the other one is here #0
A#No  it won't #0
C#One till uh uh zero till nine #0
A#But  well there one for the sound and one for the channels #0
B#Yeah  okay #0
B#But but how Yeah #0
D#Yeah #0
C#And the b Yeah #0
C##0
B#How Okay #0
C#Or two buttons #0
D#And i if we go to uh But if we have uh a me Yeah #0
A#Uh  two scroll uh wheels #0
D##0
C#If uh 'Kay c If we do If we use one  then we'll have just a switch on it  and you'll just switch it  and now it's the sound to switch back with the menu uh button #0
D##0
B#Mm #0
A#That's th that's more difficult #0
A#It's better in But if we don't have a L_C_D_ we don't have a menu #0
D#If we have a menu  uh how do we uh choose other options #0
C#And then you also can scroll uh scroll in it #0
C#Just not like all the other ones  with uh this thing  and uh here an arrow  here an arrow  here an arrow  here an arrow #0
D#Yeah #0
C#Because uh  from h hundred uh remote controls  ninety nine have it #0
B#Yeah #0
C#Uh  then we have it on the T_V_  the menu #0
D#Uh-uh #0
A#Yeah  but again maybe th How do we know the T_V_ can handle it #0
C##0
A##0
B##0
A#You don't know #0
A#So  there's no menu #0
C#I don't know #0
C#It's like some sort of uh teletext option  but we don't have teletext #0
C##0
A#No #0
A#So you can't use it #0
C#And if we put an L_C_D_ thing on it  then the costs will uh be much higher #0
A#Okay  we make two concepts #0
A#One with L_C_D_ #0
A#One without L_C_D_ #0
C#'Kay #0
C#But you all like this kind of thing #0
C#Uh With the scroll button #0
A#Good concept #0
A#But That's one #0
C#And and this one has to be soft #0
D#Uh-uh #0
C#And this has to be harder  because when it falls  it mu mu must not burst #0
C#Or some kind of rubber around it #0
B#Mm-hmm #0
A#It's one #0
A#Two #0
A#Number two #0
B#And you can and you can uh make the the power button as a trigger #0
C##0
A##0
B#Like uh Just to uh Oh  like a Yeah #0
C#Yeah #0
B##0
B##0
C#Yeah #0
C#Ah that's nice #0
C##0
B##0
D##0
C#Here #0
C##0
C#Trigger #0
A#No #0
A#But when you handle it  you put it on and off #0
A##0
B##0
C##0
A#It's not good to use #0
C##0
A##0
A#Yeah  but I'll zap #0
D##0
B##0
A##0
B##0
A#Fuck #0
A##0
C##0
A#Out #0
A##0
A#No  it's not good #0
D#Yeah #0
A#Now  second concept #0
A##0
A#One with L_C_D_  one without L_C_D_ #0
A#Then uh Paint it #0
B##0
D##0
C#Paint it #0
A##0
B##0
C#With the scroll thing on  like this #0
A#One with two scroll buttons and one with without #0
A#Yeah #0
A#Uh  one with a with a menu  and one without a menu #0
C#So #0
A#And the one with with a menu has an L_C_D_ #0
C##0
C#Draw it #0
D##0
A##0
A#Unbelievable #0
C##0
A#Do I have to do everything #0
A#Blank #0
A#You have Not so difficult #0
A##0
D##0
B#But if you put push the the menu button Yeah  wh what Yes  but you don't know which of the scroll buttons you have to choose #0
D#Yeah #0
A#Uh  that's the menu #0
A#There for the L_C_D_ screen #0
A#You have to For the menu #0
B#Yeah #0
A#Mm-hmm #0
A#One that way #0
A#And one that way #0
A#So Then it depends on the cost #0
A#S On and off #0
D##0
C#But is it easy to use #0
C#When you have it on your left side  and Separate  more separate  h yeah #0
A#When it's not too big #0
A#Just like a a phone #0
B#Mm-hmm #0
D#M uh yeah  maybe it's better if the uh scroll-wheels are um more separate  yeah #0
A#Yes  okay #0
D#Like  you have the menu button in between uh Yeah #0
C#Yeah #0
C#On the left a scroll button  and on the right a scroll button #0
C#But would it be easy to use then #0
C#If it's like you have a big uh I also think this concept is not what the young people were looking for #0
C##0
A##0
C##0
A#Very good #0
A#Is it better #0
A#When you uh the menu  you have to go there there there there #0
C#They were like round curves  uh different uh Okay  okay  okay #0
A#Yeah  okay #0
A#That's that's the outside #0
A#But now the First the buttons #0
C#Mm-hmm #0
D#Think we have we have now two buttons missing #0
D#The uh um The mute button #0
A#Sorry #0
D#We have two buttons missing #0
D#The mute button #0
D#And um  the to to uh have to uh numbers Okay #0
A#Mute #0
A#And the other #0
C##0
B##0
D##0
A##0
A#Yeah #0
A#Not so difficult #0
D#But  uh Okay #0
D##0
C#Personally  I think two scroll buttons uh aren't easy to handle #0
A#But how do you wanna solve it #0
C#With the switch button #0
A#Yeah but on the menu that's not uh easy #0
A#Then you go down  you switch  you go into the right  you switch  you go down #0
C#No like uh Oh  you mean like that #0
A#Yeah #0
C#Uh  then you can also have like uh th um Yeah  and joystick  I think #0
A#A joystick #0
A#Yeah #0
A#But is it uh Does that break  a joystick #0
A#Or a small one just like in a laptop #0
C#Yeah like in a laptop  s uh s some sort of thing #0
C#A little bit bigger  with easier thi I don't know #0
A#Mean  it's better #0
A#But how expensive it is #0
A##0
D##0
A##0
A#Oh #0
A#Why do I pay you for #0
C##0
A##0
A#Um  well um Better ideas #0
A##0
A##0
A##0
C#Or no scroll uh things #0
C#Just a shape #0
C#And No  no #0
A#For the young peoples I think scroll button's good #0
C#It won't work #0
C#Yeah #0
C#Uh-huh #0
A#So Think we have to keep them #0
C#Or a remote control more like joystick #0
A#Yeah  but is it That's not expensive than uh Joystick is better #0
A#A small one #0
C#A small one like this  like a Nintendo uh k Playstation thing #0
A#No just like in a a laptop #0
A#Small  round #0
D##0
A#Then it's not so big #0
C#No  no  no #0
C#I mean the the shape of the remote control #0
A#Oh the sh Yeah  but then you can You have to use t with one hand #0
C#Just like a Playstation thing #0
C#Yeah #0
A#So Maybe  if it's possible  it's not too expensive  I think a joystick is better #0
D##0
A##0
A#A small one #0
A#So  please look at it #0
C##0
C#No  that's okay  I got Uh they're not uh in details #0
D#And on the L_C_D_  how much it costs #0
D#Uh  it costs extra #0
C##0
A##0
C#It's more expensive or less expensive  huh #0
A#Yeah we I think you get it #0
C##0
A#So  after this meeting you have half an hour to uh fix it #0
C##0
B##0
C#Then I have to come with it #0
D##0
A#Yes #0
C#I got my personal costs #0
C#I I don't I don't know the costs #0
A#Your problem #0
C##0
D##0
A#Not mine #0
C#Then I'll uh make something up #0
A#Okay #0
A#So  do we have other concepts #0
A#Then for the components  we use a normal battery #0
B#Mm  yeah #0
A#Then it's Ch cheapest way  I think #0
C#Yeah  or the or the kinetic uh with normal battery #0
A#No  no kinetic #0
C#Yeah  I think it's uh  yeah  more expensive #0
A#Kinetic is uh ch makes it more expensive #0
C#Yeah #0
A#So we use a normal battery #0
B##0
D#Yeah #0
B#Yeah #0
C#Okay #0
D#Yes #0
A#Chip #0
A#Depends on the L_C_D_ #0
C#Depends on the scroll #0
A#Scroll #0
C#If we use a scroll  then we have the uh regular chip #0
C#If we don't use a scroll  then we can use the simple chip #0
C#And that's Uh the most expensive #0
A#Yeah #0
A#And uh  we If you use the L_C_D_  we have to Yes  okay #0
C#Yeah #0
A#So  depends on the L_C_D_ and the scroll #0
C#If we Yeah #0
A#Okay #0
C#If we No okay scroll-wheel #0
C#So  I have this #0
C#So  it will be uh the advanced chip  or the uh regu uh or the regular chip #0
A#Okay #0
A#So  uh the shapes of the design depends on the L_C_D_ and But  it has to be small #0
A#I think #0
C#Or shall we just put it on the pistol thing #0
C#And then just put also on L_C_D_ on it #0
A#If you have pistol  it L_C_D_'s not easy #0
A#Y y Yeah but If you use a phone #0
C#Just use your thumb #0
A##0
C#If you Yeah #0
C#I use my thumb #0
A#k Yeah  but but then you have it #0
A#Like  th if you have pistol  you have it so #0
C#Yeah #0
A#And the screen is Well  then you have to keep it this way to look at the screen #0
C#If you have a joystick on No  if you have like uh an uh uh a ni a uh Playstation uh game controller #0
D##0
A#Yeah #0
C#And you move up  f forward  down  left #0
C#Then you have uh just  yeah  a little bit curved #0
C#It's not just uh straight #0
A#No #0
C##0
D##0
A#No  no #0
C##0
C#That's how we use it #0
D#Uh Uh  yeah  but Yeah #0
C#That's why they make joysticks like that  I think #0
A#Yeah  but then you look forward #0
A#And then you can y N well  if you have to look at it #0
C#Yeah #0
D#If you If we have uh then something standing here  with the L_C_D_ #0
C#'Kay #0
C#Here's our designer #0
C#Yeah #0
A#Then it goes like this #0
C#Yeah  why not #0
A#If th n well Yes  of course #0
A##0
C#It's for the younger people #0
C#It's something new #0
C##0
B#It's uh Or you can uh turn it inside #0
A#That's good good #0
A#But the um  it may not break #0
C#Now we put uh rubber around it #0
A#Okay #0
A#If that's possible #0
D#Um  Yeah #0
C#Hard plastic  uh the shape  and around it hard uh around it rubber #0
C#And the uh the hand shape is also rubber #0
A#Okay #0
D#I can't see the #0
D#But  uh the easy of uh  th the ease of use wasn't uh the most important uh aspect of it #0
B#But that's No  that's true #0
D##0
C#Huh #0
D#Uh  for us it's about to sell it #0
D#Uh One #0
A##0
A#Yeah of course #0
C#This is something new #0
D##0
A#Okay #0
A#Then this is the design #0
A#And the buttons are on the next page #0
A#So  depends on the cost #1
A#So  um we have one minute #0
C#Costs are okay #0
A##0
B##0
D##0
B##0
C##0
A#I think #0
D#No #0
A#No #0
D#You have more #0
A#More #0
A#Seven #0
D#You have still ten #0
A#Next meeting #0
A#Thirty minutes #0
A#So hurry up #0
C#Oh  that's us together #0
A#You two stay here #0
D##0
A#Paint it #0
A##0
B#Okay #0
A#Now you have to #0
C##0
B##0
D##0
A#So I think it's clear #0
A#Check your mail #0
A#So It has to be ready in the next meeting #0
D#Yeah #0
C#Yes #0
A#So Next meeting is called the detailed design #0
D#What #0
B#Cookie #0
A##0
D#Okay #0
A#So Everyth everything has to be ready #0
D#Okay #0
A#Thanks for your attention #0
C#'Kay #0
A##0
A##0
D#See you at the next meeting #0
C#Bye bye #1

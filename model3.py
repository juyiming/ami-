from __future__ import unicode_literals, print_function, division

import torch
import torch.nn as nn
import torch.nn.functional as F
import config
from numpy import random
import numpy as np

embed_weight = np.load('./embed_weight.npy')
print(np.shape(embed_weight))
config.vocab_size, config.emb_dim = np.shape(embed_weight)

config.use_gpu = True
use_cuda = config.use_gpu and torch.cuda.is_available()
print(torch.cuda.is_available())
print(torch.cuda.device_count())
print(torch.cuda.get_device_name(0))
print(torch.cuda.current_device())

config.hidden_dim = 256
config.pointer_gen = None
config.persondim = 100


random.seed(123)
torch.manual_seed(123)
if torch.cuda.is_available():
    torch.cuda.manual_seed_all(123)


def init_gru_wt(gru):
    for names in gru._all_weights:
        for name in names:
            if name.startswith('weight_'):
                wt = getattr(gru, name)
                wt.data.uniform_(-config.rand_unif_init_mag, config.rand_unif_init_mag)
            elif name.startswith('bias_'):
                # set forget bias to 1
                bias = getattr(gru, name)
                n = bias.size(0)
                start, end = n // 4, n // 2
                bias.data.fill_(0.)
                bias.data[start:end].fill_(1.)


def init_linear_wt(linear):
    linear.weight.data.normal_(std=config.trunc_norm_init_std)
    if linear.bias is not None:
        linear.bias.data.normal_(std=config.trunc_norm_init_std)


def init_wt_normal(wt):
    wt.data.normal_(std=config.trunc_norm_init_std)


def init_wt_unif(wt):
    wt.data.uniform_(-config.rand_unif_init_mag, config.rand_unif_init_mag)

#对一个sentence进行encoder， 输出每一个单词的隐状态，最终表示，说话人的embedding表示
class Encoder(nn.Module):
    def __init__(self):
        super(Encoder, self).__init__()
        self.person_embedding = nn.Embedding(5,config.persondim)
        self.embedding = nn.Embedding(config.vocab_size, config.emb_dim).cuda()
        self.embedding.weight.data.copy_(torch.from_numpy(embed_weight))
        self.gru = nn.GRU(config.emb_dim, config.hidden_dim, num_layers=1, batch_first=True, bidirectional=True)
        init_gru_wt(self.gru)

    def forward(self, input,person):
        embedded = self.embedding(input)
        person_embd = self.person_embedding(person)
        output, hidden = self.gru(embedded)
        encoder_outputs = output

        return encoder_outputs, hidden ,person_embd # # B * t_k x 2*hidden_dim,  # B * t_k x 2*hidden_dim,      B , 2, hidden_dim

#一句话(utterance)中，根据encoder最终输出d，对每一个单词计算attention, 加权后计算句子表示
# 输入：d（整篇文章encoder后的最终隐状态） encoder_outputs（文章encoder后每一个单词隐状态）
#attention对encoder_outputs加权，得到句子表示c_t
# 输出： 句子表示c_t, attention表
class wordAttention(nn.Module):
    def __init__(self):
        super(wordAttention, self).__init__()
        # attention
        # if config.is_coverage:
        #     self.W_c = nn.Linear(1, config.hidden_dim * 2, bias=False)
        self.Wd = nn.Linear(config.hidden_dim * 2, config.hidden_dim * 2)
        self.Ww = nn.Linear(config.hidden_dim * 2, config.hidden_dim * 2)
        self.Wp = nn.Linear(config.persondim, config.hidden_dim * 2)
        self.v = nn.Linear(config.hidden_dim * 2, 1, bias=False)


    def forward(self, d, encoder_outputs, person):

        b, t_k, n = list(encoder_outputs.size())
        # print(b)    1
        # print(t_k)
        # print(n)    2*h
        dec_fea = self.Wd(d)  # B ,1 , 2*hidden_dim
        dec_fea_expanded = dec_fea.expand(b, t_k, n).contiguous()  # B x t_k x 2*hidden_dim
        dec_fea_expanded = dec_fea_expanded.view(-1, n)  # B * t_k x 2*hidden_dim
        p_fra = self.Wp(person)
        p_fra_expanded = p_fra.expand(b, t_k, n)
        p_fra_expanded = p_fra_expanded.view(-1, n)

        encoder_feature = encoder_outputs.view(-1, 2 * config.hidden_dim)  # B * t_k x 2*hidden_dim
        encoder_feature = self.Ww(encoder_feature)

        att_features = encoder_feature + dec_fea_expanded + p_fra_expanded  # B * t_k x 2*hidden_dim
        # if config.is_coverage:
        #     coverage_input = coverage.view(-1, 1)  # B * t_k x 1
        #     coverage_feature = self.W_c(coverage_input)  # B * t_k x 2*hidden_dim
        #     att_features = att_features + coverage_feature

        e = torch.tanh(att_features)  # B * t_k x 2*hidden_dim
        scores = self.v(e)  # B * t_k x 1
        scores = scores.view(-1, t_k)  # B x t_k

        attn_dist_ = F.softmax(scores, dim=1)  # B x t_k
        normalization_factor = attn_dist_.sum(1, keepdim=True)
        attn_dist = attn_dist_ / normalization_factor

        attn_dist = attn_dist.unsqueeze(1)  # B x 1 x t_k
        c_t = torch.bmm(attn_dist, encoder_outputs)  # B x 1 x n #attention 对encoder_outputs加权，得到句子表示c_t
        c_t = c_t.view(-1, config.hidden_dim * 2)  # B x 2*hidden_dim

        attn_dist = attn_dist.view(-1, t_k)  # B x t_k

        # if config.is_coverage:
        #     coverage = coverage.view(-1, t_k)
        #     coverage = coverage + attn_dist


        return c_t, attn_dist  # ,句子表示c_t, attention表

#对之前计算的句子表示做encoder,用来下一步计算segponit.
class SegEncoder(nn.Module):
    def __init__(self):
        super(SegEncoder, self).__init__()

        self.gru = nn.GRU(2 * config.hidden_dim, config.hidden_dim, num_layers=1, batch_first=True, bidirectional=True)
        init_gru_wt(self.gru)
        # self.W_h = nn.Linear(config.hidden_dim * 2, config.hidden_dim * 2, bias=False)

    # seq_lens should be in descending order
    def forward(self, input):
        # packed = pack_padded_sequence(embedded, seq_lens, batch_first=True)
        outputs, hidden = self.gru(input)
        # encoder_outputs, _ = pad_packed_sequence(output, batch_first=True)  # h dim = B x t_k x n)
        return outputs, hidden  # # B  ,t_k x 2*hidden_dim,        B , 2, hidden_dim

class Segdecoder(nn.Module):
    def __init__(self):
        super(Segdecoder, self).__init__()
        self.gru = nn.GRU(2 * config.hidden_dim, 2 * config.hidden_dim, num_layers=1, batch_first=True,
                          bidirectional=False)
        init_gru_wt(self.gru)

    def forward(self, h_in, startutter_exp):
        outputs, h = self.gru(startutter_exp, h_in)
        return h


#用get2theponit的方法计算segponit,计算每一个点的分布，取值最高的作为当前分段的结束点
#输入： 隐状态d，句子encoder后的表示outputs, 当前段的start位置，
#输出： 当前段的结束位置， 分布表
class SegPonit(nn.Module):
    def __init__(self):
        super(SegPonit, self).__init__()
        # attention
        # if config.is_coverage:
        #     self.W_c = nn.Linear(1, config.hidden_dim * 2, bias=False)
        self.Wd = nn.Linear(config.hidden_dim * 2, config.hidden_dim * 2)
        self.Ws = nn.Linear(config.hidden_dim * 2, config.hidden_dim * 2)
        self.Wp = nn.Linear(config.persondim, config.hidden_dim * 2)

        self.v = nn.Linear(config.hidden_dim * 2, 1, bias=False)

    def forward(self, d, outputs, start, person_emded):  # , coverage):
        B, u_s, n = list(outputs.size())
        outputs = outputs.view(u_s, n)
        outputs = outputs[start:]
        u_s, n = list(outputs.size())
        person = person_emded

        d = d.view(-1)
        dec_fea = self.Wd(d)  # B x 2*hidden_dim
        dec_fea_expanded = dec_fea.expand(u_s, -1).contiguous()  # us x 2*hidden_dim
        dec_fea_expanded = dec_fea_expanded.view(u_s, n)  # us x 2*hidden_dim
        p_fra = self.Wp(person)
        p_fra_expanded = p_fra.expand(u_s, -1)
        p_fra_expanded = p_fra_expanded.view(u_s, n)

        us_feature = self.Ws(outputs)

        att_features = us_feature + dec_fea_expanded + p_fra_expanded # B * t_k x 2*hidden_dim
        e = torch.tanh(att_features)  # B * t_k x 2*hidden_dim
        scores = self.v(e)  # B * t_k x 1
        scores = scores.view(-1, u_s)  # B x t_k

        attn_dist_ = F.softmax(scores, dim=1)  # B x us
        normalization_factor = attn_dist_.sum(1, keepdim=True)
        attn_dist = attn_dist_ / normalization_factor
        v, ind = torch.max(attn_dist, dim=1)
        end = int(ind) + start
        # print(start)
        # print(end)
        # print(outputs[start:].size())
        return end, attn_dist  # , coverage # B x 2*hidden_dim, # B x t_k, # B x t_k

#计算一个segment中，每一句话的attention 分布
#输入，隐状态，句子表示(即一个segment内,用wordattention计算的句子表示c_t的集合： [c_t1, c_t2, ...])
#输出：segment表示c_t，分布表
class utterAttention(nn.Module):
    def __init__(self):
        super(utterAttention, self).__init__()
        # attention
        # if config.is_coverage:
        #     self.W_c = nn.Linear(1, config.hidden_dim * 2, bias=False)
        self.Wd = nn.Linear(config.hidden_dim * 2, config.hidden_dim * 2)
        self.Wu = nn.Linear(config.hidden_dim * 2, config.hidden_dim * 2)
        self.v = nn.Linear(config.hidden_dim * 2, 1, bias=False)

    def forward(self, d, utterances):  # , coverage):
        u_s, n = list(utterances.size())
        # print(u_s)
        # print(n)

        dec_fea = self.Wd(d)  # B x 2*hidden_dim
        dec_fea_expanded = dec_fea.expand(1, u_s, -1).contiguous()  # us x 2*hidden_dim
        dec_fea_expanded = dec_fea_expanded.view(-1, n)  # us x 2*hidden_dim

        us_feature = self.Wu(utterances)

        att_features = us_feature + dec_fea_expanded  # B * t_k x 2*hidden_dim
        # if config.is_coverage:
        #     coverage_input = coverage.view(-1, 1)  # B * t_k x 1
        #     coverage_feature = self.W_c(coverage_input)  # B * t_k x 2*hidden_dim
        #     att_features = att_features + coverage_feature

        e = torch.tanh(att_features)  # B * t_k x 2*hidden_dim
        scores = self.v(e)  # B * t_k x 1
        scores = scores.view(-1, u_s)  # B x t_k

        attn_dist_ = F.softmax(scores, dim=1)  # B x us
        normalization_factor = attn_dist_.sum(1, keepdim=True)
        attn_dist = attn_dist_ / normalization_factor
        # print(attn_dist.size())
        # print(utterances.size())

        c_t = torch.matmul(attn_dist, utterances)  # B x 2*hidden_dim
        # print(c_t.size())

        return c_t, attn_dist  # , coverage # B x 2*hidden_dim, # B x t_k, # B x t_k


#计算segment的attention 分布
#输入，隐状态，segment表示(与之前类似,用utterattention计算的seg表示c_t的集合： [c_t1, c_t2, ...])
#输出：文章表示(后面没用)，分布表
class SegAttention(nn.Module):
    def __init__(self):
        super(SegAttention, self).__init__()
        # attention
        # if config.is_coverage:
        #     self.W_c = nn.Linear(1, config.hidden_dim * 2, bias=False)
        self.Wd = nn.Linear(config.hidden_dim * 2, config.hidden_dim * 2)
        self.Wu = nn.Linear(config.hidden_dim * 2, config.hidden_dim * 2)
        self.v = nn.Linear(config.hidden_dim * 2, 1, bias=False)

    def forward(self, d, seg_exp):  # , coverage):
        u_s, n = list(seg_exp.size())

        dec_fea = self.Wd(d)  # B x 2*hidden_dim
        dec_fea_expanded = dec_fea.expand(1, u_s, -1).contiguous()  # us x 2*hidden_dim
        dec_fea_expanded = dec_fea_expanded.view(-1, n)  # us x 2*hidden_dim

        us_feature = self.Wu(seg_exp)

        att_features = us_feature + dec_fea_expanded  # B * t_k x 2*hidden_dim
        # if config.is_coverage:
        #     coverage_input = coverage.view(-1, 1)  # B * t_k x 1
        #     coverage_feature = self.W_c(coverage_input)  # B * t_k x 2*hidden_dim
        #     att_features = att_features + coverage_feature

        e = torch.tanh(att_features)  # B * t_k x 2*hidden_dim
        scores = self.v(e)  # B * t_k x 1
        scores = scores.view(-1, u_s)  # B x t_k

        attn_dist_ = F.softmax(scores, dim=1)  # B x us
        normalization_factor = attn_dist_.sum(1, keepdim=True)
        attn_dist = attn_dist_ / normalization_factor
        # print(attn_dist.size())
        # print(utterances.size())

        c_t = torch.matmul(attn_dist, seg_exp)  # B x 2*hidden_dim
        # print(c_t.size())

        return c_t, attn_dist  # , coverage # B x 2*hidden_dim, # B x t_k, # B x t_k

#我把decoder当成model用了，包含了所有class

class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()
        self.encoder = Encoder()
        self.wordattention_network = wordAttention()
        self.utterattention_network = utterAttention()
        self.segencoder = SegEncoder()
        self.segponit = SegPonit()
        self.segdecoder = Segdecoder()
        self.segattention_network = SegAttention()
        # decoder
        self.embedding = nn.Embedding(config.vocab_size, config.emb_dim).cuda()
        self.embedding.weight.data.copy_(torch.from_numpy(embed_weight))
        # self.x_context = nn.Linear(config.hidden_dim * 2 + config.emb_dim, config.emb_dim)
        self.gru = nn.GRU(config.emb_dim, 2 * config.hidden_dim, num_layers=1, batch_first=True, bidirectional=False)
        init_gru_wt(self.gru)

        # if config.pointer_gen:
        #     self.p_gen_linear = nn.Linear(config.hidden_dim * 4 + config.emb_dim, 1)

        # p_vocab
        self.out1 = nn.Linear(config.hidden_dim * 4, config.hidden_dim)
        self.out2 = nn.Linear(config.hidden_dim, config.vocab_size)
        init_linear_wt(self.out2)
    #y_t_1输入，dt_1隐状态，segs(文章列表，[seg1,seg2,seg3...], seg1:列表[utter1, utter2 ,...])
    def forward(self, y_t_1, dt_1, segs):  # encoder_outputs, c_t_1):

        y_t_1_embd = self.embedding(y_t_1)  # size : embeddingsize

        # print(y_t_1_embd.unsqueeze(0).unsqueeze(0).size())
        # x = self.x_context(torch.cat((c_t_1, y_t_1_embd), 1))

        gru_out, d_t = self.gru(y_t_1_embd.unsqueeze(0), dt_1)
##########感觉问题主要出在这里，应该用其他方法
        seg_exp = torch.Tensor().cuda()# 记录seg表示
        all_words = torch.Tensor().cuda()#记录所有单词的attention
        allseg_words = []                 #记录seg_words  [seg_words1, seg_words2 ,seg_words3]
        for seg in segs:
            utterance_exp = torch.Tensor().cuda() #记录句子表示
            seg_words = torch.Tensor().cuda()      #记录wordatten（用utteratten加权后）
            word_attenlist = []       #包含一个seg内的utters的word_atten
            for utter_person in seg:
                person = utter_person[-1]
                encoder_outputs = utter_person[0]
                c_t_u, word_atten = self.wordattention_network(d_t, encoder_outputs, person)  #计算word_atten
                utterance_exp = torch.cat((utterance_exp, c_t_u), 0)  #计算utterance_exp
                word_atten = word_atten.view(-1)
                word_attenlist.append(word_atten)   #包含一个seg内的utters的word_atten
            c_t_s, utter_atten = self.utterattention_network(d_t, utterance_exp)    #计算utteratten
            seg_exp = torch.cat((seg_exp, c_t_s), 0)#计算seg_exp
            utter_atten = utter_atten.view(-1)
            for i, v in enumerate(word_attenlist):
                temp = word_attenlist[i] * float(utter_atten[i])   #一句话内的wordatten分别和这句话的utteratten相乘
                seg_words = torch.cat((seg_words, temp), 0)   # atten contain  a seg's words
            allseg_words.append(seg_words)   # list [seg_words1, seg_words2 ,seg_words3]
        c_t_a, seg_atten = self.segattention_network(d_t, seg_exp)  #计算segatten
        for i, v in enumerate(allseg_words):
            temp = allseg_words[i] * float(seg_atten[0][i])  #用segatten加权
            all_words = torch.cat((all_words, temp), 0)  # atten contain  a seg's words

        v_w, in_w = torch.max(all_words, 0)
        index_w = in_w
        # print(index_w)
        # index_w = int(in_w)
        # print(d_t.size())
        # print(c_t_u.size())
        output = torch.cat((d_t.view(-1, 2 * config.hidden_dim), c_t_a), 1)  # B x hidden_dim * 4
        output = self.out1(output)  # B x hidden_dim
        output = self.out2(output)  # B x vocab_size
        m = nn.LogSoftmax(dim=1)
        vocab_dist = m(output)
        value, index = torch.max(output, 1)
        # print(output.size())
        # print(vocab_dist)
        #返回word位置indexw, 隐状态，词表logsoftmax分布vocab_dist， word在词表中的位置index
        return index_w, d_t, vocab_dist, index

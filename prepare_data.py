from __future__ import print_function
from __future__ import division
from __future__ import print_function
import numpy as np
import os
import torch
from Lang import Lang
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
wnl = WordNetLemmatizer()
import re
import string
print(string.punctuation)

stop = set(stopwords.words('english'))
#print(stop)
from collections import defaultdict
import pickle

#从xml格式文件中抽出transcrips, abstract segponit person 信息



#去掉一些缩写
def norm(text):
    text = re.sub(r"can\'t", "can not", text)
    text = re.sub(r"cannot", "can not ", text)
    text = re.sub(r"what\'s", "what is", text)
    text = re.sub(r"\'ve ", " have ", text)
    text = re.sub(r"n\'t", " not ", text)
    text = re.sub(r"i\'m", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r" e mail ", " email ", text)
    text = re.sub(r" e \- mail ", " email ", text)
    text = re.sub(r" e\-mail ", " email ", text)
    text = re.sub(r" e\-mail ", " email ", text)
    text = re.sub(r" who's ", " who is ", text)
    text = re.sub(r" we\'ve ", " we have ", text)
    text = re.sub(r" they\'ve ", " they have ", text)
    return text

#去掉单词前后各种符号
def norm_w(word):
    # word = wnl.lemmatize(word)
    word = re.sub(r"\n", "", word)
    word = re.sub(r",", "", word)
    word = re.sub(r'\.', "", word)
    word = re.sub(r':', "", word)
    word = re.sub(r';', "", word)
    word = re.sub(r'"', "", word)
    word = re.sub(r'\'s', "", word)
    word = re.sub(r's\'', "", word)
    word = re.sub(r'\)', "", word)
    word = re.sub(r'\(', "", word)
    word = re.sub(r'-', "", word)
    return word

#抽取transcripts
transcripts_lang = Lang('transcripts_lang')
transcripts_files = [f for f in os.listdir('./ami-transcripts')]
train_dict = defaultdict(list)
for transcripts_filename in transcripts_files:
    meeting = transcripts_filename.split('.')[0]
    f = open('./ami-transcripts/' + transcripts_filename, 'r')
    lines = f.readlines()
    dialogue_info = []
    for line in lines:
        sentence = line.split('#')[1].lower()
        sentence = norm(sentence)
        sentence = [norm_w(x) for x in sentence.split(' ')]
        transcripts_lang.addSentence(sentence)
        sentence_id = [transcripts_lang.getIdFromWord(w) for w in sentence]
        sentence_person = line.split('#')[0]
        person_id = transcripts_lang.person2index[sentence_person]
        segid = int(line.split('#')[2].strip('\n'))
        sentence_info = [person_id, sentence_id, segid]
        dialogue_info.append(sentence_info)
    train_dict[meeting].append(dialogue_info)
print(train_dict.keys())
#抽取abstract

ab_files = [f for f in os.listdir('./ami-abstractive')]
for ab_fliename in ab_files:
    meeting = ab_fliename.split('.')[0]
    f = open('./ami-abstractive/' + ab_fliename, 'r')
    lines = f.readlines()
    ab_info = []
    for line in lines:
        sentence = line.lower()
        sentence = norm(sentence)
        sentence = [norm_w(x) for x in sentence.split(' ')]
        transcripts_lang.addSentence(sentence)
        sentence_id = [transcripts_lang.getIdFromWord(w) for w in sentence]
        ab_info.append(sentence_id)
    if meeting in train_dict.keys():
        train_dict[meeting].append(ab_info)

for key in train_dict.keys():
    if len(train_dict[key]) != 2:
        train_dict.pop(key)
print(train_dict['ES2002a'][1])
print(transcripts_lang.word2index)

#读取glove
f = open('./glove.42B.300d.txt', 'r', encoding='UTF8')
lines = f.readlines()
glove_words = {}
for line in lines:
    [word, vec] = line.split(' ', 1)
    vec = [float(x) for x in vec.split(' ')]
    vec_array = np.array(vec)
    glove_words[word] = vec_array
    # print(vec_array)
    # print(vec)

#用glove 与训练的词向量，如果没有随机初始化
words_num = len(transcripts_lang.index2word)
embedding_dim = 300
embed_weight = np.zeros((words_num, embedding_dim))
count = 0
for i in range(len(transcripts_lang.index2word)):
    word = transcripts_lang.index2word[i]
    if word in glove_words.keys():
        i_vec = glove_words[word]
    else:
        i_vec = np.random.random((1, 300))
        print(word)
        count += 1
    embed_weight[i] = i_vec

print(count)
np.save('embed_weight.npy', embed_weight)
file = open('train_dict.pickle', 'wb')
pickle.dump(train_dict, file)
file.close()
file = open('transcripts_lang.pickle', 'wb')
pickle.dump(transcripts_lang, file)
file.close()

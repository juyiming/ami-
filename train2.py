import torch
from model3 import Decoder
import pickle
import torch.nn as nn
from torch import optim
import numpy as np
import random
import shutil

torch.backends.cudnn.benchmark = True

CUDA_VISIBLE_DEVICES = 1
# inputensor = [[[1, 2, 3, 4, 5, 6, 7, 8, 9],[0]], [[0, 1, 2, 3, 4, 5, 6, 6, 7],[3]], [[0, 1, 2, 5, 6, 6, 7],[4]], [[0, 1, 5, 6, 6, 7],[2]],
#               [[0, 1, 5, 3, 4, 6, 9, 0, 6, 6, 7],[0]], [[0, 1, 5, 6, 1, 1, 1, 1, 6, 7],[1]], [[6, 9, 0, 1, 4, 3],[3]]]
# target_tensor = torch.LongTensor([4, 5, 6, 8, 6, 8, 1])
# seg_tensor = torch.LongTensor([0, 1, 0, 1, 0, 0, 1])


with open('transcripts_lang.pickle', 'rb') as file:
    transcripts_lang = pickle.load(file)
with open('train_dict.pickle', 'rb') as file:
    train_dict = pickle.load(file)
#处理数据，截断长度大于1000句的文章

key_list = []
sentencelens = []
articlelen = []
for key in train_dict.keys():
    key_list.append(key)
    targetlist = train_dict[key][1]
    if len(train_dict[key][0]) > 1000:
        train_dict[key][0] = train_dict[key][0][:1000]
    articlelen.append(len(train_dict[key][0]))
    for sentence in train_dict[key][0]:
        sentencelen = len(sentence[1])
        sentencelens.append(sentencelen)
        if sentencelen > 70:
            sentence[1] = sentence[1][:70]
    target = []
    for sentence in targetlist:
        target.extend(sentence)
    train_dict[key][1] = target


numof_train = int(len(train_dict) * 0.9)
key_fortrain = key_list[:numof_train]
key_foreval = key_list[numof_train:]

def gerner_trainpair():
    key = random.choice(key_fortrain)
    inputlist = train_dict[key][0]
    targetlist = train_dict[key][1]
    return inputlist, targetlist


def gerner_evalpair():
    key = random.choice(key_foreval)
    inputlist = train_dict[key][0]
    targetlist = train_dict[key][1]
    return inputlist, targetlist


def train(input_list, target_list, decoder, decoder_optimizer):
    target_tensor = torch.LongTensor(target_list).cuda()
    loss = 0
    loss_seg = 0
    decoder_optimizer.zero_grad()
    decoder.embedding.weight = decoder.encoder.embedding.weight
    seg_tensor = torch.LongTensor().cuda()
    sentences_encoded = []
    utterance_forseg = torch.Tensor([]).cuda()
    #转换为tensor,进行encoder,记录每一个句子表示（[encoder_outputs, hidden, person_embd]）
    for tensor in input_list:
        person = torch.LongTensor([tensor[0]]).cuda()
        sentence = torch.LongTensor(tensor[1]).cuda()
        seg_tensor = torch.cat((seg_tensor, torch.LongTensor([tensor[2]]).cuda()))

        sentence = sentence.unsqueeze(0)
        encoder_outputs, hidden, person_embd = decoder.encoder(sentence, person)
        sentences_encoded.append([encoder_outputs, hidden, person_embd])

        hidden_cat = hidden.view(1, 1, -1)
        utterance_forseg = torch.cat((utterance_forseg, hidden_cat), 1)

    cri = nn.NLLLoss()

    #根据标签得到segponit列表
    segoutputs, seghidden = decoder.segencoder(utterance_forseg)
    segmask = torch.nonzero(seg_tensor)
    a = segmask.view(-1)

    segstartlist = [0]
    segendlist = list(a)
    segstart = [int(x) + 1 for x in segendlist[:-1]]
    segstartlist.extend(segstart)

    h = seghidden.view(1, 1, -1)
    count = 0

    #对每一个segstart. 计算每一次segend预测的loss
    for segstart in segstartlist:
        startutter_exp = utterance_forseg[0][segstart]
        startutter_exp = startutter_exp.view(1, 1, -1)

        h = decoder.segdecoder(h, startutter_exp)
        person = sentences_encoded[segstart][2]
        end, attn_dist = decoder.segponit(h, segoutputs, segstart, person)
        log_dist = torch.log(attn_dist)
        target_end = segendlist[count] - segstart
        end_tensor = torch.LongTensor([target_end]).cuda()
        loss_seg += cri(log_dist, end_tensor)
        count += 1

    segs_ = []
    # segendlist
    #得到segs表示
    for i in range(len(segstartlist) - 1):
        segtemp = sentences_encoded[segstartlist[i]:segendlist[i] + 1]
        segs_.append(segtemp)
    segtemp = sentences_encoded[segstartlist[-1]:]
    segs_.append(segtemp)

    #decoder计算loss
    dt_1 = sentences_encoded[-1][-2].view(1, 1, -1)
    index_w, d_t, vocab_dist, _ = decoder(torch.LongTensor([1]).cuda(), dt_1, segs_)  # 1  sos
    dt_1 = d_t
    for target in target_tensor:
        target = target.unsqueeze(0)
        loss += cri(vocab_dist, target)
        index_w, d_t, vocab_dist, _ = decoder(target, dt_1, segs_)
        dt_1 = d_t
        # print(target)
    loss += cri(vocab_dist, torch.LongTensor([2]).cuda())  # 2  eos

    loss = loss / len(target_tensor + 1)
    loss_seg = loss_seg / count
    loss = loss + loss_seg
    loss.backward()
    decoder_optimizer.step()

    print(loss)


def trainIter(n_iters, learning_rate=0.05, learning_momentum=0.9):
    decoder = Decoder().cuda()
    #decoder = nn.DataParallel(decoder)  #, device_ids=[0,1,2])

    decoder_optimizer = optim.SGD(decoder.parameters(), lr=learning_rate, momentum=learning_momentum)
    for iter in range(n_iters):
        inputlist, targetlist = gerner_trainpair()
        train(inputlist, targetlist, decoder, decoder_optimizer)
        if iter / 50 == 0:
            torch.save(decoder.state_dict(), './modelrecord/iter{}'.format(iter))
        if iter/20 == 0:
            torch.cuda.empty_cache()
    # print(model1.encoder.embedding.weight.requires_grad)
    # for name, param in model1.encoder.named_parameters():
    #     if param.requires_grad:
    #         print(name)

#
# def evalute(modelpath):
#     input_list, targetlist = gerner_evalpair()
#     decoder = torch.load(modelpath)
#     print('load complete')
#     loss = 0
#     sentences_encoded = []
#     utterance_forseg = torch.Tensor()
#     for tensor in input_list:
#         person = torch.LongTensor([tensor[0]])
#         sentence = torch.LongTensor(tensor[1])
#         sentence = sentence.unsqueeze(0)
#         encoder_outputs, hidden, person_embd = decoder.encoder(sentence, person)
#         sentences_encoded.append([encoder_outputs, hidden, person_embd])
#
#         hidden_cat = hidden.view(1, 1, -1)
#         utterance_forseg = torch.cat((utterance_forseg, hidden_cat), 1)
#
#     segoutputs, seghidden = decoder.segencoder(utterance_forseg)
#     segstart = 0
#
#     segstartlist = []
#     segendlist = []
#     h = seghidden.view(1, 1, -1)
#     count = 0
#     while segstart < len(input_list):
#         segstartlist.append(segstart)
#         startutter_exp = utterance_forseg[0][segstart]
#         startutter_exp = startutter_exp.view(1, 1, -1)
#
#         person = sentences_encoded[segstart][2]
#         h = decoder.segdecoder(h, startutter_exp)
#         end, attn_dist = decoder.segponit(h, segoutputs, segstart, person)
#         count += 1
#         segendlist.append(end)
#         segstart = end + 1
#
#     segs_ = []
#     # segendlist
#     for i in range(len(segstartlist) - 1):
#         segtemp = sentences_encoded[segstartlist[i]:segendlist[i] + 1]
#         segs_.append(segtemp)
#     segtemp = sentences_encoded[segstartlist[-1]:]
#     segs_.append(segtemp)
#     output = []
#     dt_1 = sentences_encoded[-1][-2].view(1, 1, -1)
#     index_w, d_t, vocab_dist, index = decoder(torch.LongTensor([1]), dt_1, segs_)  # 1  sos
#     dt_1 = d_t
#     output.append(index)
#
#     while index != torch.LongTensor([2]):
#         index_w, d_t, vocab_dist, index = decoder(index, dt_1, segs_)
#         dt_1 = d_t
#         output.append(index)
#     print(output)
#     print(segendlist)

trainIter(100000)
# evalute('./modeltrained')
# decoder = Decoder()
# print(decoder.parameters())
# for name, param in decoder.named_parameters():
#     if param.requires_grad:
#         print(name)
